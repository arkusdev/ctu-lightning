declare module "@salesforce/apex/CloneTestLightningController.getSearch" {
  export default function getSearch(): Promise<any>;
}
declare module "@salesforce/apex/CloneTestLightningController.searchUsers" {
  export default function searchUsers(param: {search: any}): Promise<any>;
}
