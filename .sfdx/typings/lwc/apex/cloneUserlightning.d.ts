declare module "@salesforce/apex/cloneUserlightning.test" {
  export default function test(param: {idu: any, fn: any, ln: any, email: any, un: any, alias: any, nickn: any, gPx: any, pAx: any, qMx: any, pGMx: any, pSLAx: any}): Promise<any>;
}
declare module "@salesforce/apex/cloneUserlightning.setFields" {
  export default function setFields(param: {idu: any, fn: any, ln: any, email: any, un: any, alias: any, nickn: any, gPx: any, pAx: any, qMx: any, pGMx: any, pSLAx: any}): Promise<any>;
}
declare module "@salesforce/apex/cloneUserlightning.clone" {
  export default function clone(param: {Id: any}): Promise<any>;
}
declare module "@salesforce/apex/cloneUserlightning.save" {
  export default function save(param: {fn: any, ln: any, email: any, un: any, alias: any, nickn: any, gPx: any, pAx: any, qMx: any, pGMx: any, pSLAx: any}): Promise<any>;
}
