declare module "@salesforce/apex/CloneThisUserV3Controller.getUsers" {
  export default function getUsers(param: {search: any}): Promise<any>;
}
declare module "@salesforce/apex/CloneThisUserV3Controller.getUserCustomFields" {
  export default function getUserCustomFields(param: {userToCloneId: any}): Promise<any>;
}
declare module "@salesforce/apex/CloneThisUserV3Controller.getUserStandardFields" {
  export default function getUserStandardFields(param: {userToCloneId: any}): Promise<any>;
}
declare module "@salesforce/apex/CloneThisUserV3Controller.getGeneralOptionsFields" {
  export default function getGeneralOptionsFields(): Promise<any>;
}
declare module "@salesforce/apex/CloneThisUserV3Controller.searchDelegatedApprover" {
  export default function searchDelegatedApprover(param: {searchTerm: any, userId: any}): Promise<any>;
}
declare module "@salesforce/apex/CloneThisUserV3Controller.containsMiddleNameSuffix" {
  export default function containsMiddleNameSuffix(): Promise<any>;
}
declare module "@salesforce/apex/CloneThisUserV3Controller.cloneThisUser" {
  export default function cloneThisUser(param: {userId: any, serializedDTO: any, customFieldsDTO: any, standardFieldsDTO: any, advancedOptions: any, removeOriginalUserOptions: any}): Promise<any>;
}
declare module "@salesforce/apex/CloneThisUserV3Controller.shareReportsAndDashboards" {
  export default function shareReportsAndDashboards(param: {shareReports: any, shareDashboards: any, deleteReports: any, deleteDashboards: any, newUserId: any, oldUserId: any}): Promise<any>;
}
declare module "@salesforce/apex/CloneThisUserV3Controller.searchProfiles" {
  export default function searchProfiles(param: {searchTerm: any, userId: any}): Promise<any>;
}
declare module "@salesforce/apex/CloneThisUserV3Controller.searchRoles" {
  export default function searchRoles(param: {searchTerm: any}): Promise<any>;
}
