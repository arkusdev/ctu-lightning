declare module "@salesforce/resourceUrl/Bootstrap" {
    var Bootstrap: string;
    export default Bootstrap;
}
declare module "@salesforce/resourceUrl/CTULogo" {
    var CTULogo: string;
    export default CTULogo;
}
declare module "@salesforce/resourceUrl/RequireJS" {
    var RequireJS: string;
    export default RequireJS;
}
declare module "@salesforce/resourceUrl/SLDS0101" {
    var SLDS0101: string;
    export default SLDS0101;
}
declare module "@salesforce/resourceUrl/Velocity" {
    var Velocity: string;
    export default Velocity;
}
