import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class LwcCloneThisUserDetail extends NavigationMixin(LightningElement) {
    @api resultData;
    @api contained;
    @track oldUserUrl;
    @track newUserUrl;
    @track showClonedFields = false;
    @track showStandardClonedFields = false;
    @track showCustomClonedFields = false;
    @track showAdvOpts = false;
    @track showMemberships = false;
    connectedCallback() {
        if (this.resultData) {
            this.generateUrlFromId(this.resultData.oldUser.Id).then(url => {
                this.oldUserUrl = url;
            });

            this.generateUrlFromId(this.resultData.newUser.Id).then(url => {
                this.newUserUrl = url;
            });
        }
    }

    get isMobile(){
        return navigator.userAgent.match(
            /(iPhone|iPod|iPad|Android|webOS|BlackBerry|IEMobile|Opera Mini)/i);
    }

    generateUrlFromId(Id) {
        return this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: Id,
                actionName: 'view',
            },
        })
    }

    generateCTUUrl() {
        return this[NavigationMixin.GenerateUrl]({
            type: 'standard__component',
            attributes: {
                componentName: 'ctu__cloneThisUser',
            },
        })
    }

    handleShowClonedFields(){
        this.showClonedFields = !this.showClonedFields;
    }

    handleShowStandardClonedFields(){
        this.showStandardClonedFields = !this.showStandardClonedFields;
    }

    handleShowCustomClonedFields(){
        this.showCustomClonedFields = !this.showCustomClonedFields;
    }

    handleShowAdvOpts(){
        this.showAdvOpts = !this.showAdvOpts
    }

    handleShowMemberships(){
        this.showMemberships = !this.showMemberships
    }

    handleBack() {
        const backEvent = new CustomEvent('back', {
            bubbles: true,
            composed: true
        });
        this.dispatchEvent(backEvent);
    }

    handleCloneAgain() {
        const selectedEvent = new CustomEvent('cloneagain', {
            detail: { 
                id: this.resultData.oldUser.Id,
                permissionSetAssignments : this.resultData.advancedOptions.PermissionSetAssignments,
                queueMembership : this.resultData.advancedOptions.QueueMembership,
                publicGroupMembership : this.resultData.advancedOptions.PublicGroupMembership,
                permissionSetLicenseAssignments : this.resultData.advancedOptions.PermissionSetLicenseAssignments,
                chatterGroups : this.resultData.advancedOptions.ChatterGroups,
                generatePassword : this.resultData.advancedOptions.GeneratePassword
            },
            bubbles: true,
            composed: true
        });
        this.dispatchEvent(selectedEvent);
    }
}