import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class LwcCloneThisUserResult extends NavigationMixin(LightningElement) {
    @api user;
    @api contained;
    
    get id() {
        return this.user.Id;
    }

    get name() {
        return this.user.Name;
    }

    get userName() {
        return this.user.Username;
    }

    get profileName() {
        if (!this.user.Profile) {
            return '';
        }
        return this.user.Profile.Name;
    }

    get licenceName() {
        if (!this.user.Profile || !this.user.Profile.UserLicense) {
            return '';
        }
        return this.user.Profile.UserLicense.Name;
    }

    get roleName() {
        if (!this.user.UserRole) {
            return '';
        }
        return this.user.UserRole.Name;
    }

    get photoUrl() {
        if (!this.user.SmallPhotoUrl) {
            return '';
        }
        return this.user.SmallPhotoUrl;
    }

    get status() {
        return this.user.IsActive ? 'Active' : 'Inactive';
    }

    handleClick() {
        const comp = this;
        const selectedEvent = new CustomEvent('userselected', {
            detail: { id: comp.id},
            bubbles: true,
            composed: true
        });
        this.dispatchEvent(selectedEvent);

        // const self = this;
        // this[NavigationMixin.Navigate]({
        // type: 'standard__component',
        // attributes: {
        //     componentName: 'ctu__cloneThisUserForm',
        // },
        // state: {
        //     "ctu__userId": self.id
        // }
        // });
    }
}