import { LightningElement, api, track } from 'lwc';
import appexchange_logo from '@salesforce/resourceUrl/Appexchange_Logo';
import youtube_logo from '@salesforce/resourceUrl/YouTube_Logo';
import goToLink from '@salesforce/apex/CloneThisUserUtils.goToLink';

export default class LwcQuickLink extends LightningElement {
    @api title;
    @api name;
    @api showImage;
    @api imageClass;

    connectedCallback() {
        this.imageClass = this.name == "YouTube" ? 'youtube' : 'apexchange';
        this.showImage = this.title.includes('CTU');
    }

    get logo() {
        switch(this.name){
            case "YouTube":
                return youtube_logo;

            case "CTU":
                return appexchange_logo;
        }
    }

    handleClick(){
        const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
        const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

        const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        const systemZoom = width / window.screen.availWidth;
        const left = (width - 1080) / 2 / systemZoom + dualScreenLeft;
        const top = (height - 640) / 2 / systemZoom + dualScreenTop;

        if (this.name){
            switch (this.name){
                case 'Contact_Support':
                    const showModal = new CustomEvent('showmodal', {bubbles: true, detail: {}});
                    this.dispatchEvent(showModal);
                break;

                case 'YouTube':
                    window.open('https://www.youtube.com/embed/videoseries?list=PLGShoxebSA6fs3_xLJmWWBDGhtAFs45CY', 
                    '_blank', 
                    `
                    width=1080,
                    height=640,
                    top=${top}, 
                    left=${left}`);
                break;

                default:
                    goToLink({type: this.name})
                        .then(res =>{
                            window.open(res);
                        })
                break;
            }
        }
    }
}