/* eslint-disable @lwc/lwc/no-async-operation */
import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import CTULogo from '@salesforce/resourceUrl/CTULogo';
import Arkus_Logo from '@salesforce/resourceUrl/Arkus_Logo';
import getUserPermissions from '@salesforce/apex/CloneThisUserUtils.getUserPermissions';
import getPackageVersion from '@salesforce/apex/CloneThisUserUtils.getPackageVersion';
import isSystemAdmin from '@salesforce/apex/CloneThisUserSettings.isSystemAdmin';

export default class LwcCloneThisUserMain extends NavigationMixin(LightningElement) {
    @api search = undefined;
    @api contained;
    @api isutility;

    @track results = undefined;
    @track canManageUsers = false;
    @track manageUsersMessage = '';
    @track showUtility = true;
    @track version = '';
    @track canViewLinks = false;
    @track isSystemAdmin = false;
    @track moreresults = false;
    @track showModal = false;

    connectedCallback() {
        if(this.isutility){
            this.showUtility = false;
        }

        getUserPermissions().then((result) => {
            getPackageVersion().then((version) => {
                this.version = version;
                this.canManageUsers = result['PermissionsManageUsers'].HasPermission;
                if (!this.canManageUsers)
                    this.manageUsersMessage = result['PermissionsManageUsers'].Message;
                isSystemAdmin().then((isSysAdmin) => {
                    this.isSystemAdmin = isSysAdmin;
                    this.canViewLinks = result['CanViewQuickLinksBar'].HasPermission || isSysAdmin;
                })
            })
        })
        .catch((err) => {
            console.log(err);
        });
    }

    get logo() {
        return CTULogo;
    }

    get arkusLogo(){
        return Arkus_Logo;
    }

    get isInitialState() {
        return this.search === undefined;
    }
    get hasResults() {
        return this.results !== undefined && this.results.length > 0;
    }

    handleSearch(event) {
        console.log('Calling search from main');
        this.search = event.detail.search;
        console.log('Search text from main: ' + this.search);
        this.results = event.detail.results;
        this.moreresults = this.results.length >= 100;
    }
    openSettings() {
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName: 'ctu__Clone_This_User_Settings'
            },
        });
    }

    handleShowModal(event) {
        this.showModal = true;
    }

    handleCloseModal() {
        this.showModal = false;
    }
}