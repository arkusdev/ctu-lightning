import { LightningElement, api, track } from 'lwc';

export default class LwcCloneThisUserRole extends LightningElement {
    @track searchResultsLocalState = [];
    @track loading = false;
  
    _searchTerm = '';
    _results = [];
    _defaultSearchResults = [];
    _selectedrole = [];

    _focused = false;
    _blurOut = false;
    _searchTermHandle;
    _searchTimeOut;

    @api
    set selection(initialSelection) {
        this._selectedrole = initialSelection;
    }

    get selection() {
        return this._selectedrole;
    }

    @api
    setSearchResults(results) {
        this.loading = false;
        const resultsLocal = JSON.parse(JSON.stringify(results));
        this._results = resultsLocal.map((result) => {
            return result;
        });
        this.searchResultsLocalState = this._results.map((result) => {
            return {
                result,
                classes: 'slds-media slds-listbox__option slds-listbox__option_entity'
            };
        });
    }

    @api
    getRoleSelected() {
        return this._selectedrole;
    }

    searchTermChange(event) {
        if (this._selectedrole.length > 0) {
            return;
        }
        let newSearchTerm = event.target.value;
        this._searchTerm = newSearchTerm;
        const newCleanSearchTerm = newSearchTerm;
        if (this._searchTermHandle === newCleanSearchTerm) {
            return;
        }
        this._searchTermHandle = newCleanSearchTerm;
        if (newCleanSearchTerm.length < 2) {
            this.setSearchResults(this._defaultSearchResults);
            return;
        }
        this._searchTimeOut = setTimeout(() => {
            if (this._searchTermHandle.length >= 2) {
                this.loading = true;
                const searchEvent = new CustomEvent('search', {
                    detail: {
                        searchTerm: this._searchTermHandle
                    }
                });
                this.dispatchEvent(searchEvent);
            }
            this._searchTimeOut = null;
        }, 250);
    }

    handleResultClick(event) {
        const recordId = event.currentTarget.dataset.recordid;
        let selectedItem = this._results.filter((result) => result.id === recordId);
        if (selectedItem.length === 0) {
            return;
        }
        selectedItem = selectedItem[0];
        const newSelection = [...this._selectedrole];
        newSelection.push(selectedItem);
        this._selectedrole = newSelection;

        this._searchTermHandle = '';
        this._searchTerm = '';
        this._results = this._defaultSearchResults;
        this.dispatchEvent(new CustomEvent('selectionchange', { detail: this._selectedrole.map((sel) => sel.id) }));
    }

    handleComboboxMouseDown(event) {
        this._blurOut = true;
    }

    handleComboboxMouseUp() {
        this._blurOut = false;
    }

    handleFocus() {
        if (!this._selectedrole.length > 0) {
            this._focused = true;
        }
    }

    handleBlur() {
        if (this._selectedrole.length > 0 || this._blurOut) {
            return;
        }
        this._focused = false;
    }

    handleClearSelection() {
        this._selectedrole = [];
        this.dispatchEvent(new CustomEvent('selectionchange', { detail: this._selectedrole.map((sel) => sel.id) }));
    }

    get getDropdownClass() {
        let css = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click ';
        const isSearchTermValid = this._searchTermHandle && this._searchTermHandle.length >= 2;
        if (this._focused && (isSearchTermValid || this._results.length > 0)) {
            css += 'slds-is-open';
        }
        return css;
    }

    get getComboboxClass() {
        let css = 'slds-combobox__form-element slds-input-has-icon ';
        css += this._selectedrole.length > 0 ? 'slds-input-has-icon_left-right' : 'slds-input-has-icon_right';
        return css;
    }

    get getIconClass() {
        return 'slds-combobox__input-entity-icon ' + (this._selectedrole.length > 0 ? '' : 'slds-hide');
    }

    get getValue() {
        return this._selectedrole.length > 0 ? this._selectedrole[0].name : this._searchTerm;
    }

    get roleSelected() {
        return this._selectedrole.length > 0;
    }

    get hasResults() {
        return this._results.length > 0;
    }
}