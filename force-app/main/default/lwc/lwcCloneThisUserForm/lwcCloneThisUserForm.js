/* eslint-disable no-debugger */
/* eslint-disable no-console */
import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import { NavigationMixin } from 'lightning/navigation';

import cloneUser from '@salesforce/apex/CloneThisUserV3Controller.cloneThisUser';
import getUserCustomFields from '@salesforce/apex/CloneThisUserV3Controller.getUserCustomFields';
import getUserStandardFields from '@salesforce/apex/CloneThisUserV3Controller.getUserStandardFields';
import getGeneralOptionsFields from '@salesforce/apex/CloneThisUserV3Controller.getGeneralOptionsFields';
import searchProfileRecords from '@salesforce/apex/CloneThisUserV3Controller.searchProfiles';
import searchRoleRecords from '@salesforce/apex/CloneThisUserV3Controller.searchRoles';
import searchDelegatedApprover from '@salesforce/apex/CloneThisUserV3Controller.searchDelegatedApprover';
import containsMNS from '@salesforce/apex/CloneThisUserV3Controller.containsMiddleNameSuffix';
import getUserPermissions from '@salesforce/apex/CloneThisUserUtils.getUserPermissions';
import shareReportsAndDashboards from '@salesforce/apex/CloneThisUserV3Controller.shareReportsAndDashboards';

export default class LwcCloneThisUserForm extends NavigationMixin(LightningElement) {
    @api userId;
    @api contained;

    @api permissionSetAssignments;
    @api queueMembership;
    @api publicGroupMembership;
    @api permissionSetLicenseAssignments;
    @api chatterGroups;
    @api generatePassword;
    @api shareReports;
    @api shareDashboards;

    @track userUrl = '#';

    
    @track standardFieldDescribe;
    @track customFieldDescribe;

    @track middleNameEnabled = false;
    @track suffixEnabled = false;
    @track profileEnabled = false;
    @track roleEnabled = false;
    @track delegatedApproverEnabled = false;

    @track permissionSetPermissionChecked = false;
    @track queuesPermissionChecked = false;
    @track publicGroupsPermissionChecked = false;
    @track chatterGroupPermissionChecked = false;
    @track permissionSetPermissionLicenseChecked = false;
    @track permissionGeneratePasswordChecked = false;
    @track shareReportsPermissionChecked = false;
    @track shareDashboardsPermissionChecked = false;
    @track viewUser = false;
    @track accountTeamMembershipPermissionChecked = false;
    @track accountOwnershipPermissionChecked = false;
    @track leadOwnershipPermissionChecked = false;
    @track opportunityOwnershipPermissionChecked = false;
    @track opportunityTeamMembershipPermissionChecked = false;
    @track caseOwnershipPermissionChecked = false;
    @track caseTeamMembershipPermissionChecked = false;


    @track permissionSetPermission = {};
    @track queuesPermission = {};
    @track publicGroupsPermission = {};
    @track chatterGroupPermission = {};
    @track permissionSetPermissionLicense = {};
    @track permissionGeneratePassword = {};
    @track shareReportsPermission = {};
    @track shareDashboardsPermission = {};
    @track accountTeamMembershipPermission = {};
    @track accountOwnershipPermission = {};
    @track leadOwnershipPermission = {};
    @track opportunityOwnershipPermission = {};
    @track opportunityTeamMembershipPermission = {};
    @track caseOwnershipPermission = {};
    @track caseTeamMembershipPermission = {};

    @track userData = {
        Alias: '',
        FirstName: '',
        MiddleName: '',
        LastName: '',
        Suffix: '',
        Email: '',
        UserName: '',
        CommunityNickname: '',
    };

    @track standardFields = {};
    @track customFields = {}

    @track displayStandardFields = true;
    @track displayCustomFields = true;

    @track advancedOptions = {
        GeneratePassword: true,
        PermissionSetAssignments: true,
        QueueMembership: true,
        PublicGroupMembership: true,
        ChatterGroups: true,
        PermissionSetLicenseAssignments: true,
        shareReports: true,
        shareDashboards: true,
        accountTeamMembership: true,
        accountOwnership: true,
        leadOwnership: true,
        opportunityOwnership: true,
        opportunityTeamMembership: true,
        caseOwnership: true,
        caseTeamMembership: true
    };

    @track removeOriginalUserOptions = {
        DeactivateUser: false,
        PermissionSetLicenseAssignments: false,
        PermissionSetAssignments: false,
        QueueMembership: false,
        PublicGroupMembership: false,
        ChatterGroups: false,
        ShareReports: false,
        ShareDashboards: false,
        accountTeamMembership: false,
        opportunityTeamMembership: false,
        caseTeamMembership: false
    };

    @track showAdvOpts = true;
    @track showRemoveOriginalUser = true;
    @track errMessages;

    profileInitialSelection = [];
    roleInitialSelection = [];
    delegatedApproverInitialSelection = [];

    tooltipClass = "popover-style slds-text-align_left slds-popover slds-nubbin_left-top slds-popover_large slds-hide";
    toggleTooltip() {
        this.tooltipClass = this.tooltipClass == 'popover-style slds-text-align_left slds-popover slds-nubbin_left-top slds-popover_large slds-hide' ? "popover-style slds-text-align_left slds-popover slds-nubbin_left-top slds-popover_large" : "popover-style slds-text-align_left slds-popover slds-nubbin_left-top slds-popover_large slds-hide"
    }


    @wire(getRecord, { recordId: '$userId', fields: ['User.Id', 'User.SmallPhotoUrl', 'User.Name'] })
    record;

    // renderedCallback() {
    //     const top = this.template.querySelector('.container');
    //     top.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
    // }


    connectedCallback() {
        getGeneralOptionsFields()
            .then((results) => {
                this.advancedOptions.GeneratePassword= results["Generate new password"];
                this.advancedOptions.PermissionSetAssignments= results["Advanced Permission Set Assignments"];
                this.advancedOptions.QueueMembership= results["Advanced Queue Memberships"];
                this.advancedOptions.PublicGroupMembership= results["Advanced Public Group Memberships"];
                this.advancedOptions.ChatterGroups= results["Advanced Chatter Group Memberships"];
                this.advancedOptions.PermissionSetLicenseAssignments= results["Advanced Permission Set License"];
                this.advancedOptions.shareReports= results["Advanced Shared Reports"];
                this.advancedOptions.shareDashboards= results["Advanced Shared Dashboards"];
                this.advancedOptions.accountTeamMembership= results["Advanced Account Team Membership"];
                this.advancedOptions.accountOwnership= results["Advanced Account Ownership"];
                this.advancedOptions.leadOwnership= results["Advanced Lead Ownership"];
                this.advancedOptions.opportunityOwnership= results["Advanced Opportunity Ownership"];
                this.advancedOptions.opportunityTeamMembership= results["Advanced Opportunity Team Membership"];
                this.advancedOptions.caseOwnership= results["Advanced Case Ownership"];
                this.advancedOptions.caseTeamMembership= results["Advanced Case Team Membership"];

                this.removeOriginalUserOptions.DeactivateUser= results["Deactivate Original User"];
                this.removeOriginalUserOptions.PermissionSetAssignments= results["Remove Permission Set Assignments"];
                this.removeOriginalUserOptions.QueueMembership= results["Remove Queue Memberships"];
                this.removeOriginalUserOptions.PublicGroupMembership= results["Remove Public Group Memberships"];
                this.removeOriginalUserOptions.ChatterGroups= results["Remove Chatter Group Memberships"];
                this.removeOriginalUserOptions.PermissionSetLicenseAssignments= results["Remove Permission Set License"];
                this.removeOriginalUserOptions.ShareReports= results["Remove Shared Reports"];
                this.removeOriginalUserOptions.ShareDashboards= results["Remove Shared Dashboards"];
                this.removeOriginalUserOptions.accountTeamMembership= results["Remove Account Team Membership"];
                this.removeOriginalUserOptions.opportunityTeamMembership= results["Remove Opportunity Team Membership"];
                this.removeOriginalUserOptions.caseTeamMembership= results["Remove Case Team Membership"];
                this.userUrl = '/' + this.userId;
                const self = this;
                getUserCustomFields({ userToCloneId: this.userId })
                    .then((results) => {
                        results.forEach((elem) => {
                            var value = elem.isBoolean ? (elem.value == true || elem.value == 'true') : elem.value;
                            self.customFields[elem.name] = value;
                            elem.value = value;
                        });
                        self.customFieldDescribe = results;
                        this.displayCustomFields = this.customFieldDescribe.length > 0;
                        getUserPermissions()
                            .then((results) => {
                                this.permissionSetPermission = results['PermissionsAssignPermissionSets'];
                                this.permissionSetPermissionLicense = results['PermissionsAssignPermissionSetLicenses'];
                                this.publicGroupsPermission = results['PermissionsManagePublicGroup'];
                                this.queuesPermission = results['PermissionsQueues'];
                                this.permissionGeneratePassword = results['PermissionsResetPasswords'];
                                this.chatterGroupPermission = results['PermissionChatterGroups'];
                                this.shareReportsPermission = results['PermissionsManageReports'];
                                this.shareDashboardsPermission = results['PermissionsManageDashboards'];                               
                                this.viewUser = results['PermissionsManagePublicGroup'].HasPermission;
                                this.accountTeamMembershipPermission = results['AccountTeamMembership'];                               
                                this.accountOwnershipPermission = results['AccountOwnership'];                               
                                this.leadOwnershipPermission = results['LeadOwnership'];                               
                                this.opportunityOwnershipPermission = results['OpportunityOwnership'];                               
                                this.opportunityTeamMembershipPermission = results['OpportunityTeamMembership'];                               
                                this.caseOwnershipPermission = results['CaseOwnership'];                               
                                this.caseTeamMembershipPermission = results['CaseTeamMembership'];                          
        
                                this.permissionSetPermissionChecked = this.advancedOptions.PermissionSetAssignments && this.permissionSetPermission.HasPermission;
                                this.queuesPermissionChecked = this.advancedOptions.QueueMembership && this.queuesPermission.HasPermission;
                                this.publicGroupsPermissionChecked = this.advancedOptions.PublicGroupMembership && this.publicGroupsPermission.HasPermission;
                                this.chatterGroupPermissionChecked = this.advancedOptions.ChatterGroups && this.chatterGroupPermission.HasPermission;
                                this.permissionSetPermissionLicenseChecked = this.advancedOptions.PermissionSetLicenseAssignments && this.permissionSetPermissionLicense.HasPermission;
                                this.permissionGeneratePasswordChecked = this.advancedOptions.GeneratePassword && this.permissionGeneratePassword.HasPermission;                  
                                this.shareReportsPermissionChecked = this.advancedOptions.shareReports && this.shareReportsPermission.HasPermission;
                                this.shareDashboardsPermissionChecked = this.advancedOptions.shareDashboards && this.shareDashboardsPermission.HasPermission;
                                this.accountTeamMembershipPermissionChecked = this.advancedOptions.accountTeamMembership = this.advancedOptions.accountTeamMembership && this.accountTeamMembershipPermission.HasPermission;
                                this.accountOwnershipPermissionChecked  = this.advancedOptions.accountOwnership = this.advancedOptions.accountOwnership && this.accountOwnershipPermission.HasPermission;
                                this.leadOwnershipPermissionChecked = this.advancedOptions.leadOwnership = this.advancedOptions.leadOwnership && this.leadOwnershipPermission.HasPermission;
                                this.opportunityOwnershipPermissionChecked = this.advancedOptions.opportunityOwnership = this.advancedOptions.opportunityOwnership && this.opportunityOwnershipPermission.HasPermission;
                                this.opportunityTeamMembershipPermissionChecked = this.advancedOptions.opportunityTeamMembership = this.advancedOptions.opportunityTeamMembership && this.opportunityTeamMembershipPermission.HasPermission;
                                this.caseOwnershipPermissionChecked = this.advancedOptions.caseOwnership = this.advancedOptions.caseOwnership && this.caseOwnershipPermission.HasPermission;
                                this.caseTeamMembershipPermissionChecked  = this.advancedOptions.caseTeamMembership = this.advancedOptions.caseTeamMembership && this.caseTeamMembershipPermission.HasPermission;

                                this.RemoveDeactiveUserChecked = this.removeOriginalUserOptions.DeactivateUser;
                                this.RemovePermissionSetPermissionChecked = this.removeOriginalUserOptions.PermissionSetAssignments && this.permissionSetPermission.HasPermission;
                                this.RemovequeuesPermissionChecked = this.removeOriginalUserOptions.QueueMembership && this.queuesPermission.HasPermission;
                                this.RemovepublicGroupsPermissionChecked = this.removeOriginalUserOptions.PublicGroupMembership && this.publicGroupsPermission.HasPermission;
                                this.RemovechatterGroupPermissionChecked = this.removeOriginalUserOptions.ChatterGroups && this.chatterGroupPermission.HasPermission;
                                this.RemovepermissionSetPermissionLicenseChecked = this.removeOriginalUserOptions.PermissionSetLicenseAssignments && this.permissionSetPermissionLicense.HasPermission;
                                this.RemoveshareReportsPermissionChecked = this.removeOriginalUserOptions.ShareReports && this.shareReportsPermission.HasPermission;
                                this.RemoveshareDashboardsPermissionChecked = this.removeOriginalUserOptions.ShareDashboards && this.shareDashboardsPermission.HasPermission;
                                this.RemoveaccountTeamMembershipPermissionChecked = this.removeOriginalUserOptions.accountTeamMembership && this.accountTeamMembershipPermission.HasPermission;
                                this.RemoveopportunityTeamMembershipPermissionChecked = this.removeOriginalUserOptions.opportunityTeamMembership && this.opportunityTeamMembershipPermission.HasPermission;
                                this.RemovecaseTeamMembershipPermissionChecked = this.removeOriginalUserOptions.caseTeamMembership && this.caseTeamMembershipPermission.HasPermission;
                            })
                            .catch((err) => {
                                console.log(err);
                            })
                    })
                    .catch((err) => {
                        console.log(err);
                    })
                containsMNS({})
                    .then((results) => {
                        this.middleNameEnabled = results['MiddleName'];
                        this.suffixEnabled = results['Suffix'];
                    })
                    .catch((error) => {
                        console.log(error);
                    })
                getUserStandardFields({userToCloneId: this.userId})
                    .then((results) => {
                        let standadrdFieldDescribeCopy = [];
                        results.forEach((elem) => {
                            var value = elem.isBoolean ? (elem.value == true || elem.value == 'true') : elem.value;
                            if(elem.name != undefined){
                                self.standardFields[elem.name] = value;
                            }
                            if(elem.name != 'UserRoleId' && elem.name != 'ProfileId' && elem.name != 'DelegatedApproverId'){
                                standadrdFieldDescribeCopy.push({
                                    isRequired: elem.isRequired,
                                    name: elem.name,
                                    value: value
                                });
                            } else if(elem.name == 'ProfileId'){
                                this.profileInitialSelection = [{id: elem.value, name: elem.valueName}]
                                this.profileEnabled = true;
                                self.standardFields['ProfileId'] = elem.value;
                            } else if(elem.name == 'UserRoleId'){
                                if(elem.value != undefined){
                                    this.roleInitialSelection = [{id: elem.value, name: elem.valueName}]
                                    self.standardFields['UserRoleId'] = elem.value;
                                }
                                this.roleEnabled = true;
                            } else if(elem.name == 'DelegatedApproverId'){
                                if(elem.value != undefined){
                                    this.delegatedApproverInitialSelection = [{id: elem.value, name: elem.valueName}]
                                    self.standardFields['DelegatedApproverId'] = elem.value;
                                }
                                this.delegatedApproverEnabled = true;
                            }
                        });
                        self.standardFieldDescribe = standadrdFieldDescribeCopy;
                        this.displayStandardFields = results.length > 0;
                    })
                    .catch((error) => {
                        console.log(error);
                    })        
                    
            })
            .catch((error) => {
                console.log(error);
            })
    }

    handleGNPchange(evt) {
        this.advancedOptions.GeneratePassword = evt.target.checked;
    }

    handlePSAchange(evt) {
        this.advancedOptions.PermissionSetAssignments = evt.target.checked;
    }

    handleQMchange(evt) {
        this.advancedOptions.QueueMembership = evt.target.checked;
    }

    handlePGMchange(evt) {
        this.advancedOptions.PublicGroupMembership = evt.target.checked;
    }

    handleCGchange(evt) {
        this.advancedOptions.ChatterGroups = evt.target.checked;
    }

    handleSRchange(evt) {
        this.advancedOptions.shareReports = evt.target.checked;
    }

    handleSDchange(evt) {
        this.advancedOptions.shareDashboards = evt.target.checked;
    }

    handleAccTMchange(evt) {
        this.advancedOptions.accountTeamMembership = evt.target.checked;
    }

    handleAccOwnchange(evt) {
        this.advancedOptions.accountOwnership = evt.target.checked;
    }

    handleLOchange(evt) {
        this.advancedOptions.leadOwnership = evt.target.checked;
    }
    
    handleOppOwchange(evt) {
        this.advancedOptions.opportunityOwnership = evt.target.checked;
    }

    handleOppTMchange(evt) {
        this.advancedOptions.opportunityTeamMembership = evt.target.checked;
    }

    handleCaOwchange(evt) {
        this.advancedOptions.caseOwnership = evt.target.checked;
    }

    handleCaTMchange(evt) {
        this.advancedOptions.caseTeamMembership = evt.target.checked;
    }

    handlePSLAchange(evt) {
        this.advancedOptions.PermissionSetLicenseAssignments = evt.target.checked;
    }

    handleShowAdvOpts() {
        this.showAdvOpts = !this.showAdvOpts;
    }

    handleTimeChange(evt){
        const self = this;
        self.customFields[evt.target.dataset.targetId] = evt.target.value;
    }

    handleShowRemoveOriginalUser() {
        this.showRemoveOriginalUser = !this.showRemoveOriginalUser;
    }

    handleDeactivateChange(evt) {
        this.removeOriginalUserOptions.DeactivateUser = evt.target.checked;
    }

    handleDeactivatePSA(evt) {
        this.removeOriginalUserOptions.PermissionSetAssignments = evt.target.checked;
    }

    handleDeactivateQM(evt) {
        this.removeOriginalUserOptions.QueueMembership = evt.target.checked;
    }

    handleDeactivatePGM(evt) {
        this.removeOriginalUserOptions.PublicGroupMembership = evt.target.checked;
    }

    handleDeactivateCG(evt) {
        this.removeOriginalUserOptions.ChatterGroups = evt.target.checked;
    }

    handleDeactivateSR(evt) {
        this.removeOriginalUserOptions.ShareReports = evt.target.checked;
    }

    handleDeactivateSD(evt) {
        this.removeOriginalUserOptions.ShareDashboards = evt.target.checked;
    }

    handleDeactivateATM(evt) {
        this.removeOriginalUserOptions.accountTeamMembership = evt.target.checked;
    }

    handleDeactivateOppTM(evt) {
        this.removeOriginalUserOptions.opportunityTeamMembership = evt.target.checked;
    }

    handleDeactivateCaTM(evt) {
        this.removeOriginalUserOptions.caseTeamMembership = evt.target.checked;
    }

    handleDeactivatePSLA(evt) {
        this.removeOriginalUserOptions.PermissionSetLicenseAssignments = evt.target.checked;
    }

    handleFirstNameChange(evt) {
        this.userData.FirstName = evt.target.value;
    }

    handleMiddleNameChange(evt){
        this.userData.MiddleName = evt.target.value;
    }

    handleLastNameChange(evt) {
        this.userData.LastName = evt.target.value;
    }

    handleSuffixChange(evt){
        this.userData.Suffix = evt.target.value;
    }

    handleEmailChange(evt) {
        this.userData.Email = evt.target.value;
    }

    handleUserNameChange(evt) {
        this.userData.UserName = evt.target.value;
    }

    handleAliasChange(evt) {
        this.userData.Alias = evt.target.value;
    }

    handleNicknameChange(evt) {
        this.userData.CommunityNickname = evt.target.value;
    }

    handleBlurLastName() {
        if (this.userData.Alias === '') {
            this.userData.Alias = this.userData.FirstName.substr(0, 1) + this.userData.LastName.substr(0, 4);
        }
    }

    handleCustomFieldChange(evt) {
        this.customFields[evt.target.dataset.field] = evt.target.value === '' ? null : evt.target.value;
    }

    handleStandardFieldChange(evt){
        this.standardFields[evt.target.dataset.field] = evt.target.value === '' ? null : evt.target.value;
    }

    handleBlurEmail(event) {
        if (event.currentTarget.checkValidity()) {
            if (this.userData.UserName === '') {
                this.userData.UserName = this.userData.Email;
            }

            if (this.userData.CommunityNickname === '') {
                this.userData.CommunityNickname = this.userData.Email.split('@')[0];
            }
        }
    }

    save(event) {
        let value = event.target.name;
        let self = this;
        if(this.profileEnabled && this.standardFields['ProfileId'] == null){
            this.errMessages = 'Profile is required';
            return;
        }
        this.errMessages = undefined;
        const allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
        const allCustomValids = [...this.template.querySelectorAll('lightning-input-field')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar;
            }, true);
        if (allValid && allCustomValids) {
            cloneUser({
                userId: this.userId,
                serializedDTO: JSON.stringify(this.userData),
                customFieldsDTO: JSON.stringify(this.customFields),
                standardFieldsDTO: JSON.stringify(this.standardFields),
                advancedOptions: JSON.stringify(this.advancedOptions),
                removeOriginalUserOptions: JSON.stringify(this.removeOriginalUserOptions)
            }).then((results) => {
                this.errMessages = undefined;
                this.clearFields();

                var oldUserId = results.oldUser.Id;
                var newUserId = results.newUser.Id;

                shareReportsAndDashboards({
                    shareReports: this.advancedOptions.shareReports,
                    shareDashboards: this.advancedOptions.shareDashboards,
                    deleteReports: this.removeOriginalUserOptions.ShareReports,
                    deleteDashboards: this.removeOriginalUserOptions.ShareDashboards,
                    newUserId: newUserId,
                    oldUserId: oldUserId
                }).then(() =>{
                    if (value == 'saveAndClone') {
                        this.showToast('User cloned', 'New user was successfully cloned', 'success');
                        self.customFieldDescribe = [];
                        self.standardFieldDescribe = [];
                        this.connectedCallback();
                    }else{
                        this.emitResults(results);
                    }
                })
                .catch((error) => {
                    if (error != null && error.body != null && error.body.message)
                        this.errMessages = error.body.message;
                    else
                        this.errMessages = 'An error has occurred';
                })
            }).catch((error) => {
                if (error != null && error.body != null && error.body.message)
                    this.errMessages = error.body.message;
                else
                    this.errMessages = 'An error has occurred';
            });
        } else {
            this.errMessages = undefined;
        }
    }

    clearFields() {
        this.userData = {
            Alias: '',
            FirstName: '',
            LastName: '',
            Email: '',
            UserName: '',
            CommunityNickname: '',
        };
    }

    showToast(title, message, variant) {
        const toast = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(toast);
    }

    handleCancel() {
        const cancelEvent = new CustomEvent('cancel', {
            detail: { search: this.record.data.fields.Name.value },
            bubbles: true,
            composed: true
        });
        this.dispatchEvent(cancelEvent);
    }

    emitResults(results) {
        let data = JSON.stringify(
            {
                newUser: results.newUser,
                oldUser: results.oldUser,
                standardClonedFields: results.standardClonedFields,
                customClonedFields: results.customClonedFields,
                advancedOptions: results.advancedOptions,
                clonedMemberships: results.clonedMemberships
            }
        );
        data = unescape(encodeURIComponent(data));
        const createdEvent = new CustomEvent('usercreated', {
            detail: { data: btoa(data) },
            bubbles: true,
            composed: true
        });
        this.dispatchEvent(createdEvent);
    }

    // For profile custom lookup and Role custom lookup
    handleSearchProfile(event) {
        searchProfileRecords(event.detail)
            .then((results) => {
                this.template.querySelector('c-lwc-clone-this-user-profile').setSearchResults(results);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    handleSearchDelegatedApprover(event){
        searchDelegatedApprover(event.detail)
            .then((results) => {
                this.template.querySelector('c-lwc-clone-this-user-delegated-approver').setSearchResults(results);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    handleChangeProfile() {
        const selection = this.template.querySelector('c-lwc-clone-this-user-profile').getProfileSelected();
        this.standardFields['ProfileId'] = selection.length > 0 ? selection[0].id : null;
    }

    handleChangeDelegatedApprover() {
        const selection = this.template.querySelector('c-lwc-clone-this-user-delegated-approver').getDelegatedApproverSelected();
        this.standardFields['DelegatedApproverId'] = selection.length > 0 ? selection[0].id : null;
    }

    handleSearchRole(event) {
        searchRoleRecords(event.detail)
            .then((results) => {
                this.template.querySelector('c-lwc-clone-this-user-role').setSearchResults(results);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    handleChangeRole() {
        const selection = this.template.querySelector('c-lwc-clone-this-user-role').getRoleSelected();
        this.standardFields['UserRoleId'] = selection.length > 0 ? selection[0].id : null;
    }

    get getFirstNameClass() {
        return this.middleNameEnabled ? 'slds-col slds-size_1-of-1 slds-large-size_1-of-2 slds-p-left_xxx-small slds-p-right_xxx-small' : 
                                        'slds-col slds-size_1-of-1 slds-large-size_2-of-2 slds-p-left_xxx-small slds-p-right_xxx-small';
    }

    get getLastNameClass() {
        return this.suffixEnabled ? 'slds-col slds-size_1-of-1 slds-large-size_1-of-2 slds-p-left_xxx-small slds-p-right_xxx-small' : 
                                    'slds-col slds-size_1-of-1 slds-large-size_2-of-2 slds-p-left_xxx-small slds-p-right_xxx-small';
    }
}