import { LightningElement, track,api } from 'lwc';

export default class LwcCloneThisUser extends LightningElement {
    @track selectedUserId = undefined;
    @track clonedUserData = undefined;
    @api contained;
    @api issalesforcenative;
    @api isutility;

    connectedCallback() {
        this.contained = true;   
    }
    
    get showSearchPanel() {
        return !this.selectedUserId;
    }

    get showClonePanel() {
        return this.selectedUserId && !this.clonedUserData;
    }

    get showResultPanel() {
        return this.clonedUserData;
    }

    handleUserSelected(event) {
        this.selectedUserId = event.detail.id;
    }

    handleUserCreated(event) {
        this.clonedUserData = JSON.parse(decodeURIComponent(escape((atob(event.detail.data)))));
    }
    handleCancel() {
        this.selectedUserId = undefined;
    }

    handleBack(){
        this.selectedUserId = undefined;
        this.clonedUserData = undefined;
    }

    handleCloneAgain(event){
        this.clonedUserData = undefined;
        this.selectedUserId = event.detail.id;
    }
}