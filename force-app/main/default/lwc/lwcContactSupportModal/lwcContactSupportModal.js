import { LightningElement, track } from 'lwc';

export default class LwcContactSupportModal extends LightningElement {
    closeModal() {
        const showModal = new CustomEvent('closemodal', {bubbles: true, detail: {}});
        this.dispatchEvent(showModal);
    }
    submitDetails() {
        const showModal = new CustomEvent('closemodal', {bubbles: true, detail: {}});
        this.dispatchEvent(showModal);
    }
}