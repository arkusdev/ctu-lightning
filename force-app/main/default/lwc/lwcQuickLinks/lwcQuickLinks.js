import { LightningElement, track, api } from 'lwc';

export default class LwcQuickLinks extends LightningElement {
    @track display = false;
    @api className = 'body';
    @track options = [
        {name: 'Users', label: 'Users'},
        {name: 'Profiles', label: 'Profiles'},
        {name: 'Roles', label: 'Roles'},
        {name: 'Public_Groups', label: 'Public Groups'},
        {name: 'Queues', label: 'Queues'},
        {name: 'Permission_Sets', label: 'Permission Sets'},
        {name: 'Contact_Support', label: 'Contact Support'},
        {name: 'YouTube', label: 'CTU on'},
        {name: 'CTU', label: 'CTU in'}
    ];

    handleShow(){
        this.display = !this.display;
        this.className = this.display ? 'body-display' : 'body';
    }

    handleShowModal(event){
        const showModal = new CustomEvent('showmodal', {bubbles: true, detail: {}});
        this.dispatchEvent(showModal);
    }
}