/* eslint-disable @lwc/lwc/no-async-operation */
import { LightningElement, track, api } from 'lwc';
import searchUsers from '@salesforce/apex/CloneThisUserV3Controller.getUsers';
export default class LwcCloneThisUserSearch extends LightningElement {
    @track showSpinner = false;
    @api search = undefined;
    timeout;
    error;
    searchDelay = 300;

    handleKeyUp(event) {
        this.search = event.target.value;
        this.doSearch();
    }

    connectedCallback(){
        if(this.search !== undefined){
            this.doSearch();
        }
    }

    doSearch(){
        console.log('Starting to search for users');
        clearTimeout(this.timeout);
        const self = this;
        console.log('Searching: ' + self.search);
        this.timeout = setTimeout(() => {
            this.showSpinner = true;
            searchUsers({ search: self.search })
                .then((results) => {
                    console.log('results found size: ' + results.length);
                    self.emitResults(results);
                    self.showSpinner = false;
                })
                .catch((error) => {
                    console.log('Error when searching for users: ' + JSON.stringify(error));
                    self.error = error;
                    self.showSpinner = false;
                });
        }, self.searchDelay);
    }

    emitResults(results) {
        const searchEvent = new CustomEvent('search', {
            detail: {search: this.search, results: results}
        });
        this.dispatchEvent(searchEvent);
    }
}