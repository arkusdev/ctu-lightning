@isTest
public class CloneThisUserTest {
        @TestSetup
        static void makeData(){
            PermissionSet ps = new PermissionSet(Label = 'Only testing', Name = 'Only_Testing');
            insert ps;
            PermissionSetLicense psl = [SELECT Id FROM PermissionSetLicense LIMIT 1];
            
            PermissionSetLicenseAssign psla = new PermissionSetLicenseAssign(PermissionSetLicenseId = psl.Id, AssigneeId = UserInfo.getUserId());
            insert psla;
            
            PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = UserInfo.getUserId(), PermissionSetId = ps.Id);
            insert psa;
        }
        static testMethod void test(){
            
            ApexPages.currentPage().getParameters().put('q', 'test');
            ApexPages.currentPage().getParameters().put('success', 'test');
            
            CloneThisUserList listClass = new CloneThisUserList();
            
            ApexPages.currentPage().getParameters().put('Id', UserInfo.getUserId());
            Profile p = [select id from profile where name='System Administrator'];
            User u = new User(ProfileId=p.Id,LastName='aa',FirstName='new 1',Username='fefe@dfd.com',email='example@aa.com',alias='fefe',LocaleSidKey='en_US',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',TimeZoneSidKey='America/New_York');
            insert u;

            System.runAs(u){
                CloneThisUser cloneClass = new CloneThisUser();
                cloneClass.save();
                cloneClass.goBack();
                cloneClass.setErrorMessage(Statuscode.LICENSE_LIMIT_EXCEEDED);
                cloneClass.setErrorMessage(Statuscode.DUPLICATE_USERNAME);
                cloneClass.setErrorMessage(Statuscode.DUPLICATE_COMM_NICKNAME);
                cloneClass.setErrorMessage(Statuscode.FIELD_CUSTOM_VALIDATION_EXCEPTION);
                System.assertEquals(listClass.lstUser != null, true);
            }
        }

        static testMethod void testDesktop(){
            CloneThisUserList listClass = new CloneThisUserList();
            ApexPages.currentPage().getParameters().put('q', 'test');
            ApexPages.currentPage().getParameters().put('Desktop', 'test');
            ApexPages.currentPage().getParameters().put('success', 'test');
            
            ApexPages.currentPage().getParameters().put('Id', UserInfo.getUserId());

            CloneThisUser cloneClass = new CloneThisUser();
            cloneClass.goBack();
            System.assertEquals(listClass.lstUser != null, true);
        }

        static testMethod void testDesktopNoQuery(){
            CloneThisUserList listClass = new CloneThisUserList();
            ApexPages.currentPage().getParameters().put('Desktop', 'test');
            ApexPages.currentPage().getParameters().put('success', 'test');
            
            ApexPages.currentPage().getParameters().put('Id', UserInfo.getUserId());

            CloneThisUser cloneClass = new CloneThisUser();
            cloneClass.goBack();
            System.assertEquals(listClass.lstUser != null, true);
        }
        
        static testMethod void testThisUserSuccess(){
            Profile p = [select id from profile where name='Standard User'];
            User u = new User(ProfileId=p.Id,LastName='aa',FirstName='new 1',Username='fefe@dfd.com',email='example@aa.com',alias='fefe',LocaleSidKey='en_US',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',TimeZoneSidKey='America/New_York');
            insert u;
            User u2 = new User(ProfileId=p.Id,LastName='aa2',FirstName='new 2',Username='a2a@ferghit.com',email='example@aa2.com',alias='fefe',LocaleSidKey='en_US',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',TimeZoneSidKey='America/New_York');
            insert u2;
            ApexPages.currentPage().getParameters().put('old', u.Id);
            ApexPages.currentPage().getParameters().put('new', u2.Id);
            ApexPages.currentPage().getParameters().put('cloned', 'one,two,three');
            CloneThisUserSuccess listClass = new CloneThisUserSuccess();
            system.assertequals(listClass.clonedThings != null,true);
          
        }

        static testMethod void testThisUserAsReadOnlySuccess(){
            Profile p = [select id from profile where name='Read Only'];
            User u = new User(ProfileId=p.Id,LastName='aa',FirstName='new 1',Username='fefe@dfd.com',email='example@aa.com',alias='fefe',LocaleSidKey='en_US',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',TimeZoneSidKey='America/New_York');
            insert u;
            User u2 = new User(ProfileId=p.Id,LastName='aa2',FirstName='new 2',Username='a2a@ferghit.com',email='example@aa2.com',alias='fefe',LocaleSidKey='en_US',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',TimeZoneSidKey='America/New_York');
            insert u2;
            ApexPages.currentPage().getParameters().put('q', '*');
            ApexPages.currentPage().getParameters().put('old', u.Id);
            ApexPages.currentPage().getParameters().put('new', u2.Id);
            ApexPages.currentPage().getParameters().put('success', '1');
            ApexPages.currentPage().getParameters().put('cloned', 'one,two,three');
            System.runAs(u){
                CloneThisUserList listClass = new CloneThisUserList();
                System.assertEquals(listClass.lstUser != null, true);
            }
          
        }

        static testMethod void testGoBackToCTUPage(){
            CloneThisUserList listClass = new CloneThisUserList();
            PageReference page = listClass.goBack();
            System.assert(page != null);
        }

        static testMethod void testSettings(){
            CloneThisUserList listClass = new CloneThisUserList();
            PageReference page = listClass.Settings();
            System.assert(page != null);
        }
}