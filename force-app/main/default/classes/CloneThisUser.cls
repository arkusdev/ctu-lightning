public with sharing class CloneThisUser{

    public class AccessibilityException extends Exception {}

    public User u{get;set;}
    public User toClone{get;set;}
    public Boolean generatePassword{get;set;}
    public Boolean PermissionSetAssignments{get;set;}
    public Boolean QueueMembership{get;set;}
    public Boolean PublicGroupMembership{get;set;}
    public Boolean PermissionSetLicenseAssignments{get;set;}
    public Map<String, CloneThisUserUtils.PermissionInfo> Permissions{get;set;}
    public String errorMsg{get;set;}
    String Desktop;
    String Id;
    
    public List<String> fieldsToClone;
    public Map<String, Schema.SObjectField> fields;
    public CloneThisUser(){
        if (!User.sObjectType.getDescribe().isCreateable()){
            throw new AccessibilityException('The user has no access to create new users');
        }
        fieldsToClone = new List<String>();
        fieldsToClone.add('CallCenterId');
        fieldsToClone.add('CompanyName');
        fieldsToClone.add('EmailEncodingKey');
        fieldsToClone.add('ManagerId');
        fieldsToClone.add('LanguageLocaleKey');
        fieldsToClone.add('LocaleSidKey');
        fieldsToClone.add('ProfileId');
        fieldsToClone.add('UserRoleId');
        fieldsToClone.add('TimeZoneSidKey');
        fieldsToClone.add('UserPermissionsWorkDotComUserFeature');
        fieldsToClone.add('UserPermissionsMarketingUser');
        fieldsToClone.add('UserPermissionsOfflineUser');
        fieldsToClone.add('UserPermissionsInteractionUser');
        fieldsToClone.add('UserPreferencesHideS1BrowserUI');
        fieldsToClone.add('UserPermissionsSFContentUser');
        fieldsToClone.add('UserPreferencesApexPagesDeveloperMode');
        fieldsToClone.add('ForecastEnabled');
        fieldsToClone.add('UserPreferencesContentNoEmail');
        fieldsToClone.add('UserPreferencesContentEmailAsAndWhen');
        fieldsToClone.add('UserPermissionsKnowledgeUser');
        fieldsToClone.add('UserPermissionsSiteforceContributorUser');
        fieldsToClone.add('UserPermissionsSiteforcePublisherUser');
        fieldsToClone.add('UserPermissionsSupportUser');
        fieldsToClone.add('ReceivesAdminInfoEmails');
        fieldsToClone.add('Street');
        fieldsToClone.add('City');
        fieldsToClone.add('PostalCode');
        fieldsToClone.add('Country');
        fieldsToClone.add('State');
        fieldsToClone.add('DelegatedApproverId');
        fieldsToClone.add('JigsawImportLimitOverride');
        fieldsToClone.add('ReceivesInfoEmails');
        errorMsg = '';
        Id = ApexPages.CurrentPage().getParameters().get('Id');
        Desktop = ApexPages.CurrentPage().getParameters().get('Desktop');
        if(Id != null){
            Schema.SObjectType s = User.sObjectType ;
            Schema.DescribeSObjectResult r = s.getDescribe() ;
            fields = r.fields.getMap();

            String exceptionMessage = 'You need access to the following field: ';
            if (!Schema.sObjectType.User.fields.isActive.isAccessible()){
                throw new AccessibilityException(exceptionMessage + 'User Is Active');
            }
            if (!Schema.sObjectType.User.fields.Name.isAccessible()){
                throw new AccessibilityException(exceptionMessage + 'Name');
            }
            if (!Schema.sObjectType.UserLicense.fields.Name.isAccessible()){
                throw new AccessibilityException(exceptionMessage + 'User License Name');
            }

            String query = 'Select Email, Profile.UserLicense.Name, Name, isActive';
            for(String f : fieldsToClone){
                if ((fields.get(f) != null) && (fields.get(f).getDescribe().isAccessible())){
                    if(fields.ContainsKey(f)){
                        query += ', ' + f;
                    }
                }
            }
            query += ' FROM User WHERE Id=: Id';
            toClone = Database.query(query);//[SELECT Profile.UserLicense.Name, Name, CallCenterId, CompanyName, EmailEncodingKey, ManagerId, LanguageLocaleKey, LocaleSidKey, ProfileId, UserRoleId, TimeZoneSidKey, isActive, UserPermissionsWorkDotComUserFeature, UserPermissionsMarketingUser, UserPermissionsOfflineUser, UserPermissionsInteractionUser, UserPreferencesHideS1BrowserUI, UserPermissionsSFContentUser, UserPreferencesApexPagesDeveloperMode, ForecastEnabled, UserPreferencesContentNoEmail, UserPreferencesContentEmailAsAndWhen, UserPermissionsKnowledgeUser, UserPermissionsSiteforceContributorUser, UserPermissionsSiteforcePublisherUser, ReceivesAdminInfoEmails, City, PostalCode, Country, Street, DelegatedApproverId, JigsawImportLimitOverride, ReceivesInfoEmails FROM User WHERE Id=: Id];
            u = cloneUser(toClone);
        }
        Permissions = CloneThisUserUtils.getUserPermissions();
        generatePassword = true;
        PermissionSetAssignments = Permissions.get('PermissionsAssignPermissionSets').HasPermission;
        QueueMembership = Permissions.get('PermissionsQueues').HasPermission;
        PublicGroupMembership = Permissions.get('PermissionsManagePublicGroup').HasPermission;
        PermissionSetLicenseAssignments = Permissions.get('PermissionsAssignPermissionSetLicenses').HasPermission;
        
    }
    
    public PageReference save(){
        String clonedData = '';
        System.SavePoint sp = Database.setSavepoint();
        try{
            if (User.sObjectType.getDescribe().isCreateable()){
                if(generatePassword){
                    Database.DMLOptions dlo = new Database.DMLOptions();
                    dlo.EmailHeader.triggerUserEmail = true;
                    u.setOptions(dlo); 
                }
                insert u;
            }else {
                throw new AccessibilityException('The user has no access to create new users');
            }
        }
        catch(System.DmlException dml){
            return setErrorMessage(dml.getDmlType(0));
        }
        catch(Exception e){
            system.debug('####Generic Exception: ' + e);
            errorMsg =  e.getMessage();
            return null;
        }
        errorMsg = '';
        try{
            if(PermissionSetLicenseAssignments){
                clonedData += 'Permission Set Assignments';
                List<PermissionSetLicenseAssign> pslaInsertList = new List<PermissionSetLicenseAssign>();
                List<PermissionSetLicenseAssign> existingPermitions = [SELECT PermissionSetLicenseId FROM PermissionSetLicenseAssign WHERE AssigneeId =: u.Id];
                for(PermissionSetLicenseAssign psla : [Select AssigneeId, PermissionSetLicenseId FROM PermissionSetLicenseAssign WHERE AssigneeId =: toClone.Id]){
                    if (existingPermitions.isEmpty()){
                        PermissionSetLicenseAssign pslaNew = new PermissionSetLicenseAssign(AssigneeId = u.Id, PermissionSetLicenseId = psla.PermissionSetLicenseId);
                        pslaInsertList.add(pslaNew);
                    } else {
                        for (PermissionSetLicenseAssign existingPsla : existingPermitions){
                            PermissionSetLicenseAssign pslaNew = new PermissionSetLicenseAssign(AssigneeId = u.Id, PermissionSetLicenseId = psla.PermissionSetLicenseId);
                            if (!(existingPsla.PermissionSetLicenseId == pslaNew.PermissionSetLicenseId)){
                                pslaInsertList.add(pslaNew);
                            }  
                        }
                    }
                }
                if (!Test.isRunningTest() && PermissionSetLicenseAssign.sObjectType.getDescribe().isCreateable()) insert pslaInsertList;
            }
        }
        catch(DmlException e){
            errorMsg = e.getDmlMessage(0);
            Database.rollback(sp);
            u.Id = null;
            return null;
        }
        
        //Create permissionSetAssignments
        boolean ins;
        if(QueueMembership){
            clonedData += ',Queue Memberships';
        }
        if(PublicGroupMembership){
            clonedData += ',Public Group Memberships';
        }

        List<GroupMember> gmInsertList = new List<GroupMember>();
        for(GroupMember gm : [Select UserOrGroupId, GroupId, Group.Type FROM GroupMember WHERE UserOrGroupId=: toClone.Id]){
            ins = false;
            if(QueueMembership && gm.Group.Type == 'Queue'){
                ins = true;
            }
            else if(PublicGroupMembership && gm.Group.Type != 'Queue'){
                ins = true;
            }
            if(ins && Schema.sObjectType.GroupMember.fields.UserOrGroupId.isCreateable() && Schema.sObjectType.GroupMember.fields.GroupId.isCreateable()){
                GroupMember gmNew = new GroupMember(UserOrGroupId = u.Id, GroupId = gm.GroupId );
                gmInsertList.add(gmNew);
            }
        }
        if (!Test.isRunningTest() && GroupMember.sObjectType.getDescribe().isCreateable()){
            insert gmInsertList;
        }

        if(PermissionSetAssignments){
            clonedData += ',Permission Set License Assignments';
            List<PermissionSetAssignment> psaInsertList = new List<PermissionSetAssignment>();
            List<PermissionSetAssignment> existingPermitionsAssign = [SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId =: u.Id AND PermissionSet.IsOwnedByProfile = false];
            for(PermissionSetAssignment psa : [Select AssigneeId, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment WHERE AssigneeId =: toClone.Id AND PermissionSet.IsOwnedByProfile = false]){
                String i = toClone.ProfileId;
                if (existingPermitionsAssign.isEmpty()){
                    PermissionSetAssignment psaNew = new PermissionSetAssignment(AssigneeId = u.Id, PermissionSetId = psa.PermissionSetId);
                    psaInsertList.add(psaNew);
                } else {
                    for (PermissionSetAssignment existingPsa : existingPermitionsAssign){
                        PermissionSetAssignment psaNew = new PermissionSetAssignment(AssigneeId = u.Id, PermissionSetId = psa.PermissionSetId);
                        if (!(existingPsa.PermissionSetId == psaNew.PermissionSetId)){
                            psaInsertList.add(psaNew);
                        }  
                    }
                }
            }
            if (!Test.isRunningTest() && PermissionSetAssignment.sObjectType.getDescribe().isCreateable()) insert psaInsertList;
        }
        PageReference pr;
        if(Desktop != null){
            //pr = new PageReference('/' + u.Id + '?noredirect=1');
            pr = new PageReference('/apex/ctu__CloneThisUserSuccess?old=' + toClone.Id + '&new=' + u.Id + '&pass=' + generatePassword + '&cloned=' + clonedData);
        }
        else{
            pr = new PageReference('/apex/ctu__CloneThisUserSuccess?old=' + toClone.Id + '&new=' + u.Id + '&pass=' + generatePassword + '&cloned=' + clonedData);
            //pr = new PageReference('/apex/ctu__CloneThisUserList?success=1');
        }
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference goBack(){
        String query = ApexPages.CurrentPage().getParameters().get('q');
        PageReference pr;
        if(query != null){
             if(Desktop != null){
                pr = new PageReference('/' + Id + '?noredirect=1');
            }
            else{
                pr = new PageReference('/apex/ctu__CloneThisUserList?q=' + query);
            }
            pr.setRedirect(true);
            return pr;
        }
         if(Desktop != null){
            pr = new PageReference('/' + Id + '?noredirect=1');
        }
        else{
            pr = new PageReference('/apex/ctu__CloneThisUserList');
        }
        pr.setRedirect(true);
        return pr;
    }
    
    private User cloneUser(User u){
        User userNew = new User();
        for(String f : fieldsToClone){
            if(fields.ContainsKey(f)){
                userNew.put(f, u.get(f));
            }
        }
        if (Test.isRunningTest()){
            Boolean hasUserNamePerm = Schema.sObjectType.User.fields.UserName.isCreateable();
            Boolean hasLastNamePerm = Schema.sObjectType.User.fields.LastName.isCreateable();
            Boolean hasEmailPerm = Schema.sObjectType.User.fields.Email.isCreateable();
            Boolean hasAliasPerm = Schema.sObjectType.User.fields.Alias.isCreateable();
            Boolean hasAllPerms = hasUserNamePerm && hasLastNamePerm && hasEmailPerm && hasAliasPerm;
            if (hasAllPerms){
                userNew.UserName = 'user@testman.com';
                userNew.LastName = 'Testman';
                userNew.Email = u.Email;
                userNew.Alias = 'Testman';
            }
        }
        return userNew;
    }
    
    public PageReference setErrorMessage(system.statusCode code){
        if(code == Statuscode.LICENSE_LIMIT_EXCEEDED){
            errorMsg = 'You do not have enough feature licenses to clone this user.';//'You don’t have enough ' + toClone.Profile.UserLicense.Name + ' licenses to clone this user';
        }
        else if(code == Statuscode.DUPLICATE_USERNAME){
            u.UserName.AddError('Duplicate Username');
            //errorMsg = 'Duplicate UserName';
        }
        else if(code == Statuscode.DUPLICATE_COMM_NICKNAME){
            u.CommunityNickname.AddError('Duplicate Nickname');
            //errorMsg =  'Duplicate Nickname';
        }
        else if(code == Statuscode.FIELD_CUSTOM_VALIDATION_EXCEPTION){
            errorMsg =  'Custom Field Validation Failed: You cannot clone this user';
        }
        else{
            system.debug('#### ' + code);
        }
        return null;
        
    }

}