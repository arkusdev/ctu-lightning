public class SendSingleEmail {

    public static void send(String subject, String body){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String userEmail = UserInfo.getUserEmail();
        String[] toAddress = new String[] {userEmail};
        email.setToAddresses(toAddress);
        email.setSubject(subject);
        email.setPlainTextBody(body);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
    }
}