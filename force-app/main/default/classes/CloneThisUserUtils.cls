public without sharing class CloneThisUserUtils {
    
    private static final Map<String, Schema.SObjectField> PROFILE_FIELDS = Profile.sObjectType.getDescribe().fields.getMap();
    private static final Map<String, Schema.SObjectField> PERMISSION_SET_FIELDS = PermissionSet.sObjectType.getDescribe().fields.getMap();
    

    @AuraEnabled
    public static Map<String, PermissionInfo> getUserPermissions(){
        Set<String> profileFields = new Set<String>{'PermissionsAssignPermissionSets', 'PermissionsModifyAllData', 'PermissionsEditPublicFilters',
            'PermissionsManageUsers', 'PermissionsResetPasswords', 'PermissionsCustomizeApplication',
            'PermissionsManageReportsInPubFolders', 'PermissionsManageDashbdsInPubFolders'
        };

        Set<String> permissionSetFields = new Set<String>{'PermissionsAssignPermissionSets', 'PermissionsModifyAllData', 'PermissionsEditPublicFilters',
            'PermissionsManageUsers', 'PermissionsResetPasswords', 'PermissionsCustomizeApplication',
            'PermissionsManageReportsInPubFolders', 'PermissionsManageDashbdsInPubFolders'
        };
        
        Map<String, PermissionInfo> userPermissions = new Map<String, PermissionInfo>();
        Id userProfileId = [SELECT ProfileId FROM User WHERE Id =: UserInfo.getUserId()].ProfileId;
        List<ctu__CTU_Settings__c> ctu_settings = [SELECT ctu__Do_not_Check_Manage_Users_Permission__c, ctu__Do_not_Check_Modify_All_Data_Permission__c,
                                                            ctu__Do_not_Check_Assign_PS_Permission__c, ctu__Do_not_Check_Reset_Password_Permission__c,
                                                            ctu__Do_not_Check_Queue_Permission__c, ctu__Non_Sys_Admin_Can_View_Quick_Links__c
                                                    FROM ctu__CTU_Settings__c LIMIT 1];

        // Check if chatter is enabled
        Boolean isChatterEnabled = chatterEnabled();

        // To Assign Permission Sets
        Boolean canAssignPermissionsAssignPermissionSets = !ctu_settings.isEmpty() ? ctu_settings[0].ctu__Do_not_Check_Assign_PS_Permission__c : false;

        // To Assign Permission Set Licenses and Create Public Groups
        Boolean canAssignPermissionsManageUsers = ctu_settings.size() > 0 ? ctu_settings[0].ctu__Do_not_Check_Manage_Users_Permission__c : false;

        // To Create or change queues
        Boolean canManageQueues = ctu_settings.size() > 0 ? ctu_settings[0].ctu__Do_not_Check_Queue_Permission__c : false;

        // To Generate User Password and notify Users
        Boolean canAssignPermissionsResetPasswords = !ctu_settings.isEmpty() ? ctu_settings[0].ctu__Do_not_Check_Reset_Password_Permission__c : false;

        // To Create Chatter Groups
        Boolean canCloneChatterGroups = !ctu_settings.isEmpty() ? ctu_settings[0].ctu__Do_not_Check_Modify_All_Data_Permission__c : false;

        //Permission Modify All Data
        Boolean hasModifyAllData = !ctu_settings.isEmpty() ? ctu_settings[0].ctu__Do_not_Check_Modify_All_Data_Permission__c : false;

        Boolean canManageReports = false;

        Boolean canManageDashboards = false;

        //Get the profiles permissions as default 
        String profileEnabledFields = getEnabledFieldsForQuery('Profile', profileFields);
        String profileQuery = String.escapeSingleQuotes('SELECT Id, ' + profileEnabledFields + ' FROM Profile WHERE Id = :userProfileId');
        Profile userProfile = Database.query(profileQuery);

        //Check against the permission sets
        List<PermissionSetAssignment> userPermissionsAssigned = [SELECT Id, PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId =: UserInfo.getUserId()];
        List<Id> userPermissionsAssignedIds = new List<Id>();

        for (PermissionSetAssignment p : userPermissionsAssigned){
            userPermissionsAssignedIds.add(p.PermissionSetId);
        }

        String permissionSetEnabledFields = getEnabledFieldsForQuery('PermissionSet', permissionSetFields);
        String permissionSetQuery = String.escapeSingleQuotes('SELECT ' + permissionSetEnabledFields + ' FROM PermissionSet  WHERE ProfileId =: userProfileId OR Id IN: userPermissionsAssignedIds');
        for(PermissionSet permission : Database.query(permissionSetQuery)){
            //To Assign Permission Set: PermissionsAssignPermissionSets is needed
            if(getFieldValue(PERMISSION_SET_FIELDS, permission, 'PermissionsAssignPermissionSets')){
                canAssignPermissionsAssignPermissionSets = true;
            }

            //To assign Permission Set License and Public Groups: PermissionsManageUsers is needed
            if(getFieldValue(PERMISSION_SET_FIELDS, permission, 'PermissionsManageUsers')){
                canAssignPermissionsManageUsers = true;
            }

            //To create or change queues: PermissionsEditPublicFilters, PermissionsManageUsers and PermissionsCustomizeApplication are needed
            if(getFieldValue(PERMISSION_SET_FIELDS, permission, 'PermissionsEditPublicFilters')
                && getFieldValue(PERMISSION_SET_FIELDS, permission, 'PermissionsManageUsers')
                && getFieldValue(PERMISSION_SET_FIELDS, permission, 'PermissionsCustomizeApplication')){
                canManageQueues = true;
            }

            //To Reset Password: PermissionsResetPasswords is needed
            if(getFieldValue(PERMISSION_SET_FIELDS, permission, 'PermissionsResetPasswords')){
                canAssignPermissionsResetPasswords = true;
            }

            //To clone chatter Groups, PermissionsModifyAllData is needed
            if(getFieldValue(PERMISSION_SET_FIELDS, permission, 'PermissionsModifyAllData')){
                canCloneChatterGroups = true;
                hasModifyAllData = true;
            }

            if (getFieldValue(PERMISSION_SET_FIELDS, permission, 'PermissionsManageReportsInPubFolders')){
                canManageReports = true;
            }
            if (getFieldValue(PERMISSION_SET_FIELDS, permission, 'PermissionsManageDashbdsInPubFolders')){
                canManageDashboards = true;
            }
        }

        Boolean canViewQuickLinksBar = ctu_settings.size() > 0 ? ctu_settings[0].ctu__Non_Sys_Admin_Can_View_Quick_Links__c : false;

        Boolean canAssignPermissionSets = canAssignPermissionsAssignPermissionSets || getFieldValue(PROFILE_FIELDS, userProfile, 'PermissionsAssignPermissionSets');
        Boolean canAssignPermissionSetLicense = canAssignPermissionsManageUsers || getFieldValue(PROFILE_FIELDS, userProfile, 'PermissionsManageUsers');
        Boolean canManagePublicGroups = canAssignPermissionsManageUsers || getFieldValue(PROFILE_FIELDS, userProfile, 'PermissionsManageUsers');
        Boolean canCreateAndChangeQueues = canManageQueues || (getFieldValue(PROFILE_FIELDS, userProfile, 'PermissionsEditPublicFilters') && getFieldValue(PROFILE_FIELDS, userProfile, 'PermissionsManageUsers') && getFieldValue(PROFILE_FIELDS, userProfile, 'PermissionsCustomizeApplication'));
        Boolean canResetPasswords = canAssignPermissionsResetPasswords || getFieldValue(PROFILE_FIELDS, userProfile, 'PermissionsResetPasswords');
        canManageReports = canManageReports || getFieldValue(PROFILE_FIELDS, userProfile, 'PermissionsManageReportsInPubFolders');
        canManageDashboards = canManageDashboards || getFieldValue(PROFILE_FIELDS, userProfile, 'PermissionsManageDashbdsInPubFolders');

        // Chatter Groups first need to check if chatter is enabled
        Boolean canChatterGroups = isChatterEnabled && (canCloneChatterGroups || userProfile.PermissionsModifyAllData);
        
        Boolean canAccountTeamMembership = (Boolean)Schema.getGlobalDescribe().containsKey('UserAccountTeamMember');
        Boolean canAccountOwnership = getObjectPermissions('Account', 'PermissionsEdit', userProfileId);
        Boolean canLeadOwnership = getObjectPermissions('Lead', 'PermissionsEdit', userProfileId);
        Boolean canOpportunityOwnership = getObjectPermissions('Opportunity', 'PermissionsEdit', userProfileId);
        Boolean canOpportunityTeamMembership = (Boolean)Schema.getGlobalDescribe().containsKey('UserTeamMember');
        Boolean canCaseOwnership = getObjectPermissions('Case', 'PermissionsEdit', userProfileId);
        Boolean canCaseTeamMembership = canCaseOwnership;

        PermissionInfo assignPermissionSetsInfo = new PermissionInfo(canAssignPermissionSets, 'The current user needs to have the permission \'Assign Permission Sets\'');
        PermissionInfo assignPermissionSetLicenseInfo = new PermissionInfo(canAssignPermissionSetLicense, 'The current user needs to have the permission \'Manage Users\'');
        PermissionInfo managePublicGroupInfo = new PermissionInfo(canManagePublicGroups, 'The current user needs to have the permission \'Manage Users\'');
        PermissionInfo createChangeQueuesInfo = new PermissionInfo(canCreateAndChangeQueues, 'The current user needs to have the permission \'PermissionsEditPublicFilters\', \'PermissionsManageUsers\' and \'PermissionsCustomizeApplication\'');
        PermissionInfo resetPasswordsInfo = new PermissionInfo(canResetPasswords, 'The current user needs to have the permission \'Reset User Passwords and Unlock Users\'');
        PermissionInfo modifyAllData = new PermissionInfo(hasModifyAllData, 'Chatter must be enabled and the current user needs to have the permission \'Modify all Data\'');
        PermissionInfo chatterInfo = new PermissionInfo(canChatterGroups, 'Chatter must be enabled and the current user needs to have the permission \'Modify all Data\'');
        PermissionInfo viewQuickLinksBar = new PermissionInfo(canViewQuickLinksBar, '');
        PermissionInfo manageReports = new PermissionInfo(canManageReports, 'The current user needs to have the permission Manage Reports in Public Folders');
        PermissionInfo manageDashboards = new PermissionInfo(canManageDashboards, 'The current user needs to have the permission Manage Dashboards in Public Folders');
        
        PermissionInfo accountTeamMembership = new PermissionInfo(canAccountTeamMembership, 'The current user needs to enable the \'Account Team Memberships\'');
        PermissionInfo accountOwnership = new PermissionInfo(canAccountOwnership, 'The current user needs to have the Edit permission on \'Account\' Object');
        PermissionInfo leadOwnership = new PermissionInfo(canLeadOwnership, 'The current user needs to have the Edit permission on \'Lead\' Object');
        PermissionInfo opportunityOwnership = new PermissionInfo(canOpportunityOwnership, 'The current user needs to have the Edit permission on \'Opportunity\' Object');
        PermissionInfo opportunityTeamMembership = new PermissionInfo(canOpportunityTeamMembership, 'The current user needs to enable the \'Opportunity Team Memberships\'');
        PermissionInfo caseOwnership = new PermissionInfo(canCaseOwnership, 'The current user needs to have the Edit permission on \'Case\' Object');
        PermissionInfo caseTeamMembership = new PermissionInfo(canCaseTeamMembership, 'The current user needs to have the Edit permission on \'Case\' Object');
        

        userPermissions.put('PermissionsAssignPermissionSets', assignPermissionSetsInfo);
        userPermissions.put('PermissionsAssignPermissionSetLicenses', assignPermissionSetLicenseInfo);
        userPermissions.put('PermissionsManagePublicGroup', managePublicGroupInfo);
        userPermissions.put('PermissionsQueues', createChangeQueuesInfo);
        userPermissions.put('PermissionsResetPasswords', resetPasswordsInfo);
        userPermissions.put('PermissionChatterGroups', chatterInfo);
        userPermissions.put('CanViewQuickLinksBar', viewQuickLinksBar);

        userPermissions.put('AccountTeamMembership', accountTeamMembership);
        userPermissions.put('AccountOwnership', accountOwnership);
        userPermissions.put('LeadOwnership', leadOwnership);
        userPermissions.put('OpportunityOwnership', opportunityOwnership);
        userPermissions.put('OpportunityTeamMembership', opportunityTeamMembership);
        userPermissions.put('CaseOwnership', caseOwnership);
        userPermissions.put('CaseTeamMembership', caseTeamMembership);

        //Previously used
        userPermissions.put('PermissionsManageUsers', assignPermissionSetLicenseInfo); //assignPermissionSetLicenseInfo require Manage Users permission
        userPermissions.put('PermissionsModifyAllData', modifyAllData);
        userPermissions.put('PermissionsManageReports', manageReports);
        userPermissions.put('PermissionsManageDashboards', manageDashboards);

        return userPermissions;
    }

    public static Boolean getObjectPermissions(String record, String permission, String userProfileId) {
        String query= String.escapeSingleQuotes('SELECT sObjectType, PermissionsCreate, PermissionsRead, PermissionsEdit, PermissionsDelete, PermissionsModifyAllRecords, PermissionsViewAllRecords FROM ObjectPermissions WHERE sObjectType=: record AND Parent.ProfileId=: userProfileId LIMIT 1');
        ObjectPermissions dSoql= Database.query(query);
        return (Boolean)dSoql.get(permission);
    }

    private static Boolean getFieldValue(Map<String, Schema.SObjectField> fields, SObject record, String fieldName) {
        return fields.containsKey(fieldName) && (Boolean)record.get(fieldName);
    }
    private static String getEnabledFieldsForQuery(String objectName, Set<String> fields) {
        String enabledFieldsQuery = '';
        Set<String> fieldsToCheck = objectName == 'Profile' ? PROFILE_FIELDS.keySet() : PERMISSION_SET_FIELDS.keySet();
        for (String objectField : fields) {
            if (fieldsToCheck.contains(objectField.toLowerCase())){
                enabledFieldsQuery += objectField + ',';
            }
        }
        return enabledFieldsQuery.removeEnd(',');
    }

    @AuraEnabled
    public static String goToLink(String type){
        String url = '';
        switch on type {
            when 'Users' {
                url = setupURL('ManageUsers');
            }
            when 'Profiles' {
                url = setupURL('EnhancedProfiles');
            }
            when 'Roles' {
                url = setupURL('Roles');
            }
            when 'Public_Groups' {
                url = setupURL('PublicGroups');
            }
            when 'Queues' {
                url = setupURL('Queues');
            }
            when 'Permission_Sets' {
                url = setupURL('PermSets');
            }
            when 'CTU' {
                url = ' https://appexchange.salesforce.com/appxListingDetail?listingId=a0N3000000B5jTkEAJ';
            }
        }
        return url;
    }

    @AuraEnabled
    public static PackageInfo getPackageVersion(){
        Version ver = System.requestVersion();
        return new PackageInfo(ver.major() + '.' + ver.minor(), System.Today().year());
    }

    private static String setupURL(String setting){
        return '/lightning/setup/' + setting + '/home';
    }

    public static Boolean chatterEnabled(){
        Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe(); 
        return !Test.isRunningTest() && ConnectApi.Organization.getSettings().features.chatter && System.Type.forName('Schema.CollaborationGroupMember') != null && globalDescribe.containsKey('FeedItem');
    }

    public static List<String> getCommunityUserLicenses() {
        return new List<String>{
            'External Apps', 'High Volume Customer Portal', 'Service Cloud Portal',
            'Authenticated Sites Portal', 'Customer Community', 'Customer Community Plus',
            'Customer Community Login', 'Customer Community Plus Login', 'Partner Community Login',
            'Customer Portal — Enterprise Administration', 'Customer Portal Manager Standard',
            'External Apps Login', 'Channel Account Login', 'Gold Partner', 'Silver Partner',
            'Customer Portal Manager Custom', 'Partner Community', 'Gold Partner', 'Channel Account'
        };
    }

    public static void insertSObjectThroughApi(List<SObject> listRecords, String objName) {
        String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm();
        String restAPIURL = sfdcURL + '/services/data/v53.0/composite/tree/' + objName; 
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId()); 
        httpRequest.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
        httpRequest.setHeader('Content-Type', 'application/json');
        httpRequest.setMethod('POST');
        httpRequest.setEndpoint(restAPIURL); 
        String response = '';
        try { 
            Http http = new Http();  
            //Creating Json Object
            Integer count = 0;
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();     
            gen.writeFieldName('records');
            gen.writeStartArray();
            for(SObject objRecord : listRecords){
                count++;
                gen.writeStartObject();
                // Attributes
                gen.writeFieldName('attributes');
                gen.writeStartObject();
                gen.writeStringField('type', objName);
                gen.writeStringField('referenceId', 'ref' + count);
                gen.writeEndObject();
                // Values
                Map<String, Object> values = objRecord.getPopulatedFieldsAsMap();
                for(String fieldName: values.keySet()) {
                    Map<String, Schema.SObjectField> mapFields = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
                    if(fieldName != 'Id' &&  mapFields.get(fieldName).getDescribe().isCreateable()){
                        gen.writeStringField(fieldName, String.valueOf(values.get(fieldName)));
                    }
                }
                gen.writeEndObject();
            }
            gen.writeEndArray();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            httpRequest.setBody(jsonData);
            HttpResponse httpResponse = http.send(httpRequest); 
            if (httpResponse.getStatusCode() == 201 ) { 
                response = JSON.serializePretty( JSON.deserializeUntyped(httpResponse.getBody()) ); 
            } else { 
                throw new CalloutException( httpResponse.getBody() ); 
            }
        } catch( System.Exception e) { 
            throw e; 
        } 
    }

    public static void deleteSObjectThroughApi(List<String> idsList) {
        if(!idsList.isEmpty()) {
            String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm();
            String restAPIURL = sfdcURL + '/services/data/v53.0/composite/sobjects?ids=' + String.join(idsList, ','); 
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId()); 
            httpRequest.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
            httpRequest.setMethod('DELETE');
            httpRequest.setEndpoint(restAPIURL); 
            String response = '';
            try { 
                Http http = new Http();
                HttpResponse httpResponse = http.send(httpRequest); 
                if (httpResponse.getStatusCode() == 200 ) { 
                    response = JSON.serializePretty( JSON.deserializeUntyped(httpResponse.getBody()) ); 
                } else { 
                    throw new CalloutException( httpResponse.getBody() ); 
                }  
            } catch( System.Exception e) {
                throw e; 
            } 
        }
    }

    public static List<SObject> createDynamicObjects(String sobjType, String keyField, List<String> fields, String userId, List<SObject> records){
        List<SObject> clonedRecords = new List<SObject>();
        for(SObject record: records){
            SObject recordCloned= Schema.getGlobalDescribe().get(sobjType).newSObject();
            for(String field: fields){
                recordCloned.put(field, (field == keyField ? userId : record.get(field)));    
            }
            clonedRecords.add(recordCloned);
        }
        return clonedRecords;
    }

    public class PermissionInfo{
        @AuraEnabled public Boolean HasPermission {set; get;}
        @AuraEnabled public String Message {set; get;}

        public PermissionInfo(Boolean somePermission, String someMessage){
            HasPermission = somePermission;
            Message = someMessage;
        }
    }

    public class PackageInfo {
        @AuraEnabled public String packageVersion { set; get; }
        @AuraEnabled public Integer packageYear { set; get; }
        
        public PackageInfo(String pPackageVersion, Integer pPackageYear) {
            this.packageVersion = pPackageVersion;
            this.packageYear = pPackageYear;
        }
    }
}