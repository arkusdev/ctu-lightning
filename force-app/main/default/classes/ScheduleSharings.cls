public without sharing class ScheduleSharings implements Schedulable, Database.AllowsCallouts {
    
    private Id oldUserId { set; get; }
    private Id newUserId { set; get; }
    private Boolean shareReports { set; get; }
    private Boolean shareDashboards { set; get; }
    private Boolean deleteReports { set; get; }
    private Boolean deleteDashboards { set; get; }

    private static final String endpoint = URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v49.0/folders';

    public ScheduleSharings(Id pOldUserId, Id pNewUserId, Boolean pShareReports, Boolean pShareDashboards, Boolean pDeleteReports, Boolean pDeleteDashboards){
        this.oldUserId = pOldUserId;
        this.newUserId = pNewUserId;
        this.shareReports = pShareReports;
        this.shareDashboards = pShareDashboards;
        this.deleteReports = pDeleteReports;
        this.deleteDashboards = pDeleteDashboards;
    }

    public void execute(SchedulableContext sc) {
        shareReports(this.oldUserId, this.newUserId, this.shareReports, this.shareDashboards, this.deleteReports, this.deleteDashboards, sc.getTriggerId());
    }

    private static void shareReports(Id oldUserId, Id newUserId, Boolean shareReports, Boolean shareDashboards, Boolean deleteReports, Boolean deleteDashboards, String schedulableId){
        String sessionId = UserInfo.getSessionId();
        Database.executeBatch(new BatchGetFoldersShare(oldUserId, newUserId, endpoint, sessionId, shareReports, shareDashboards, deleteReports, deleteDashboards, schedulableId), 10);
    }

    public class FoldersInfo {
        public List<FolderInfo> folders { set; get; }
        public Integer totalSize { set; get; }
        public String url { set; get; }
        public String nextPageUrl { set; get; }
    }

    public class FolderInfo{
        public String id { set; get; }
        public String type { set; get; }
        public String label { set; get; }
        public String namespace { set; get; }
        public String url { set; get; }
        public String name { set; get; }
    }

    public class FoldersShareInfo {
        public List<FolderShareInfo> shares { set; get; }
    }

    public class FolderShareInfo {
        public String folderId { set; get; }
        public String sharedWithId { set; get; }
        public String accessType { set; get; }
        public String imageColor { set; get; }
        public String imageUrl { set; get; }
        public String shareId { set; get; }
        public String shareType { set; get; }
        public String sharedWithLabel { set; get; }
        public String url { set; get; }
    }

    public class FolderSharesBody{
        public List<FolderShareBody> shares { set; get; }
    }

    public class FolderShareBody{
        public String accessType { set; get; }
        public String shareWithId { set; get; }
        public String shareType { set; get; }
    }
}