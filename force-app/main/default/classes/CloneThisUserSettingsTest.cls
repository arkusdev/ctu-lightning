@isTest
public class CloneThisUserSettingsTest {
    
    @isTest
    public static void testGetData(){
        ctu__CTU_Custom_Field_Ignored__c cfi = new ctu__CTU_Custom_Field_Ignored__c(ctu__API_Name__c = 'Name');
        insert cfi;
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(ProfileId=p.Id,LastName='aa',FirstName='new 1',Username='fefe@dfd.com',email='example@aa.com',alias='fefe',LocaleSidKey='en_US',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',TimeZoneSidKey='America/New_York');
        insert u;
        CloneThisUserSettings.cmpdata result;
        System.runAs(u){
            Test.startTest();
            result = CloneThisUserSettings.getData();
            Test.stopTest();            
        }
        System.assertEquals(true, result.isSystemAdmin);
    }

    @IsTest
    static void getStandardDataTest(){
        User u = new User(ProfileId=[SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id, 
                            LastName='aa',FirstName='new 1',Username='fefe@dfd.com',email='example@aa.com',
                            alias='fefe',LocaleSidKey='en_US',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',
                            TimeZoneSidKey='America/New_York');
        insert u;
        Test.startTest();
        System.runAs(u){
            CloneThisUserSettings.cmpdata data = CloneThisUserSettings.getStandardData();
            System.assertEquals(true, data.isSystemAdmin);
        }
        Test.stopTest();
    }

    @isTest
    public static void testUpdateData(){
        
        ctu__CTU_Custom_Field_Ignored__c cfi = new ctu__CTU_Custom_Field_Ignored__c(ctu__API_Name__c = 'Name');
        insert cfi;

        List<String> data = new List<String>();
        data.add('Name');

        Test.startTest();
        String result = CloneThisUserSettings.updateData(data, new List<ctu__Field_Default_Value_Obj__c>());
        Test.stopTest();

        system.assertEquals('Success', result);
    }

    @isTest
    public static void testIsSystemAdmin(){
        Test.startTest();
        Boolean result = CloneThisUserSettings.isSystemAdmin();
        Test.stopTest();

        system.assertEquals(true, result);
    }

    @isTest
    public static void testNotUpdateManageUsers(){
        String result = CloneThisUserSettings.updateManageUsers(false, false, false, false, false);
        System.assertEquals('Success', result);
        List<ctu__CTU_Settings__c> ctu_settings = [SELECT ctu__Do_not_Check_Manage_Users_Permission__c FROM ctu__CTU_Settings__c LIMIT 1];
        System.assertEquals(true, ctu_settings.size() > 0 && ctu_settings[0].ctu__Do_not_Check_Manage_Users_Permission__c);
    }

    @isTest
    public static void testUpdateManageUsers(){
        String result = CloneThisUserSettings.updateManageUsers(true, true, true, true, true);
        System.assertEquals('Success', result);
        List<ctu__CTU_Settings__c> ctu_settings = [SELECT ctu__Do_not_Check_Manage_Users_Permission__c FROM ctu__CTU_Settings__c LIMIT 1];
        System.assertEquals(false, ctu_settings.size() > 0 && ctu_settings[0].ctu__Do_not_Check_Manage_Users_Permission__c);
    }

    @IsTest
    static void goToLinkTest(){
        
        Test.startTest();
        String linkUsers = CloneThisUserUtils.goToLink('Users');
        String linkProfiles = CloneThisUserUtils.goToLink('Profiles');
        String linkRoles = CloneThisUserUtils.goToLink('Roles');
        String linkPublicGroups = CloneThisUserUtils.goToLink('Public_Groups');
        String linkQueues = CloneThisUserUtils.goToLink('Queues');
        String linkPs = CloneThisUserUtils.goToLink('Permission_Sets');
        String linkCtu = CloneThisUserUtils.goToLink('CTU');

        System.assertEquals('/lightning/setup/ManageUsers/home', linkUsers);
        System.assertEquals('/lightning/setup/EnhancedProfiles/home', linkProfiles);
        System.assertEquals('/lightning/setup/Roles/home', linkRoles);
        System.assertEquals('/lightning/setup/PublicGroups/home', linkPublicGroups);
        System.assertEquals('/lightning/setup/Queues/home', linkQueues);
        System.assertEquals('/lightning/setup/PermSets/home', linkPs);
        System.assertEquals(' https://appexchange.salesforce.com/appxListingDetail?listingId=a0N3000000B5jTkEAJ', linkCtu);
        Test.stopTest();
        
    }

    @isTest
    static void getVersionTest(){
        CloneThisUserUtils.PackageInfo version = CloneThisUserUtils.getPackageVersion();
        System.assert(version != null);
    }

    @isTest
    static void getObjectsTest() {
        User newUser = new User(
            ProfileId = [Select id From Profile Where Name = 'System Administrator'].Id,
            LastName = 'LastNameTest',
            Email = 'userBatch@emailTest.com',
            Username = 'userBatch@emailTest.com',
            CompanyName = 'Company Test',
            Title = 'Title Test',
            Alias = 'aTest',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        insert newUser;

        Id [] fixedSearchResults = new Id[]{newUser.Id};
        Test.setFixedSearchResults(fixedSearchResults);

        String searchParam = 'Test';
        String objectName = 'User';
        List<sObject> users = CloneThisUserSettings.getObjects(searchParam, objectName);


        System.assertEquals(1, users.size(), 'Only one user should match');
        System.assertEquals('LastNameTest', users[0].get('Name'), 'Incorrect user found');
    }

    @isTest
    static void updateViewQuickLinksTest() {
        CloneThisUserSettings.updateViewQuickLinks(true);
        System.assertEquals(1, [SELECT ctu__Non_Sys_Admin_Can_View_Quick_Links__c FROM ctu__CTU_Settings__c].size(), 'No custom setting added');
    }

    @isTest
    static void getGeneralOptionsDataTest() {
        List<ctu__General_Option_Value_Obj__c> setting = new List<ctu__General_Option_Value_Obj__c>{
            new ctu__General_Option_Value_Obj__c(Name='Generate new password', ctu__Data_Label__c='Generate new password and notify user immediately', ctu__Data_Category__c='General', ctu__Default_Value__c= false),
            // Advanced Options
            new ctu__General_Option_Value_Obj__c(Name='Advanced Chatter Group Memberships', ctu__Data_Label__c='Chatter Group Memberships', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Shared Dashboards', ctu__Data_Label__c='Shared Dashboards', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Permission Set Assignments', ctu__Data_Label__c='Permission Set Assignments', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Shared Reports', ctu__Data_Label__c='Shared Reports', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Queue Memberships', ctu__Data_Label__c='Queue Memberships', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Public Group Memberships', ctu__Data_Label__c='Public Group Memberships', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Permission Set License', ctu__Data_Label__c='Permission Set License Assignments', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Account Team Membership', ctu__Data_Label__c='Account Team Membership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Account Ownership', ctu__Data_Label__c='Account Ownership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Lead Ownership', ctu__Data_Label__c='Lead Ownership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Opportunity Ownership', ctu__Data_Label__c='Opportunity Ownership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Opportunity Team Membership', ctu__Data_Label__c='Opportunity Team Membership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Case Ownership', ctu__Data_Label__c='Case Ownership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Case Team Membership', ctu__Data_Label__c='Case Team Membership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            // Remove Options
            new ctu__General_Option_Value_Obj__c(Name='Deactivate Original User', ctu__Data_Label__c='Deactivate Original User', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Chatter Group Memberships', ctu__Data_Label__c='Chatter Group Memberships', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Shared Dashboards', ctu__Data_Label__c='Shared Dashboards', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Permission Set Assignments', ctu__Data_Label__c='Permission Set Assignments', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Shared Reports', ctu__Data_Label__c='Shared Reports', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Queue Memberships', ctu__Data_Label__c='Queue Memberships', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Public Group Memberships', ctu__Data_Label__c='Public Group Memberships', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Permission Set License', ctu__Data_Label__c='Permission Set License Assignments', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Account Team Membership', ctu__Data_Label__c='Account Team Membership', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Opportunity Team Membership', ctu__Data_Label__c='Opportunity Team Membership', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Case Team Membership', ctu__Data_Label__c='Case Team Membership', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false)
        };
        insert setting;

        Test.startTest();
        List<ctu__General_Option_Value_Obj__c> data= CloneThisUserSettings.getGeneralOptionsData();
        Test.stopTest();

        List<ctu__General_Option_Value_Obj__c> result= [Select Id, Name From ctu__General_Option_Value_Obj__c];

        System.assertEquals(data.size(), result.size(), 'It should be the same size');
    }

    @isTest
    static void saveAdvancedOptionsTest() {
        Boolean res= true;
        List<ctu__General_Option_Value_Obj__c> setting= new List<ctu__General_Option_Value_Obj__c>();
        for (Integer i = 0; i < 10; i++) {
            ctu__General_Option_Value_Obj__c record= new ctu__General_Option_Value_Obj__c();
            record.Name= 'Name ' + i;
            record.ctu__Data_Category__c= 'Category ' + i;
            record.ctu__Data_Label__c= 'Label ' + i;
            record.ctu__Default_Value__c= (math.mod(i, 2) == 0? true: false);
            setting.add(record);
        }
        insert setting;

        Test.startTest();
        try {
            List<ctu__General_Option_Value_Obj__c> data= [Select Id, Name, ctu__Default_Value__c From ctu__General_Option_Value_Obj__c Order By Name Asc Limit 1];
            data[0].ctu__Default_Value__c= true;
            CloneThisUserSettings.saveAdvancedOptions(data);
        } catch (Exception ex) {
            res= false;
        }
        Test.stopTest();

        List<ctu__General_Option_Value_Obj__c> result= [Select Id, Name, ctu__Default_Value__c From ctu__General_Option_Value_Obj__c Order By Name Asc Limit 1];
        
        System.assertEquals(true, result[0].ctu__Default_Value__c, 'It should be the same value for the field');
    }
}