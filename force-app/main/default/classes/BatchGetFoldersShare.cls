public with sharing class BatchGetFoldersShare implements Database.Batchable<ScheduleSharings.FolderInfo>, Database.AllowsCallouts, Database.Stateful {
    private Id oldUserId { set; get; }
    private Id newUserId { set; get; }
    private Boolean shareReports { set; get; }
    private Boolean shareDashboards { set; get; }
    private Boolean deleteReports { set; get; }
    private Boolean deleteDashboards { set; get; }
    private List<ScheduleSharings.FolderShareInfo> folderShares { set; get; }
    private Map<Id, ScheduleSharings.FolderSharesBody> newSharings { set; get; }
    private String endpoint { set; get; }
    private String sessionId { set; get; }
    private String schedulableId { set; get; }

    public BatchGetFoldersShare(Id pOldUserId, Id pNewUserId, String pEndpoint, String sessionId, Boolean shareReports, Boolean shareDashboards, Boolean deleteReports, Boolean deleteDashboards, String pSchedulableId){
        this.oldUserId = pOldUserId;
        this.newUserId = pNewUserId;
        this.endpoint = pEndpoint;
        this.folderShares = new List<ScheduleSharings.FolderShareInfo>();
        this.sessionId = sessionId;
        this.shareReports = shareReports;
        this.shareDashboards = shareDashboards;
        this.deleteReports = deleteReports;
        this.deleteDashboards = deleteDashboards;
        this.schedulableId = pSchedulableId;
        this.newSharings = new Map<Id, ScheduleSharings.FolderSharesBody>();
    }

    public List<ScheduleSharings.FolderInfo> start(Database.BatchableContext bc){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        request.setEndpoint(this.endpoint + '?page=1&pageSize=300');
        request.setMethod('GET');
        request.setHeader('Authorization', 'Bearer ' + this.sessionId);
        List<ScheduleSharings.FolderInfo> folders = new List<ScheduleSharings.FolderInfo>();
        try{
            HttpResponse response = http.send(request);
            ScheduleSharings.FoldersInfo foldersInfo = (ScheduleSharings.FoldersInfo)JSON.deserializeStrict(response.getBody(), ScheduleSharings.FoldersInfo.class);

            for (ScheduleSharings.FolderInfo folder : foldersInfo.folders) {
                if (folder.type == 'report' && shareReports)
                {
                    folders.add(folder);
                }

                if (folder.type == 'dashboard' && shareDashboards)
                {
                    folders.add(folder);
                }
            }
            return folders;
        }catch(Exception ex){
            System.debug(ex.getMessage());
            return folders;
        }
    }

    public void execute(Database.BatchableContext bc, List<ScheduleSharings.FolderInfo> folders){
        for (ScheduleSharings.FolderInfo folder : folders) {
            Boolean addFolder = false;
            ScheduleSharings.FolderSharesBody newFolderShares = new ScheduleSharings.FolderSharesBody();
            newFolderShares.shares = new List<ScheduleSharings.FolderShareBody>();

            Http http = new Http();
            HttpRequest request = new HttpRequest();
            String endPoint = this.endpoint + '/' + folder.id + '/shares';
            request.setEndpoint(endPoint);
            request.setMethod('GET');
            request.setHeader('Authorization', 'Bearer ' + this.sessionId);
            HttpResponse response = http.send(request);
            
            ScheduleSharings.FoldersShareInfo foldersShare = (ScheduleSharings.FoldersShareInfo)JSON.deserializeStrict(response.getBody(), ScheduleSharings.FoldersShareInfo.class);

            for (ScheduleSharings.FolderShareInfo folderShare : foldersShare.shares) {
                ScheduleSharings.FolderShareBody newFolderSharing = new ScheduleSharings.FolderShareBody();
                newFolderSharing.accessType = folderShare.accessType;
                newFolderSharing.shareType = folderShare.shareType;

                if (folderShare.sharedWithId == oldUserId){
                    addFolder = true;

                    newFolderSharing.shareWithId = newUserId;
                    if ((!this.deleteReports && folder.type == 'report') || (!this.deleteDashboards && folder.type == 'dashboard'))
                    {
                        ScheduleSharings.FolderShareBody originalFolderSharing = new ScheduleSharings.FolderShareBody();
                        originalFolderSharing.accessType = folderShare.accessType;
                        originalFolderSharing.shareType = folderShare.shareType;
                        originalFolderSharing.shareWithId = folderShare.sharedWithId;
                        newFolderShares.shares.add(originalFolderSharing);
                    }
                } else {
                    newFolderSharing.shareWithId = folderShare.sharedWithId;
                }
                newFolderShares.shares.add(newFolderSharing);
                
            }
            if (addFolder){
                newSharings.put(folder.id, newFolderShares);
            }
        }
    }

    public void finish(DataBase.BatchableContext bc){
        Database.executeBatch(new BatchShareFolders(this.newSharings, this.endpoint, this.sessionId, this.schedulableId), 100);
    }
}