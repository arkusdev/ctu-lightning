@isTest
public with sharing class CalloutMock implements HttpCalloutMock{
    
    private String userId { set; get; }

    public CalloutMock(String pUserId){
        this.userId = pUserId;
    }

    public HttpResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setStatus('OK');
        res.setStatusCode(200);

        String body = '';

        if (req.getEndpoint().endsWith('?page=1&pageSize=300')){
            body = getFolders();
        }

        if (req.getEndpoint().endsWith('/shares')){
            if (req.getMethod() == 'GET'){
                body = getShares();
            }
        }

        res.setBody(body);
        return res;
    }

    private String getFolders(){
        String response = '{';
        response += '"folders" : [{"id" : "100000000000ABC", "type": "report", "label": "some folder", "namespace": "", "url": "test.com", "name": "Folder test"}],';
        response += '"totalSize": 1,';
        response += '"url": "test.com",';
        response += '"nextPageUrl": "somepage.com"}';
        return response;
    }

    private String getShares(){
        String response = '{';
        response += '"shares" : [{"folderId" : "100000000000ABC", "sharedWithId": "' + this.userId + '",';
        response += '"accessType": "Read",';
        response += '"imageColor": "",';
        response += '"shareType": "Read"}]}';
        return response;
    }
}