public  class cloneUserlightning {

    public static User u = new User();
    public static User toClone{get;set;}
    public static Boolean generatePassword{get;set;}
    public static Boolean PermissionSetAssignments{get;set;}
    public static Boolean QueueMembership{get;set;}
    public static Boolean PublicGroupMembership{get;set;}
    public static Boolean PermissionSetLicenseAssignments{get;set;}
    public static String errorMsg{get;set;}
    //String  Desktop;
    //String  Id;
   
    public static  List<String> fieldsToClone;
    public  static Map<String, Schema.SObjectField> fields;
    
    public cloneUserlightning(){
        
    }
    @AuraEnabled
    public static boolean test(String idu,String fn ,String ln,String email, String un ,String alias,String nickn,
                               boolean gPx,boolean pAx, boolean qMx, boolean pGMx, boolean pSLAx){
        system.debug( '############ data user  ###########' );
        system.debug( '########## '+ fn+' ########## '+ ln+' ########## '+email +' ########## ' + un+' ########## '+ alias+' ########## '+nickn +' ##########');                           
        system.debug( '############# data user up ##########' );
        system.debug( '############ boolean ##########' );
        system.debug( '########## '+ gPx +' ########## '+ pAx+' ########## '+qMx +' ########## ' + pGMx+' ########## '+ pSLAx+' ########## ');                           
        system.debug( '############# boolean up ############' );
        return true;
    }
    @AuraEnabled
    public static boolean setFields( String idu , String fn ,String ln,String email, String un ,String alias,String nickn,
                               boolean gPx,boolean pAx, boolean qMx, boolean pGMx, boolean pSLAx){
        system.debug( '############ data user  ###########' );
        system.debug( '########## '+ fn+' ########## '+ ln+' ########## '+email +' ########## ' + un+' ########## '+ alias+' ########## '+nickn +' ##########');                           
        system.debug( '############# data user up ##########' );
        system.debug( '############ boolean ##########' );
        system.debug( '########## '+ gPx +' ########## '+ pAx+' ########## '+qMx +' ########## ' + pGMx+' ########## '+ pSLAx+' ########## ');                           
        system.debug( '############# boolean up ############' );
          
          clone( idu);                         
          return save( fn , ln, email,  un , alias, nickn, gPx, pAx,  qMx,  pGMx,  pSLAx);                         
    }
    @AuraEnabled
    public static void clone(String Id){
        fieldsToClone = new List<String>();
        fieldsToClone.add('CallCenterId');
        fieldsToClone.add('CompanyName');
        fieldsToClone.add('EmailEncodingKey');
        fieldsToClone.add('ManagerId');
        fieldsToClone.add('LanguageLocaleKey');
        fieldsToClone.add('LocaleSidKey');
        fieldsToClone.add('ProfileId');
        fieldsToClone.add('UserRoleId');
        fieldsToClone.add('TimeZoneSidKey');
        fieldsToClone.add('UserPermissionsWorkDotComUserFeature');
        fieldsToClone.add('UserPermissionsMarketingUser');
        fieldsToClone.add('UserPermissionsOfflineUser');
        fieldsToClone.add('UserPermissionsInteractionUser');
        fieldsToClone.add('UserPreferencesHideS1BrowserUI');
        fieldsToClone.add('UserPermissionsSFContentUser');
        fieldsToClone.add('UserPreferencesApexPagesDeveloperMode');
        fieldsToClone.add('ForecastEnabled');
        fieldsToClone.add('UserPreferencesContentNoEmail');
        fieldsToClone.add('UserPreferencesContentEmailAsAndWhen');
        fieldsToClone.add('UserPermissionsKnowledgeUser');
        fieldsToClone.add('UserPermissionsSiteforceContributorUser');
        fieldsToClone.add('UserPermissionsSiteforcePublisherUser');
        fieldsToClone.add('UserPermissionsSupportUser');
        fieldsToClone.add('ReceivesAdminInfoEmails');
        fieldsToClone.add('Street');
        fieldsToClone.add('City');
        fieldsToClone.add('PostalCode');
        fieldsToClone.add('Country');
        fieldsToClone.add('State');
        fieldsToClone.add('DelegatedApproverId');
        fieldsToClone.add('JigsawImportLimitOverride');
        fieldsToClone.add('ReceivesInfoEmails');
        //Id = ApexPages.CurrentPage().getParameters().get('Id');
        //Desktop = ApexPages.CurrentPage().getParameters().get('Desktop');
        if(Id != null){
            Schema.SObjectType s = User.sObjectType ;
            Schema.DescribeSObjectResult r = s.getDescribe() ;
            fields = r.fields.getMap();
            String query = 'Select Profile.UserLicense.Name, Name, isActive';
            for(String f : fieldsToClone){
                if(fields.ContainsKey(f)){
                    query += ', ' + f;
                }
            }
            query += ' FROM User WHERE Id=: Id';
            toClone = Database.query(query);//[SELECT Profile.UserLicense.Name, Name, CallCenterId, CompanyName, EmailEncodingKey, ManagerId, LanguageLocaleKey, LocaleSidKey, ProfileId, UserRoleId, TimeZoneSidKey, isActive, UserPermissionsWorkDotComUserFeature, UserPermissionsMarketingUser, UserPermissionsOfflineUser, UserPermissionsInteractionUser, UserPreferencesHideS1BrowserUI, UserPermissionsSFContentUser, UserPreferencesApexPagesDeveloperMode, ForecastEnabled, UserPreferencesContentNoEmail, UserPreferencesContentEmailAsAndWhen, UserPermissionsKnowledgeUser, UserPermissionsSiteforceContributorUser, UserPermissionsSiteforcePublisherUser, ReceivesAdminInfoEmails, City, PostalCode, Country, Street, DelegatedApproverId, JigsawImportLimitOverride, ReceivesInfoEmails FROM User WHERE Id=: Id];
            u = cloneUser(toClone);
        }
        generatePassword = true;
        PermissionSetAssignments = true;
        QueueMembership = true;
        PublicGroupMembership = true;
        PermissionSetLicenseAssignments = true;
        errorMsg = '';
    }
    private static User cloneUser(User un){
        User userNew = new User();
        for(String f : fieldsToClone){
            if(fields.ContainsKey(f)){
                userNew.put(f, un.get(f));
            }
        }
        return userNew;
    }
    @AuraEnabled
    public static boolean save(String fn ,String ln,String email, String un ,String alias,String nickn,
                               boolean gPx,boolean pAx, boolean qMx, boolean pGMx, boolean pSLAx){
        u.FirstName	= 	fn;
        u.LastName	=	ln;
        u.UserName	=	un;
        u.Email		=	email;
        u.Alias		=	alias;
        u.CommunityNickname	= nickn;
        generatePassword 				=	gPx  ;
    	PermissionSetAssignments 		=	pAx  ;
     	QueueMembership 				=	qMx  ;
     	PublicGroupMembership 			=	pGMx ;
     	PermissionSetLicenseAssignments =	pSLAx;
        String clonedData = '';
        System.SavePoint sp = Database.setSavepoint();
        try{
            if (!Test.isRunningTest()){
                if(Schema.sObjectType.User.fields.FirstName.isCreateable() && Schema.sObjectType.User.fields.LastName.isCreateable() && Schema.sObjectType.User.fields.UserName.isCreateable() && Schema.sObjectType.User.fields.Email.isCreateable() && Schema.sObjectType.User.fields.Alias.isCreateable() && Schema.sObjectType.User.fields.CommunityNickname.isCreateable()){
                    insert u;
                }
            }
        }
        catch(System.DmlException dml){
            //system.debug('#### ' + dml);
            return setErrorMessage(dml.getDmlType(0));
        }
        catch(Exception e){
            //system.debug('#### ' + e);
            errorMsg =  e.getMessage();
            return false;
        }
        errorMsg = '';
        if(generatePassword){
            if (!Test.isRunningTest()){
                system.resetPassword(u.Id, true);
            }   
        }
        try{
            if(PermissionSetLicenseAssignments){
                clonedData += 'Permission Set Assignments';
                for(PermissionSetLicenseAssign psla : [Select AssigneeId, PermissionSetLicenseId FROM PermissionSetLicenseAssign WHERE AssigneeId =: toClone.Id]){
                    PermissionSetLicenseAssign pslaNew = new PermissionSetLicenseAssign(AssigneeId = u.Id, PermissionSetLicenseId = psla.PermissionSetLicenseId);
                    if (!Test.isRunningTest() && (PermissionSetLicenseAssign.sObjectType.getDescribe().isCreateable())){
                        insert pslaNew;
                    }
                }
            }
        }
        catch(DmlException e){
            errorMsg = e.getDmlMessage(0);
            Database.rollback(sp);
            u.Id = null;
            return false;
        }
        
        //Create permissionSetAssignments
        boolean ins;
        if(PublicGroupMembership){
            clonedData += ',Public Groups';
        }
        if(QueueMembership){
            clonedData += ',Queue Membership';
        }
        for(GroupMember gm : [Select UserOrGroupId, GroupId, Group.Type FROM GroupMember WHERE UserOrGroupId=: toClone.Id]){
            ins = false;
            if(QueueMembership && gm.Group.Type == 'Queue'){
                ins = true;
            }
            else if(PublicGroupMembership && gm.Group.Type != 'Queue'){
                ins = true;
            }
            if(ins && Schema.sObjectType.GroupMember.fields.UserOrGroupId.isCreateable() && Schema.sObjectType.GroupMember.fields.GroupId.isCreateable()){
                GroupMember gmNew = new GroupMember(UserOrGroupId = u.Id, GroupId = gm.GroupId );
                if (!Test.isRunningTest() && GroupMember.sObjectType.getDescribe().isCreateable()){
                    insert gmNew;
                }
            }
        }
        if(PermissionSetAssignments){
            clonedData += ',Permission Set License Assignments';
            for(PermissionSetAssignment psa : [Select AssigneeId, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment WHERE AssigneeId =: toClone.Id AND PermissionSet.IsOwnedByProfile = false]){
                String i = toClone.ProfileId;
                PermissionSetAssignment psaNew = new PermissionSetAssignment(AssigneeId = u.Id, PermissionSetId = psa.PermissionSetId);
                
                if (!Test.isRunningTest() && PermissionSetAssignment.sObjectType.getDescribe().isCreateable()){
                    insert psaNew;
                }
            }
        }
       /* PageReference pr;
        if(Desktop != null){
            //pr = new PageReference('/' + u.Id + '?noredirect=1');
            pr = new PageReference('/apex/ctu__CloneThisUserSuccess?old=' + toClone.Id + '&new=' + u.Id + '&pass=' + generatePassword + '&cloned=' + clonedData);
        }
        else{
            pr = new PageReference('/apex/ctu__CloneThisUserList?success=1');
        }
        pr.setRedirect(true);*/
        return true;
    }
    
    public static  boolean setErrorMessage(system.statusCode code){
        if(code == Statuscode.LICENSE_LIMIT_EXCEEDED){
            errorMsg = 'You do not have enough feature licenses to clone this user.';//'You don’t have enough ' + toClone.Profile.UserLicense.Name + ' licenses to clone this user';
        }
        else if(code == Statuscode.DUPLICATE_USERNAME){
            u.UserName.AddError('Duplicate Username');
            //errorMsg = 'Duplicate UserName';
        }
        else if(code == Statuscode.DUPLICATE_COMM_NICKNAME){
            u.CommunityNickname.AddError('Duplicate Nickname');
            //errorMsg =  'Duplicate Nickname';
        }
        else{
            system.debug('#### ' + code);
        }
        return false;
        
    }
}