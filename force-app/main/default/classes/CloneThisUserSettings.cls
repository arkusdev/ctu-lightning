public with sharing class CloneThisUserSettings {
    private static List<ctu__Field_Default_Value_Obj__c> defaultValues = [SELECT Id, ctu__Data_Type__c,
                                                                          ctu__Default_Value__c, ctu__Default_Value_Label__c,
                                                                          ctu__Field_Name__c
                                                                          FROM ctu__Field_Default_Value_Obj__c
                                                                          LIMIT 500];
    private static final String SYS_ADMIN = 'System Administrator';
    private static final List<String> availableStandardFields = new List<String>{
        'IsActive', 'MiddleName', 'Suffix', 'Title', 'CompanyName', 'Department', 'Division', 'Phone', 'Extension',
        'Fax', 'MobilePhone', 'EmailEncodingKey', 'EmployeeNumber', 'Street',
        'City', 'PostalCode', 'State', 'Country', 'TimeZoneSidKey', 'LanguageLocaleKey', 
        'LocaleSidKey', 'ManagerId', 'DelegatedApproverId', 'ProfileId', 'UserRoleId',
        'UserPermissionsCallCenterAutoLogin', 'UserPermissionsChatterAnswersUser',
        'UserPermissionsInteractionUser', 'UserPermissionsJigsawProspectingUser',
        'UserPermissionsKnowledgeUser', 'UserPermissionsLiveAgentUser', 'UserPermissionsMarketingUser',
        'UserPermissionsOfflineUser', 'UserPermissionsSFContentUser', 'UserPermissionsSiteforceContributorUser',
        'UserPermissionsSiteforcePublisherUser', 'UserPermissionsSupportUser', 'UserPermissionsWirelessUser',
        'UserPermissionsWorkDotComUserFeature', 'DefaultCurrencyIsoCode'
    };

    private static final List<ctu__General_Option_Value_Obj__c> availableGeneralOptions = new List<ctu__General_Option_Value_Obj__c>{
        new ctu__General_Option_Value_Obj__c(Name='Generate new password', ctu__Data_Label__c='Generate new password and notify user immediately', ctu__Data_Category__c='General', ctu__Default_Value__c= true),
        // Advanced Options
        new ctu__General_Option_Value_Obj__c(Name='Advanced Chatter Group Memberships', ctu__Data_Label__c='Chatter Group Memberships', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        new ctu__General_Option_Value_Obj__c(Name='Advanced Shared Dashboards', ctu__Data_Label__c='Shared Dashboards', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        new ctu__General_Option_Value_Obj__c(Name='Advanced Permission Set Assignments', ctu__Data_Label__c='Permission Set Assignments', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        new ctu__General_Option_Value_Obj__c(Name='Advanced Shared Reports', ctu__Data_Label__c='Shared Reports', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        new ctu__General_Option_Value_Obj__c(Name='Advanced Queue Memberships', ctu__Data_Label__c='Queue Memberships', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        new ctu__General_Option_Value_Obj__c(Name='Advanced Public Group Memberships', ctu__Data_Label__c='Public Group Memberships', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        new ctu__General_Option_Value_Obj__c(Name='Advanced Permission Set License', ctu__Data_Label__c='Permission Set License Assignments', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        new ctu__General_Option_Value_Obj__c(Name='Advanced Account Team Membership', ctu__Data_Label__c='Account Team Membership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        new ctu__General_Option_Value_Obj__c(Name='Advanced Account Ownership', ctu__Data_Label__c='Account Ownership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        new ctu__General_Option_Value_Obj__c(Name='Advanced Lead Ownership', ctu__Data_Label__c='Lead Ownership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        new ctu__General_Option_Value_Obj__c(Name='Advanced Opportunity Ownership', ctu__Data_Label__c='Opportunity Ownership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        new ctu__General_Option_Value_Obj__c(Name='Advanced Opportunity Team Membership', ctu__Data_Label__c='Opportunity Team Membership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        new ctu__General_Option_Value_Obj__c(Name='Advanced Case Ownership', ctu__Data_Label__c='Case Ownership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        new ctu__General_Option_Value_Obj__c(Name='Advanced Case Team Membership', ctu__Data_Label__c='Case Team Membership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
        // Remove Options
        new ctu__General_Option_Value_Obj__c(Name='Deactivate Original User', ctu__Data_Label__c='Deactivate Original User', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
        new ctu__General_Option_Value_Obj__c(Name='Remove Chatter Group Memberships', ctu__Data_Label__c='Chatter Group Memberships', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
        new ctu__General_Option_Value_Obj__c(Name='Remove Shared Dashboards', ctu__Data_Label__c='Shared Dashboards', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
        new ctu__General_Option_Value_Obj__c(Name='Remove Permission Set Assignments', ctu__Data_Label__c='Permission Set Assignments', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
        new ctu__General_Option_Value_Obj__c(Name='Remove Shared Reports', ctu__Data_Label__c='Shared Reports', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
        new ctu__General_Option_Value_Obj__c(Name='Remove Queue Memberships', ctu__Data_Label__c='Queue Memberships', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
        new ctu__General_Option_Value_Obj__c(Name='Remove Public Group Memberships', ctu__Data_Label__c='Public Group Memberships', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
        new ctu__General_Option_Value_Obj__c(Name='Remove Permission Set License', ctu__Data_Label__c='Permission Set License Assignments', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
        new ctu__General_Option_Value_Obj__c(Name='Remove Account Team Membership', ctu__Data_Label__c='Account Team Membership', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
        new ctu__General_Option_Value_Obj__c(Name='Remove Opportunity Team Membership', ctu__Data_Label__c='Opportunity Team Membership', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
        new ctu__General_Option_Value_Obj__c(Name='Remove Case Team Membership', ctu__Data_Label__c='Case Team Membership', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false)
    };

    @AuraEnabled
    public static cmpdata getData() {
        Map<String, ctu__Field_Default_Value_Obj__c> fieldValues = getFieldsDefaultValues();
        List<data> result = new List<data>();
        Boolean checkManageUsers = true;
        Boolean checkModifyAllData = true;
        Boolean checkPsAssign = true;
        Boolean checkResetPassword = true;
        Boolean checkQueue = true;
        Boolean canViewQuickLinksBar = false;
        User usr = [SELECT Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
        if(usr.Profile.Name == SYS_ADMIN) {
            List<String> ignoredField = new List<String>();
            for (ctu__CTU_Custom_Field_Ignored__c cfi : [SELECT ctu__API_Name__c FROM ctu__CTU_Custom_Field_Ignored__c]) {
                ignoredField.add(cfi.ctu__API_Name__c);
            }
            Map<String, SObjectField> fieldsByName = Schema.SObjectType.User.fields.getMap();
            Boolean isFromPackage = false;
            for(SObjectField field : fieldsByName.values()) {
                ctu__Field_Default_Value_Obj__c defaultValue = fieldValues.get(field.getDescribe().getName());
                if (defaultValue != null && defaultValue.ctu__Data_Type__c == 'BOOLEAN'){
                    defaultValue.ctu__Default_Value_Label__c = defaultValue.ctu__Default_Value_Label__c == 'true' ? 'Yes' : (defaultValue.ctu__Default_Value_Label__c == 'false' ? 'No' : defaultValue.ctu__Default_Value_Label__c);
                }
                if (field.getDescribe().isCustom() && !field.getDescribe().isCalculated() && !field.getDescribe().getName().toLowerCase().endsWith('__latitude__s') && !field.getDescribe().getName().toLowerCase().endsWith('__longitude__s')) {
                    isFromPackage = field.getDescribe().getName() != field.getDescribe().getLocalName();
                    result.add(new data(    field.getDescribe().getLabel(),
                                            field.getDescribe().getLocalName(),
                                            isFromPackage ? field.getDescribe().getName().split('__')[0] : '' ,
                                            String.valueOf(field.getDescribe().getType()),
                                            ignoredField.contains(field.getDescribe().getLocalName()),
                                            isFieldRequired(field.getDescribe()),
                                            defaultValue
                    ));
                }
            }

            List<ctu__CTU_Settings__c> ctu_settings = [SELECT ctu__Do_not_Check_Manage_Users_Permission__c, ctu__Do_not_Check_Modify_All_Data_Permission__c, 
                                                                ctu__Do_not_Check_Assign_PS_Permission__c, ctu__Do_not_Check_Reset_Password_Permission__c,
                                                                ctu__Do_not_Check_Queue_Permission__c, ctu__Non_Sys_Admin_Can_View_Quick_Links__c
                                                        FROM ctu__CTU_Settings__c LIMIT 1];
            checkManageUsers = ctu_settings.size() > 0 ? !ctu_settings[0].ctu__Do_not_Check_Manage_Users_Permission__c : true;
            checkModifyAllData = ctu_settings.size() > 0 ? !ctu_settings[0].ctu__Do_not_Check_Modify_All_Data_Permission__c : true;
            checkPsAssign = ctu_settings.size() > 0 ? !ctu_settings[0].ctu__Do_not_Check_Assign_PS_Permission__c : true;
            checkResetPassword = ctu_settings.size() > 0 ? !ctu_settings[0].ctu__Do_not_Check_Reset_Password_Permission__c : true;
            checkQueue = ctu_settings.size() > 0 ? !ctu_settings[0].ctu__Do_not_Check_Queue_Permission__c : true;
            canViewQuickLinksBar = ctu_settings.size() > 0 ? ctu_settings[0].ctu__Non_Sys_Admin_Can_View_Quick_Links__c : false;
        }
        cmpdata cmpdata = new cmpdata(isSystemAdmin(), checkManageUsers, checkModifyAllData, checkPsAssign, checkResetPassword, checkQueue, result, canViewQuickLinksBar);

        return cmpdata;
    }

    @AuraEnabled
    public static cmpdata getStandardData() {
        Map<String, ctu__Field_Default_Value_Obj__c> fieldValues = getFieldsDefaultValues();
        List<data> result = new List<data>();
        Boolean checkManageUsers = true;
        User usr = [SELECT Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
        if(usr.Profile.Name == SYS_ADMIN) {
            List<String> ignoredField = new List<String>();
            for (ctu__CTU_Custom_Field_Ignored__c cfi : [SELECT ctu__API_Name__c FROM ctu__CTU_Custom_Field_Ignored__c]) {
                ignoredField.add(cfi.ctu__API_Name__c);
            }
            Boolean isFromPackage = false;
            Map<String, SObjectField> fieldsByName = Schema.SObjectType.User.fields.getMap();
            for(SObjectField field : fieldsByName.values()) {
                isFromPackage = field.getDescribe().getName().split('__').size() > 1;
                DescribeFieldresult fieldDescription = field.getDescribe();
                ctu__Field_Default_Value_Obj__c defaultValue = fieldValues.get(fieldDescription.getName());
                if (defaultValue != null && defaultValue.ctu__Data_Type__c == 'BOOLEAN'){
                    defaultValue.ctu__Default_Value_Label__c = defaultValue.ctu__Default_Value_Label__c == 'true' ? 'Yes' : (defaultValue.ctu__Default_Value_Label__c == 'false' ? 'No' : defaultValue.ctu__Default_Value_Label__c);
                }
                if (availableStandardFields.contains(fieldDescription.getName()) && !fieldDescription.isCalculated()) {
                    result.add(new data(    fieldDescription.getLabel(),
                                            fieldDescription.getLocalName(),
                                            isFromPackage ? fieldDescription.getName().split('__')[0] : '' ,
                                            String.valueOf(fieldDescription.getType()),
                                            ignoredField.contains(fieldDescription.getLocalName()),
                                            isFieldRequired(fieldDescription),
                                            defaultValue
                    ));
                }
            }

            List<ctu__CTU_Settings__c> ctu_settings = [SELECT ctu__Do_not_Check_Manage_Users_Permission__c, ctu__Non_Sys_Admin_Can_View_Quick_Links__c FROM ctu__CTU_Settings__c LIMIT 1];
            checkManageUsers = ctu_settings.size() > 0 ? !ctu_settings[0].ctu__Do_not_Check_Manage_Users_Permission__c : true;
        }
        cmpdata cmpdata = new cmpdata(isSystemAdmin(), checkManageUsers, result);

        return cmpdata;
    }

    @AuraEnabled
    public static List<ctu__General_Option_Value_Obj__c> getGeneralOptionsData() {
        try {
            List<ctu__General_Option_Value_Obj__c> currentValues = [SELECT Id, Name, ctu__Data_Category__c, ctu__Data_Label__c, ctu__Default_Value__c FROM ctu__General_Option_Value_Obj__c];
            List<ctu__General_Option_Value_Obj__c> result = new List<ctu__General_Option_Value_Obj__c>();
            for(ctu__General_Option_Value_Obj__c available: availableGeneralOptions){
                for(ctu__General_Option_Value_Obj__c current: currentValues){
                    if(current.Name == available.Name){
                        available.ctu__Default_Value__c= current.ctu__Default_Value__c;
                    }
                }
                result.add(available);
            }
            return result;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(cacheable = true)
    public static List<sObject> getObjects(String searchParam, String objectName){
        List<String> communityLicenses = CloneThisUserUtils.getCommunityUserLicenses();
        List<List<sObject>> queryResult = [
                                            FIND :searchParam IN NAME FIELDS RETURNING
                                            Profile(Id, Name WHERE Profile.UserLicense.Name NOT IN: communityLicenses),
                                            UserRole(Id, Name),
                                            User(Id, Name)
                                        ];
        switch on objectName {
            when 'Profile' {
                return queryResult[0];
            }
            when 'UserRole' {
                return queryResult[1];
            }
            when 'User' {
                return queryResult[2];
            }
        }
        return new List<SObject>();
    }

    private static boolean isFieldRequired(DescribeFieldResult describe){
        return !describe.isNillable() && describe.isCreateable();
    }

    @AuraEnabled
    public static String updateData(List<String> data, List<ctu__Field_Default_Value_Obj__c> defaultValues) {
        upsert defaultValues ctu__Field_Name__c;

        Map<String, ctu__CTU_Custom_Field_Ignored__c> cfi_up = new Map<String, ctu__CTU_Custom_Field_Ignored__c>();
        for(ctu__CTU_Custom_Field_Ignored__c cfi : [SELECT ctu__API_Name__c FROM ctu__CTU_Custom_Field_Ignored__c LIMIT 10000]){
            cfi_up.put(cfi.ctu__API_Name__c, cfi);
        }

        //Remove
        List<ctu__CTU_Custom_Field_Ignored__c> cfi_ToDelete = new List<ctu__CTU_Custom_Field_Ignored__c>();
        for(ctu__CTU_Custom_Field_Ignored__c cfi : cfi_up.values()) {
            if(!data.contains(cfi.ctu__API_Name__c)) {
                cfi_ToDelete.add(cfi);
            }
        }
        delete cfi_ToDelete;

        //Add
        List<ctu__CTU_Custom_Field_Ignored__c> cfi_ToInsert = new List<ctu__CTU_Custom_Field_Ignored__c>();
        for(String field : data){
            if(!cfi_up.containsKey(field)) {
                cfi_ToInsert.add(new ctu__CTU_Custom_Field_Ignored__c(ctu__API_Name__c = field));
            }
        }
        insert cfi_ToInsert;

        return 'Success';
    }

    @AuraEnabled
    public static String updateManageUsers(Boolean manageUsers, Boolean modifyAllData, Boolean assignPS, Boolean resetPassword, Boolean cqueue) {
        List<ctu__CTU_Settings__c> ctu_settings = [SELECT ctu__Do_not_Check_Manage_Users_Permission__c, ctu__Do_not_Check_Modify_All_Data_Permission__c, 
                                                        ctu__Do_not_Check_Assign_PS_Permission__c, ctu__Do_not_Check_Reset_Password_Permission__c,
                                                        ctu__Do_not_Check_Queue_Permission__c
                                                    FROM ctu__CTU_Settings__c LIMIT 1];
        if (ctu_settings.size() > 0){
            ctu__CTU_Settings__c ctu_setting = ctu_settings[0];
            ctu_setting.ctu__Do_not_Check_Manage_Users_Permission__c = !manageUsers;
            ctu_setting.ctu__Do_not_Check_Modify_All_Data_Permission__c = !modifyAllData;
            ctu_setting.ctu__Do_not_Check_Assign_PS_Permission__c = !assignPS;
            ctu_setting.ctu__Do_not_Check_Reset_Password_Permission__c = !resetPassword;
            ctu_setting.ctu__Do_not_Check_Queue_Permission__c = !cqueue;
            update ctu_setting;
        }else {
            ctu__CTU_Settings__c newSetting = new ctu__CTU_Settings__c(
                ctu__Do_not_Check_Manage_Users_Permission__c = !manageUsers,
                ctu__Do_not_Check_Modify_All_Data_Permission__c = !modifyAllData,
                ctu__Do_not_Check_Assign_PS_Permission__c = !assignPS,
                ctu__Do_not_Check_Reset_Password_Permission__c = !resetPassword,
                ctu__Do_not_Check_Queue_Permission__c = !cqueue
            );
            insert newSetting;
        }
        return 'Success';
    }

    @AuraEnabled
    public static String updateViewQuickLinks(Boolean viewQuickLinks) {
        List<ctu__CTU_Settings__c> ctu_settings = [SELECT ctu__Non_Sys_Admin_Can_View_Quick_Links__c
                                                    FROM ctu__CTU_Settings__c LIMIT 1];
        if (ctu_settings.size() > 0){
            ctu__CTU_Settings__c ctu_setting = ctu_settings[0];
            if (Schema.sObjectType.ctu__CTU_Settings__c.fields.ctu__Non_Sys_Admin_Can_View_Quick_Links__c.isUpdateable()){
                ctu_setting.ctu__Non_Sys_Admin_Can_View_Quick_Links__c = viewQuickLinks;
                update ctu_setting;
            }
        }else {
            if (Schema.sObjectType.ctu__CTU_Settings__c.fields.ctu__Non_Sys_Admin_Can_View_Quick_Links__c.isCreateable()){
                ctu__CTU_Settings__c newSetting = new ctu__CTU_Settings__c(
                    ctu__Non_Sys_Admin_Can_View_Quick_Links__c = viewQuickLinks
                );
                insert newSetting;
            }
        }
        return 'Success';
    }
    
    
    
    @AuraEnabled
    public static Boolean isSystemAdmin() {
        User usr = [SELECT Profile.Name FROM User WHERE Id = :UserInfo.getUserId() ];
        return usr.Profile.Name == 'System Administrator';
    }

    public static Map<String, ctu__Field_Default_Value_Obj__c> getFieldsDefaultValues() {
        Map<String, ctu__Field_Default_Value_Obj__c> defaultValuesByName = new Map<String, ctu__Field_Default_Value_Obj__c>();
        for (ctu__Field_Default_Value_Obj__c defaultValue : defaultValues) {
            defaultValuesByName.put(defaultValue.ctu__Field_Name__c, defaultValue);
        }
        return defaultValuesByName;
    }

    @AuraEnabled
    public static List<String> getPicklistValues(String fieldName){
        String objectName = 'User';
        List<String> picklistValues = new List<String>();
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for (Schema.PicklistEntry plv : ple) {
            picklistValues.add(plv.label);
        }

        return picklistValues;
    }

    @AuraEnabled
    public static string getNameByRecordId(Id recordId){
        try {
            String dataType = recordId.getsobjecttype().getDescribe().getName();
            String query = 'SELECT Name FROM ' + String.escapeSingleQuotes(dataType) + ' WHERE Id = \'' + String.escapeSingleQuotes(recordId) + '\'';
            return (String)(Database.query(query)[0].get('Name'));
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void saveAdvancedOptions(List<ctu__General_Option_Value_Obj__c> advancedOptions) {
        try {
            upsert advancedOptions Name;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    public class cmpdata {
        @AuraEnabled public Boolean isSystemAdmin { get; set; }
        @AuraEnabled public Boolean checkManageUsers { get; set; }
        @AuraEnabled public Boolean checkModifyAllData { get; set; }
        @AuraEnabled public Boolean checkPsAssign { get; set; }
        @AuraEnabled public Boolean checkResetPassword { get; set; }
        @AuraEnabled public Boolean checkQueue { get; set; }
        @AuraEnabled public List<data> returnData { get; set; }
        @AuraEnabled public Boolean canViewQuickLinksBar { get; set; }
        public cmpdata (Boolean pIsSystemAdmin, Boolean pCheckManageUsers, List<data> pReturnData) {
            isSystemAdmin = pIsSystemAdmin;
            checkManageUsers = pCheckManageUsers;
            returnData = pReturnData;
        }
        public cmpdata (Boolean pIsSystemAdmin, Boolean pCheckManageUsers, Boolean pCheckModifyAllData, Boolean pcheckPsAssign, Boolean pCheckResetPassword, Boolean pCheckQueue, List<data> pReturnData, Boolean pcanViewQuickLinksBar) {
            isSystemAdmin = pIsSystemAdmin;
            checkManageUsers = pCheckManageUsers;
            checkModifyAllData = pCheckModifyAllData;
            checkPsAssign = pcheckPsAssign;
            checkResetPassword = pCheckResetPassword;
            checkQueue = pCheckQueue;
            returnData = pReturnData;
            canViewQuickLinksBar = pcanViewQuickLinksBar;
            
        }
    }

    public class data {
        
        @AuraEnabled public String FieldLabel { get; set; }
        @AuraEnabled public String APIName { get; set; }
        @AuraEnabled public String InstalledPackage { get; set; }
        @AuraEnabled public String DataType { get; set; }
        @AuraEnabled public Boolean IsHidden { get; set; }
        @AuraEnabled public Boolean IsRequired { get; set; }
        @AuraEnabled public ctu__Field_Default_Value_Obj__c DefaultValue { get; set; }
        public data(String pFieldLabel, String pAPIName, String pInstalledPackage, String pDataType, Boolean pIsHidden, Boolean pIsRequired, ctu__Field_Default_Value_Obj__c pDefaultValue) {
            FieldLabel = pFieldLabel;
            APIName = pAPIName;
            InstalledPackage = pInstalledPackage;
            DataType = pDataType;
            IsHidden = pIsHidden;
            IsRequired = pIsRequired;
            DefaultValue = pDefaultValue;
        }
    }
}