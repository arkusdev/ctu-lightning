public with sharing class CloneThisUserSuccess {
	public User oldUser {get;set;}
	public User newUser {get;set;}
	public List<String> clonedThings {get;set;}
	public List<String> clonedFields {get;set;}
	public Boolean generated {get;set;}
	
	public CloneThisUserSuccess(){
		String oldUserId = ApexPages.currentPage().getParameters().get('old');
		String newUserId = ApexPages.currentPage().getParameters().get('new');
		Boolean isAccessible = Schema.sObjectType.User.isAccessible();
		if(oldUserId != null && newUserId != null && isAccessible){
			oldUser = [SELECT Name, Id FROM User WHERE Id =: oldUserId];
			newUser = [SELECT Name, Id FROM User WHERE Id =: newUserId];
			String cloned = ApexPages.currentPage().getParameters().get('cloned');
			clonedFields = 'Call Center,Company,Email Encoding,Manager,Language,Locale,Profile,Role,Timezone,Work.com User,Marketing User,Offline User,Force.com Flow User,Salesforce1 User,Salesforce CRM Content User,Development Mode,Allow Forecasting,Receive Salesforce CRM Content Alerts,Receive Salesforce CRM Content Alerts as Daily Digest,Knowledge User,Address,Newsletter,Admin Newsletter,Site.com Contribution User,Site.com Publisher User,Data.com Monthly Addition Limit,Delegated Approver'.split(',');
			clonedThings = cloned.split(',');
			String pass = ApexPages.currentPage().getParameters().get('pass');
			if(pass == 'true'){
				generated = true;
			}
			else{
				generated = false;
			}
		}
	}
}