@isTest
private class CloneThisUserV3ControllerTest {
    
    @isTest
    static  void testSearchUsers(){
        User user = createTestUser();
        List<User> searchResults = CloneThisUserV3Controller.getUsers('clone12345@clone12345.com');
        User result = NULL;
        for(User u: searchResults){
            if(user.Id == u.Id){
                result = u;
            }
        }
        system.assertEquals(user.Id, result.Id);
    }
    
    @isTest
    static  void testCloneUser(){
        Test.startTest();
        User user = createTestUser();
        Group grp = createTestGroup();
        Group queue = createTestQueue();
        
        addUserToGroup(user, grp);
        addUserToGroup(user, queue);

        PermissionSet permset = createTestPermissionSet();
        addUserToPset(user, permset);
        CloneThisUserV3Controller.CTUOptions opts = new CloneThisUserV3Controller.CTUOptions();
        opts.GeneratePassword = true;
        opts.PermissionSetLicenseAssignments = true;
        opts.QueueMembership = true;
        opts.PublicGroupMembership = true;
        opts.PermissionSetAssignments = true;

        CloneThisUserV3Controller.RemoveOptions removeOpts = new CloneThisUserV3Controller.RemoveOptions();
        removeOpts.DeactivateUser = true;
        removeOpts.PermissionSetLicenseAssignments = false;
        removeOpts.QueueMembership = false;
        removeOpts.PublicGroupMembership = false;
        removeOpts.PermissionSetAssignments = false;

        User userDTO = new User(LastName='someclone',FirstName='someclone',Username='someclone@someclone.com',Email='someclone@someclone.com',Alias='someca', CommunityNickname='somecaTEST');
        User userCF = new User();
        // User userSF = new User();
        Profile profile = [SELECT Id FROM profile WHERE Name=: 'Standard User'];
        String standardFields = '{"CompanyName":"CompanyTestClon","TimeZoneSidKey":"America/Los_Angeles","LocaleSidKey":"en_US","EmailEncodingKey":"ISO-8859-1","ProfileId":"' + profile.Id + '","LanguageLocaleKey":"en_US"}';
        CloneThisUserV3Controller.CTUStatus status = CloneThisUserV3Controller.cloneThisUser(user.Id, JSON.serialize(userDTO), JSON.serialize(userCF), standardFields ,JSON.serialize(opts), JSON.serialize(removeOpts));
        System.assertNotEquals(NULL, status);
        User clonedUser = [SELECT LastName, FirstName, UserName, Email,Alias,CommunityNickname FROM User WHERE Id =: status.newUser.Id];
        List<GroupMember> gmembership = [Select Id FROM GroupMember WHERE GroupId = : grp.Id AND UserOrGroupId =: clonedUser.Id];
        List<PermissionSetAssignment> pmsa = [SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId =: clonedUser.Id AND PermissionSet.IsOwnedByProfile = false];
        Test.stopTest();
        System.assertNotEquals(NULL, clonedUser);
        System.assertEquals(userDTO.LastName, clonedUser.LastName);
        System.assertEquals(userDTO.FirstName, clonedUser.FirstName);
        System.assertEquals(userDTO.Username, clonedUser.Username);
        System.assertEquals(userDTO.Email, clonedUser.Email);
        System.assertEquals(userDTO.Alias, clonedUser.Alias);
        System.assertEquals(userDTO.CommunityNickname, clonedUser.CommunityNickname);
        System.assertEquals(1, gmembership.Size());
        System.assertEquals(1, pmsa.size());
    }
    @isTest
    static  void testCloneUserWithPermissions(){
        Test.startTest();
        User user = createTestUser();
        Group grp = createTestGroup();
        Group queue = createTestQueue();

        PermissionSet ps = new PermissionSet(Label = 'Only testing', Name = 'Only_Testing');
        insert ps;
        PermissionSetLicense psl = [SELECT Id FROM PermissionSetLicense LIMIT 1];
        
        PermissionSetLicenseAssign psla = new PermissionSetLicenseAssign(PermissionSetLicenseId = psl.Id, AssigneeId = user.Id);
        insert psla;
        
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = user.Id, PermissionSetId = ps.Id);
        insert psa;
        
        addUserToGroup(user, grp);
        addUserToGroup(user, queue);

        PermissionSet permset = createTestPermissionSet();
        addUserToPset(user, permset);
        CloneThisUserV3Controller.CTUOptions opts = new CloneThisUserV3Controller.CTUOptions();
        opts.GeneratePassword = true;
        opts.PermissionSetLicenseAssignments = true;
        opts.QueueMembership = true;
        opts.PublicGroupMembership = true;
        opts.PermissionSetAssignments = true;

        CloneThisUserV3Controller.RemoveOptions removeOpts = new CloneThisUserV3Controller.RemoveOptions();
        removeOpts.DeactivateUser = true;
        removeOpts.PermissionSetLicenseAssignments = true;
        removeOpts.QueueMembership = true;
        removeOpts.PublicGroupMembership = true;
        removeOpts.PermissionSetAssignments = true;

        User userDTO = new User(LastName='someclone',FirstName='someclone',Username='someclone@someclone.com',Email='someclone@someclone.com',Alias='someca', CommunityNickname='somecaTEST');
        User userCF = new User();
        Profile profile = [SELECT Id FROM profile WHERE Name=: 'Standard User'];
        String standardFields = '{"CompanyName":"CompanyTestClon","TimeZoneSidKey":"America/Los_Angeles","LocaleSidKey":"en_US","EmailEncodingKey":"ISO-8859-1","ProfileId":"' + profile.Id + '","LanguageLocaleKey":"en_US"}';
        CloneThisUserV3Controller.CTUStatus status = CloneThisUserV3Controller.cloneThisUser(user.Id, JSON.serialize(userDTO), JSON.serialize(userCF), standardFields,JSON.serialize(opts), JSON.serialize(removeOpts));
        System.assertNotEquals(NULL, status);
        User clonedUser = [SELECT LastName, FirstName, UserName, Email,Alias,CommunityNickname FROM User WHERE Id =: status.newUser.Id];
        List<GroupMember> gmembership = [Select Id FROM GroupMember WHERE GroupId = : grp.Id AND UserOrGroupId =: clonedUser.Id];
        List<PermissionSetAssignment> pmsa = [SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId =: clonedUser.Id AND PermissionSet.IsOwnedByProfile = false];
        Test.stopTest();
        System.assertNotEquals(NULL, clonedUser);
        System.assertEquals(userDTO.LastName, clonedUser.LastName);
        System.assertEquals(userDTO.FirstName, clonedUser.FirstName);
        System.assertEquals(userDTO.Username, clonedUser.Username);
        System.assertEquals(userDTO.Email, clonedUser.Email);
        System.assertEquals(userDTO.Alias, clonedUser.Alias);
        System.assertEquals(userDTO.CommunityNickname, clonedUser.CommunityNickname);
        System.assertEquals(1, gmembership.Size());
        System.assertEquals(2, pmsa.size());
    }

    @isTest
    static void cloneThisUserException(){
        try {
            User user = createTestUser();
            Group grp = createTestGroup();
            Group queue = createTestQueue();
            
            addUserToGroup(user, grp);
            addUserToGroup(user, queue);

            PermissionSet permset = createTestPermissionSet();
            addUserToPset(user, permset);
            CloneThisUserV3Controller.CTUOptions opts = new CloneThisUserV3Controller.CTUOptions();
            opts.GeneratePassword = true;
            opts.PermissionSetLicenseAssignments = true;
            opts.QueueMembership = true;
            opts.PublicGroupMembership = true;
            opts.PermissionSetAssignments = true;

            CloneThisUserV3Controller.RemoveOptions removeOpts = new CloneThisUserV3Controller.RemoveOptions();
            removeOpts.DeactivateUser = false;
            removeOpts.PermissionSetLicenseAssignments = false;
            removeOpts.QueueMembership = false;
            removeOpts.PublicGroupMembership = false;
            removeOpts.PermissionSetAssignments = false;

            User userDTO = new User(LastName='someclone',FirstName='someclone',Username='clone12345@clone12345.com',Email='someclone@someclone.com',Alias='someca', CommunityNickname='somecaTEST');
            User userCF = new User();
            CloneThisUserV3Controller.CTUStatus status = CloneThisUserV3Controller.cloneThisUser(user.Id, JSON.serialize(userDTO), JSON.serialize(userCF), JSON.serialize(userCF),JSON.serialize(opts), JSON.serialize(removeOpts));
            System.assert(false);
        } catch (AuraHandledException exc) {
            System.assert(true);
        }
    }

    @isTest
    static void cloneThisUserIntegrityException(){
        try {
            User user = createTestUser();
            Group grp = createTestGroup();
            Group queue = createTestQueue();
            
            addUserToGroup(user, grp);
            addUserToGroup(user, queue);

            PermissionSet permset = createTestPermissionSet();
            addUserToPset(user, permset);
            CloneThisUserV3Controller.CTUOptions opts = new CloneThisUserV3Controller.CTUOptions();
            opts.GeneratePassword = true;
            opts.PermissionSetLicenseAssignments = true;
            opts.QueueMembership = true;
            opts.PublicGroupMembership = true;
            opts.PermissionSetAssignments = true;

            CloneThisUserV3Controller.RemoveOptions removeOpts = new CloneThisUserV3Controller.RemoveOptions();
            removeOpts.DeactivateUser = true;
            removeOpts.PermissionSetLicenseAssignments = false;
            removeOpts.QueueMembership = false;
            removeOpts.PublicGroupMembership = false;
            removeOpts.PermissionSetAssignments = false;

            User userDTO = new User(LastName='someclone',FirstName='someclone',Username='clone12345@clone12345',Email='someclone@someclone.com',Alias='someca', CommunityNickname='somecaTEST');
            User userCF = new User();
            CloneThisUserV3Controller.CTUStatus status = CloneThisUserV3Controller.cloneThisUser(user.Id, JSON.serialize(userDTO), JSON.serialize(userCF), JSON.serialize(userCF), JSON.serialize(opts), JSON.serialize(removeOpts));
            System.assert(false);
        } catch (AuraHandledException exc) {
            System.assert(true);
        }
    }
    
    @isTest
    static  void testLicenseError(){
        String error = CloneThisUserV3Controller.getErrorMessage(Statuscode.LICENSE_LIMIT_EXCEEDED, 'Message', new List<String>{''}, 1, 'Stack');
        System.assertEquals('You do not have enough feature licenses to clone this user.', error);
    }
    
    @isTest
    static  void testDuplicateNameError(){
        String error = CloneThisUserV3Controller.getErrorMessage(Statuscode.DUPLICATE_USERNAME, 'Message', new List<String>{''}, 1, 'Stack');
        System.assertEquals('Duplicate Username', error);
    }
    
    @isTest
    static  void testDuplicateNickError(){
        String error = CloneThisUserV3Controller.getErrorMessage(Statuscode.DUPLICATE_COMM_NICKNAME, 'Message', new List<String>{''}, 1, 'Stack');
        System.assertEquals('Duplicate Nickname', error);
    }
    
    @isTest
    static  void testCustomValidationError(){
        String error = CloneThisUserV3Controller.getErrorMessage(Statuscode.FIELD_CUSTOM_VALIDATION_EXCEPTION, 'Message', new List<String>{''}, 1, 'Stack');
        System.assertEquals('Custom Field Validation Failed: You cannot clone this user', error);
    }
    
    
    @isTest
    static  void testInvalidEmailError(){
        String error = CloneThisUserV3Controller.getErrorMessage(Statuscode.INVALID_EMAIL_ADDRESS, 'Message', new List<String>{''}, 1, 'Stack');
        System.assertEquals('Invalid Email Address', error);
    }

    @isTest
    static  void testrequiredFieldError(){
        String error = CloneThisUserV3Controller.getErrorMessage(Statuscode.REQUIRED_FIELD_MISSING, 'Message', new List<String>{''}, 1, 'Stack');
        System.assert(error.contains('A required field is missing'));
    }
    
    @isTest
    static  void testUnknownDmlErrorError(){
        String error = CloneThisUserV3Controller.getErrorMessage(StatusCode.INTERNAL_ERROR, 'Message', new List<String>{''}, 1, 'Stack');
        System.assert(error.contains('An unexpected error ocurred cloning this user'));
    }
    
    @isTest
    static void testgetUserCustomFields(){
		User user = createTestUser();
        try{
			CloneThisUserV3Controller.getUserCustomFields(user.Id);
			System.assert(true);
        }catch(Exception e){
            System.assert(false);
        }
    }

    @IsTest
    static void getUserStandardFieldsTest(){
        User user = createTestUser();
        Test.startTest();
        List<CloneThisUserV3Controller.CustomField> fields = CloneThisUserV3Controller.getUserStandardFields(user.Id); 
        System.assertEquals(false, fields.isEmpty());
        Test.stopTest();
        
    }

    @IsTest
    static void containsMiddleNameSuffixTest(){
        
        Test.startTest();
        Map<String,Boolean> fields = CloneThisUserV3Controller.containsMiddleNameSuffix();
        System.assertEquals(false, fields.keySet().isEmpty());
        Test.stopTest();
        
    }
    static void addUserToPset(User user, PermissionSet pset){
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId=user.Id, PermissionSetId=pset.Id);
        insert psa;
    }
    
    static PermissionSet createTestPermissionSet(){
        PermissionSet pset = new PermissionSet(Name='test', Label='test');
        insert pset;
		return pset;
    }
    
    static User createTestUser(){
        String profileName = 'Standard User';
        String lastName = 'clone';
        String firstName = 'clone';
        String userName = 'clone12345@clone12345.com';
        String email ='clone12345@clone12345.com';
        String alias = 'clonea';
        String localeSidKey = 'en_US';
        String emailEncodingKey = 'UTF-8';
        String languageLocaleKey = 'en_US';
        String timeZoneSidKey = 'America/New_York';
        
        Profile profile = [SELECT Id FROM profile WHERE Name=: profileName];
        User user = new User(ProfileId=profile.Id,LastName=lastName,FirstName=firstName,Username=userName,Email=email,Alias=alias,LocaleSidKey=localeSidKey,EmailEncodingKey=emailEncodingkey,LanguageLocaleKey=languageLocaleKey,TimeZoneSidKey=timeZoneSidKey);
        insert user;
        return user;
    }
    
    static void addUserToGroup(User user, Group grp){
        GroupMember gm = new GroupMember(UserOrGroupId = user.Id, GroupId = grp.Id);
        insert gm;
    }
    
    static Group createTestGroup(){
        Group test = new Group();
        test.Name = 'Test Group';
        insert test;
        return test;
    }
    
    static Group createTestQueue(){
        Group test = new Group();
        test.Name = 'Test Queue';
        test.Type = 'Queue';
        insert test;
        return test;
    }

    @isTest
    static void getGeneralOptionsFieldsTest() {
        List<ctu__General_Option_Value_Obj__c> setting = new List<ctu__General_Option_Value_Obj__c>{
            new ctu__General_Option_Value_Obj__c(Name='Generate new password', ctu__Data_Label__c='Generate new password and notify user immediately', ctu__Data_Category__c='General', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Chatter Group Memberships', ctu__Data_Label__c='Chatter Group Memberships', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Shared Dashboards', ctu__Data_Label__c='Shared Dashboards', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Permission Set Assignments', ctu__Data_Label__c='Permission Set Assignments', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Shared Reports', ctu__Data_Label__c='Shared Reports', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Queue Memberships', ctu__Data_Label__c='Queue Memberships', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Public Group Memberships', ctu__Data_Label__c='Public Group Memberships', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Permission Set License', ctu__Data_Label__c='Permission Set License Assignments', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Account Team Membership', ctu__Data_Label__c='Account Team Membership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Account Ownership', ctu__Data_Label__c='Account Ownership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Lead Ownership', ctu__Data_Label__c='Lead Ownership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Opportunity Ownership', ctu__Data_Label__c='Opportunity Ownership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Opportunity Team Membership', ctu__Data_Label__c='Opportunity Team Membership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Case Ownership', ctu__Data_Label__c='Case Ownership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            new ctu__General_Option_Value_Obj__c(Name='Advanced Case Team Membership', ctu__Data_Label__c='Case Team Membership', ctu__Data_Category__c='Advanced Options', ctu__Default_Value__c= true),
            new ctu__General_Option_Value_Obj__c(Name='Deactivate Original User', ctu__Data_Label__c='Deactivate Original User', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Chatter Group Memberships', ctu__Data_Label__c='Chatter Group Memberships', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Shared Dashboards', ctu__Data_Label__c='Shared Dashboards', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Permission Set Assignments', ctu__Data_Label__c='Permission Set Assignments', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Shared Reports', ctu__Data_Label__c='Shared Reports', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Queue Memberships', ctu__Data_Label__c='Queue Memberships', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Public Group Memberships', ctu__Data_Label__c='Public Group Memberships', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Permission Set License', ctu__Data_Label__c='Permission Set License Assignments', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Account Team Membership', ctu__Data_Label__c='Account Team Membership', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Opportunity Team Membership', ctu__Data_Label__c='Opportunity Team Membership', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false),
            new ctu__General_Option_Value_Obj__c(Name='Remove Case Team Membership', ctu__Data_Label__c='Case Team Membership', ctu__Data_Category__c='Remove Original User', ctu__Default_Value__c= false)
        };
        insert setting;

        Test.startTest();
        Map<String, Boolean> data= CloneThisUserV3Controller.getGeneralOptionsFields();
        Test.stopTest();

        List<ctu__General_Option_Value_Obj__c> result= [Select Id, Name From ctu__General_Option_Value_Obj__c];
        
        System.assertEquals(data.size(), result.size(), 'It should be the same size ');
    }
}