public with sharing class CloneTestLightningController {
    //public String search{get;set;}
   
    //public List<User> lstUser{get;set;}
    public Boolean showMsg {get;set;}
    public CloneTestLightningController(){
      /*  search = '';
        showMsg = false;
        lstUser = new List<User>();
        String query = ApexPages.CurrentPage().getParameters().get('q');
        String success = ApexPages.CurrentPage().getParameters().get('success');
        if(query != null){
            search = query;
            //Search();
        }
        if(success == '1'){
            showMsg = true;
        }*/
    }
    @AuraEnabled
    public static  list<User> getSearch(){
        List<User> lstUser = new List<User>();
        String search = 'test';
        if(search.contains('*')){
            search = search.replaceAll('\\*', '');
        }
        return lstUser = [SELECT Id, Name, UserName, UserRole.Name, Profile.Name, Profile.UserLicense.Name, isActive FROM User WHERE ( Name LIKE: '%' + search + '%' OR FirstName LIKE: '%' + search + '%' OR LastName LIKE: '%' + search + '%' OR Email LIKE: '%'+search+'%' ) ORDER BY LastName ASC];
    }
    @AuraEnabled
    public static  List<user> searchUsers(String search){
        system.debug(search);
        if(search.contains('*')){
            search = search.replaceAll('\\*', '');
        }
        return  [SELECT Id, Name, UserName, UserRole.Name, Profile.Name, Profile.UserLicense.Name, isActive FROM User WHERE ( Name LIKE: '%' + search + '%' OR FirstName LIKE: '%' + search + '%' OR LastName LIKE: '%' + search + '%' OR Email LIKE: '%'+search+'%' ) ORDER BY LastName ASC];
    }
}