public with sharing class CloneThisUserList{

    public class AccessibilityException extends Exception {}

    public String search{get;set;}
    public List<User> lstUser{get;set;}
    public Boolean showMsg {get;set;}
    public Boolean canClone {get;set;}
    public Boolean isSysAdmin {get;set;}
    public String permissionsMessage {get;set;}
    public CloneThisUserUtils.PackageInfo version {get;set;}
    
    public CloneThisUserList(){
        Map<String, CloneThisUserUtils.PermissionInfo> permissions = CloneThisUserUtils.getUserPermissions();
        canClone = permissions.get('PermissionsManageUsers').HasPermission;
        permissionsMessage = !canClone ? permissions.get('PermissionsManageUsers').Message : '';
        search = '';
        showMsg = false;
        isSysAdmin = CloneThisUserSettings.isSystemAdmin();
        lstUser = new List<User>();
        this.version = CloneThisUserUtils.getPackageVersion();
        String query = ApexPages.CurrentPage().getParameters().get('q');
        String success = ApexPages.CurrentPage().getParameters().get('success');
        if(query != null){
        	search = query;
        	Search();
        }
        if(success == '1'){
        	showMsg = true;
        }
    }
    
    public void Search(){
    	if(search.contains('*')){
    		search = search.replaceAll('\\*', '');
        }
        search = String.escapeSingleQuotes(search);
    	ApexPages.CurrentPage().getParameters().put('q', search);

        String exceptionMessage = 'You need access to the following field: ';
        if (!Schema.sObjectType.User.fields.isActive.isAccessible()){
            throw new AccessibilityException(exceptionMessage + 'User Is Active');
        }
        if (!Schema.sObjectType.User.fields.Name.isAccessible()){
            throw new AccessibilityException(exceptionMessage + 'Name');
        }
        if (!Schema.sObjectType.User.fields.UserName.isAccessible()){
            throw new AccessibilityException(exceptionMessage + 'User Name');
        }
        if (!Schema.sObjectType.UserRole.fields.Name.isAccessible()){
            throw new AccessibilityException(exceptionMessage + 'User Role Name');
        }
        if (!Schema.sObjectType.Profile.fields.Name.isAccessible()){
            throw new AccessibilityException(exceptionMessage + 'Profile Name');
        }
        if (!Schema.sObjectType.UserLicense.fields.Name.isAccessible()){
            throw new AccessibilityException(exceptionMessage + 'User License Name');
        }
        String query = '';
        if (Schema.sObjectType.User.isAccessible()){
            Boolean hasPortalEnabledField = User.getSobjectType().getDescribe().fields.getMap().keySet().contains('IsPortalEnabled');
            if(hasPortalEnabledField) {
                query = 'SELECT Id,Name,FirstName,SmallPhotoUrl,LastName,UserName,UserRole.Name,Profile.Name,Profile.UserLicense.Name,Email,CommunityNickname,Alias,isActive FROM User WHERE ( Name LIKE \'%' + search + '%\' OR FirstName LIKE \'%' + search + '%\' OR LastName LIKE \'%' + search + '%\' OR UserName LIKE \'%' + search + '%\'  OR Email LIKE \'%' + search + '%\' ) AND IsPortalEnabled = false ORDER BY LastName ASC LIMIT 100';
            }else{
                query = 'SELECT Id,Name,FirstName,SmallPhotoUrl,LastName,UserName,UserRole.Name,Profile.Name,Profile.UserLicense.Name,Email,CommunityNickname,Alias,isActive FROM User WHERE ( Name LIKE \'%' + search + '%\' OR FirstName LIKE \'%' + search + '%\' OR LastName LIKE \'%' + search + '%\' OR UserName LIKE \'%' + search + '%\'  OR Email LIKE \'%' + search + '%\' ) ORDER BY LastName ASC LIMIT 100';
            }
            lstUser = Database.query(query);
        }
    }

    public PageReference Settings(){
        PageReference ctuSettings = Page.CloneThisUserSettings;

        ctuSettings.setRedirect(true);

        return ctuSettings;
    }

    public PageReference goBack(){
        PageReference ctuList = Page.CloneThisUserList;

        ctuList.setRedirect(true);

        return ctuList;
    }
}