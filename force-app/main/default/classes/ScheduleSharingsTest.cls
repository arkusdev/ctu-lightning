@isTest
public with sharing class ScheduleSharingsTest {
    @isTest
    static void ScheduleSharings(){
        User adminUser = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];

        System.runAs(adminUser){
            Test.startTest();
                Test.setMock(HttpCalloutMock.class, new CalloutMock(adminUser.Id));
                CloneThisUserV3Controller.shareReportsAndDashboards(true, true, false, false, adminUser.Id, adminUser.Id);
            Test.stopTest();
        }
        System.assert(true);
    }

    @isTest
    static void GettersAndSettersTest(){
        ScheduleSharings.FolderInfo folderInfo = new ScheduleSharings.FolderInfo();
        folderInfo.id = 'Some Id';
        folderInfo.type = 'Some Type';
        folderInfo.name = 'Some name';
        folderInfo.label = 'Some label';
        folderInfo.namespace = 'Some namespace';

        ScheduleSharings.FolderShareBody folderShareBody = new ScheduleSharings.FolderShareBody();
        folderShareBody.accessType = 'Some access type';
        folderShareBody.shareType = 'Some Type';
        folderShareBody.shareWithId = 'Some Id';

        ScheduleSharings.FolderShareInfo folderShareInfo = new ScheduleSharings.FolderShareInfo();
        folderShareInfo.accessType = 'Some access type';
        folderShareInfo.sharedWithId = 'Some id';
        folderShareInfo.shareType = 'Some Type';
        folderShareInfo.shareId = 'Some id';
        folderShareInfo.imageColor = 'Some color';
        folderShareInfo.folderId = 'Some id';
        folderShareInfo.imageUrl = 'Some url';

        System.assertEquals('Some Id', folderInfo.id);
        System.assertEquals('Some Type', folderInfo.type);
        System.assertEquals('Some access type', folderShareBody.accessType);
        System.assertEquals('Some Type', folderShareBody.shareType);
        System.assertEquals('Some Id', folderShareBody.shareWithId);
        System.assertEquals('Some access type', folderShareInfo.accessType);
        System.assertEquals('Some id', folderShareInfo.sharedWithId);
        System.assertEquals('Some Type', folderShareInfo.shareType);
    }
}