public with sharing class BatchCreateData {
    public BatchCreateData() {

    }

    public static void createData(){
        //Create a user
        User user = new User(
            ProfileId = [Select id From Profile Where Name = 'Standard Platform User'].Id,
            LastName = 'UserForTest',
            Email = 'scriptusr2@mailtest.com',
            Username = 'scriptusr3@mailtest.com',
            CompanyName = 'Company Test',
            Title = 'Title Test',
            Alias = 'ScrptUs2', 
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        if (Schema.sObjectType.User.isCreateable()) {
            insert user;
        }
        //Assign a PermissionSetLicense to originalUser
        //PermissionSetLicenseAssign psla = new PermissionSetLicenseAssign(AssigneeId = user.Id, PermissionSetLicenseId = [Select Id From PermissionSetLicense Where DeveloperName = 'SalesConsoleUser'].Id);
        //insert psla;
        
        //Create PermissionSets
        List<PermissionSet> listPermissionSet = new List<PermissionSet>();
        for(Integer i = 0; i < 30; i++){
            PermissionSet ps = new PermissionSet(Label = 'Script Permission Set Test' + i, Name = 'Script_Permission_Set_Test' + i);
            listPermissionSet.add(ps);
        }
        if (Schema.sObjectType.PermissionSet.isCreateable()) {
            insert listPermissionSet;
        }

        //Assign each permission set to the user
        List<PermissionSet> permissionSetToAssign = [Select Id From PermissionSet Where Name LIKE 'Script_Permission_Set_Test%'];
        List<PermissionSetAssignment> permissionSetToInsert = new List<PermissionSetAssignment>();
        for(PermissionSet p : permissionSetToAssign){
            PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = user.Id, PermissionSetId = p.Id);
            permissionSetToInsert.add(psa);
        }
        if (Schema.sObjectType.PermissionSetAssignment.isCreateable()) {
            insert permissionSetToInsert;
        }

        //Create the Groups, Regular and Queue
        List<Group> groups = new List<Group>();
        for(Integer i = 0; i < 30; i++){
            Group rg = new Group(Name = 'Script Regular Group Test' + i, Type = 'Regular');
            groups.add(rg);
        }
        for(Integer i = 0; i < 30; i++){
            Group qg = new Group(Name = 'Script Queue Group Test' + i, Type = 'Queue');
            groups.add(qg);
        }
        if (Schema.sObjectType.Group.isCreateable()) {
            insert groups;
        }

        //Assign each group to the user
        List<Group> groupsToAssign = [Select Id From Group Where Name Like 'Script Regular Group Test%' OR Name Like 'Script Queue Group Test%'];
        List<GroupMember> groupMemberToInsert = new List<GroupMember>();
        for(Group g : groupsToAssign){
            GroupMember gm = new GroupMember(UserOrGroupId = user.Id, GroupId = g.Id);
            groupMemberToInsert.add(gm);
        }
        if (Schema.sObjectType.GroupMember.isCreateable()) {
            insert groupMemberToInsert;
        }
        /*
        //Create another user that will be the cloned
        User clone = new User(
            ProfileId = [Select id From Profile Where Name = 'Standard Platform User'].Id,
            LastName = 'UserForTest',
            Email = 'scriptusr@mailtest.com',
            Username = 'scriptusrc@mailtest.com',
            CompanyName = 'Company Test',
            Title = 'Title Test',
            Alias = 'ScrptUsr', 
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        insert clone;
        */
    }
}