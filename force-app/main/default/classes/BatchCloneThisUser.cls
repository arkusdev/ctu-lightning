global with sharing class BatchCloneThisUser implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    //Store the Id of the clone
    private String userId;
    //Store the Id of the original user to clone
    private String toCloneId;
    //Store the type (PermissionSetLicenseAssign, PermissionSetAssignment, GroupMember)
    private String type;
    private List<ctu__CTU_Mass_Clone__c> listCTU;
    private Map<String, Boolean> removeOriginalFromType;

    private Map<String, List<String>> listOfOriginalIdRecords;
    private Map<String, String> errorMap;

    private Id userProfileId = [SELECT ProfileId FROM User WHERE Id =: UserInfo.getUserId()].ProfileId;

    public BatchCloneThisUser(String userId, String toCloneId, String type, List<ctu__CTU_Mass_Clone__c> listCTU, Map<String, Boolean> removeOriginalFromType) {
        this.userId = userId;
        this.toCloneId = toCloneId;
        this.type = type;
        this.listCTU = listCTU;
        this.removeOriginalFromType = removeOriginalFromType;
        this.listOfOriginalIdRecords= new Map<String, List<String>>();
        this.errorMap= new Map<String, String>();
    }

    global Database.QueryLocator start(Database.BatchableContext bc){
        //Create a query based on the type passed
        String query;
        if(type == 'PermissionSetLicenseAssign'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Permission Set License Assignment'){
                    query = 'SELECT PermissionSetLicenseId FROM PermissionSetLicenseAssign WHERE AssigneeId = \'' + toCloneId + '\'';
                }
            }
        }
        if(type == 'PermissionSetAssignment'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Permission Set Assignment'){
                    query = 'Select AssigneeId, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment WHERE PermissionSet.IsOwnedByProfile = false AND AssigneeId = \'' + toCloneId + '\'';
                }
            }
        }
        if(type == 'GroupMemberQueue'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Queue Membership'){
                    query = 'Select UserOrGroupId, GroupId, Group.Type FROM GroupMember WHERE UserOrGroupId = \'' + toCloneId + '\' AND Group.Type = \'Queue\'';
                }
            }
        }
        if(type == 'PublicGroupMembership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Public Group Membership'){
                    query = 'Select UserOrGroupId, GroupId, Group.Type FROM GroupMember WHERE UserOrGroupId = \'' + toCloneId + '\' AND Group.Type <> \'Queue\'';
                }
            }
        }
        if(type == 'ChatterGroup'){
            Boolean isChatterEnabled = CloneThisUserUtils.chatterEnabled();
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Chatter Group' && isChatterEnabled){
                    query = 'SELECT CollaborationGroupId FROM CollaborationGroupMember WHERE MemberId = \'' + toCloneId + '\'';
                }
            }
        }
        if(type == 'accountOwnership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Account Ownership' && CloneThisUserUtils.getObjectPermissions('Account', 'PermissionsEdit', userProfileId)){
                    query = 'SELECT Id, OwnerId FROM Account WHERE OwnerId = \'' + toCloneId + '\'';
                }else { 
                    this.errorMap.put('accountOwnership', 'User has no permission to change Account Ownership');
                }
            }
        }
        if(type == 'leadOwnership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Lead Ownership' && CloneThisUserUtils.getObjectPermissions('Lead', 'PermissionsEdit', userProfileId)){
                    query = 'SELECT OwnerId FROM Lead WHERE OwnerId = \'' + toCloneId + '\'';
                } else { 
                    this.errorMap.put('leadOwnership', 'User has no permission to change Lead Ownership');
                }
            }
        }
        if(type == 'opportunityOwnership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Opportunity Ownership' && CloneThisUserUtils.getObjectPermissions('Opportunity', 'PermissionsEdit', userProfileId)){
                    query = 'SELECT OwnerId FROM Opportunity WHERE OwnerId = \'' + toCloneId + '\'';
                } else { 
                    this.errorMap.put('opportunityOwnership', 'User has no permission to change Opportunity Ownership');
                }
            }
        }
        if(type == 'caseOwnership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Case Ownership' && CloneThisUserUtils.getObjectPermissions('Case', 'PermissionsEdit', userProfileId)){
                    query = 'SELECT OwnerId FROM Case WHERE OwnerId = \'' + toCloneId + '\'';
                } else { 
                    this.errorMap.put('caseOwnership', 'User has no permission to change Case Ownership');
                }
            }
        }
        if(type == 'caseTeamMembership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Case Team Membership' && CloneThisUserUtils.getObjectPermissions('Case', 'PermissionsEdit', userProfileId)){
                    query = 'SELECT MemberId, ParentId, TeamRoleId FROM CaseTeamMember WHERE MemberId = \'' + toCloneId + '\'';
                } else {
                    this.errorMap.put('caseTeamMembership', 'User has no permission to create Case Teams');
                }
            }
        }
        if(type == 'opportunityTeamMembership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Opportunity Team Membership' && (Boolean)Schema.getGlobalDescribe().containsKey('UserTeamMember')){
                    query =  'SELECT Id, OpportunityAccessLevel, OwnerId, TeamMemberRole, UserId FROM UserTeamMember WHERE OwnerId = \'' + toCloneId + '\'';
                }
            }
        }
        if(type == 'opportunityTeam'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Opportunity Team' && (Boolean)Schema.getGlobalDescribe().containsKey('OpportunityTeamMember')){
                    query =  'SELECT Id, OpportunityAccessLevel, OpportunityId, TeamMemberRole, UserId ' + (Schema.getGlobalDescribe().containsKey('CurrencyType') ? ', CurrencyIsoCode ' : '') + ' FROM OpportunityTeamMember WHERE UserId = \'' + toCloneId + '\'';
                }
            }
        }
        if(type == 'accountTeamMembership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Account Team Membership' && (Boolean)Schema.getGlobalDescribe().containsKey('UserAccountTeamMember')){
                    query = 'SELECT Id, AccountAccessLevel, CaseAccessLevel, OpportunityAccessLevel, TeamMemberRole, UserId, OwnerId FROM UserAccountTeamMember WHERE OwnerId = \'' + toCloneId + '\'';
                }
            }
        }
        if(type == 'accountTeam'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Account Team' && (Boolean)Schema.getGlobalDescribe().containsKey('AccountTeamMember')){
                    query =  'SELECT Id, AccountAccessLevel, AccountId, CaseAccessLevel, ContactAccessLevel, OpportunityAccessLevel, TeamMemberRole, UserId ' + (Schema.getGlobalDescribe().containsKey('CurrencyType') ? ', CurrencyIsoCode ' : '') + ' FROM AccountTeamMember WHERE UserId = \'' + toCloneId + '\'';
                }
            }
        }

        if(query == null){
            query = 'Select Id From ctu__CTU_Mass_Clone__c Where ctu__Type__c = \'NotExists\'';
        }
        return Database.getQueryLocator(query);
    }


    global void execute(Database.BatchableContext bc, List<sObject> scope){
        Boolean removeOriginal = this.removeOriginalFromType.get(type);
        //Check which type is and create the records
        if(type == 'PermissionSetLicenseAssign'){
            //Cast the generic sObject
            List<PermissionSetLicenseAssign> userPermissionSLA = (List<PermissionSetLicenseAssign>)scope;
            //Create a list to store the permissions
            List<PermissionSetLicenseAssign> pSetLicenseAssignmentInsert = new List<PermissionSetLicenseAssign>();
            //For each permission of User add to the clone
            for(PermissionSetLicenseAssign ps : userPermissionSLA){
                //Create the permission with the cloned user Id and the permission set licence Id and add to the list
                PermissionSetLicenseAssign psNew = new PermissionSetLicenseAssign(AssigneeId = userId, PermissionSetLicenseId = ps.PermissionSetLicenseId);
                pSetLicenseAssignmentInsert.add(psNew);
            }
            if (Schema.sObjectType.PermissionSetLicenseAssign.isCreateable()) {
                insert pSetLicenseAssignmentInsert;
            }

            if (removeOriginal && Schema.sObjectType.PermissionSetLicenseAssign.isDeletable()){
                delete userPermissionSLA;
            }
        }
        if(type == 'PermissionSetAssignment'){
            //Cast the generic sObject
            List<PermissionSetAssignment> userPermissionSA = (List<PermissionSetAssignment>)scope;
            //Create a list to store the permissions
            List<PermissionSetAssignment> pSetAssignmentInsert = new List<PermissionSetAssignment>();
            //For each permission of the original User add it to the clone
            for(PermissionSetAssignment psa : userPermissionSA){
                //Create the permission and add it to the list
                PermissionSetAssignment psaNew = new PermissionSetAssignment(AssigneeId = userId, PermissionSetId = psa.PermissionSetId);
                pSetAssignmentInsert.add(psaNew);
            }
            if (Schema.sObjectType.PermissionSetAssignment.isCreateable()) {
                insert pSetAssignmentInsert;
            }

            if (removeOriginal && Schema.sObjectType.PermissionSetAssignment.isDeletable()){
                delete userPermissionSA;
            }
        }
        if(type == 'GroupMemberQueue'){
            //Cast the generic sObject
            List<GroupMember> userGroupMember = (List<GroupMember>)scope;
            //Create a list to store the permissions
            List<GroupMember>  gmInsert = new List<GroupMember>();
            //For each group member of the original user add it to the clone
            if(GroupMember.sObjectType.getDescribe().isCreateable() && Schema.sObjectType.GroupMember.fields.GroupId.isCreateable() && Schema.sObjectType.GroupMember.fields.UserOrGroupId.isCreateable()){
                for(GroupMember gm : userGroupMember){
                    //Create the groupmember and add it to the list
                    GroupMember gmNew = new GroupMember(UserOrGroupId = userId, GroupId = gm.GroupId);
                    gmInsert.add(gmNew);
                }
                if (Schema.sObjectType.GroupMember.isCreateable()) {
                    insert gmInsert;
                }

                if (removeOriginal){
                    if (GroupMember.sObjectType.getDescribe().isDeletable()){
                        delete userGroupMember;
                    }
                }
            } 
        }
        if(type == 'PublicGroupMembership'){
            //Cast the generic sObject
            List<GroupMember> userGroupMemberPublic = (List<GroupMember>)scope;
            //Create a list to store the permissions
            List<GroupMember>  gmpInsert = new List<GroupMember>();
            //For each group member of the original user add it to the clone
            if(GroupMember.sObjectType.getDescribe().isCreateable() && Schema.sObjectType.GroupMember.fields.GroupId.isCreateable() && Schema.sObjectType.GroupMember.fields.UserOrGroupId.isCreateable()){
                for(GroupMember gmp : userGroupMemberPublic){
                    //Create the groupmember and add it to the list
                    GroupMember gmpNew = new GroupMember(UserOrGroupId = userId, GroupId = gmp.GroupId);
                    gmpInsert.add(gmpNew);
                }
                if (Schema.sObjectType.GroupMember.isCreateable()) {
                    insert gmpInsert;
                }

                if (removeOriginal){
                    if (GroupMember.sObjectType.getDescribe().isDeletable()){
                        delete userGroupMemberPublic;
                    }
                }
            }
        }
        if(type == 'ChatterGroup'){
            Boolean isChatterEnabled = CloneThisUserUtils.chatterEnabled();
            if(isChatterEnabled){
                List<sObject> userChatterGroups = scope;
                List<sObject> gms = new List<sObject>();
                for(sObject gm : userChatterGroups){
                    sObject gmNew = Schema.getGlobalDescribe().get('CollaborationGroupMember').newSObject();
                    gmNew.put('MemberId', userId);
                    gmNew.put('CollaborationGroupId', gm.get('CollaborationGroupId'));
                    gms.add(gmNew);
                }
                if (!gms.isEmpty()){
                    SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.CREATABLE, gms);
                    insert securityDecision.getRecords();
                }
                if (removeOriginal){
                    String objectStr = 'CollaborationGroupMember';
                    Schema.SObjectType convertType = Schema.getGlobalDescribe().get(objectStr);
                    Schema.DescribeSObjectResult cgmSObj = convertType.getDescribe();
                    if (cgmSObj.isDeletable()){
                        delete userChatterGroups;
                    }
                }
            }
        }
        if(type == 'accountOwnership'){
            this.errorMap= new Map<String, String>();
            for(sObject ar : scope){
                ar.put('OwnerId', userId);
            }
            if (!scope.isEmpty()){
                Update scope;
            }
        }
        if(type == 'leadOwnership'){
            this.errorMap= new Map<String, String>();
            for(sObject ar : scope){
                ar.put('OwnerId', userId);
            }
            if (!scope.isEmpty()){
                Update scope;
            }
        }
        if(type == 'opportunityOwnership'){
            this.errorMap= new Map<String, String>();
            for(sObject ar : scope){
                ar.put('OwnerId', userId);
            }
            if (!scope.isEmpty()){
                Update scope;
            }
        }
        if(type == 'caseOwnership'){
            this.errorMap= new Map<String, String>();
            for(sObject ar : scope){
                ar.put('OwnerId', userId);
            }
            if (!scope.isEmpty()){
                Update scope;
            }
        }
        if(type == 'caseTeamMembership'){
            try {
                this.errorMap= new Map<String, String>();
                List<SObject> clonedRecords= new List<SObject>();
                List<String> ids = new List<String>();
                for(SObject record: scope){
                    Schema.SObjectType sObjType  = Schema.getGlobalDescribe().get('CaseTeamMember');
                    SObject recordCloned = sObjType.newSObject();
                    recordCloned.put('MemberId', userId);
                    recordCloned.put('ParentId', String.valueOf(record.get('ParentId')));
                    recordCloned.put('TeamRoleId', String.valueOf(record.get('TeamRoleId')));
                    clonedRecords.add(recordCloned);
                }
                if (!clonedRecords.isEmpty()){
                    Insert clonedRecords;
                }
                if (removeOriginal){
                    delete scope;
                }
            } catch (Exception e) {
                this.errorMap.put('caseTeamMembership', 'User has no permission to create Case Teams');
            }
        }
        if(type == 'opportunityTeamMembership'){
            this.listOfOriginalIdRecords = new Map<String, List<String>>();
            if (!scope.isEmpty() && (Boolean)Schema.getGlobalDescribe().containsKey('UserTeamMember')) {
                List<String> ids = new List<String>();
                for(SObject record: scope) {
                    if(removeOriginal) {
                        ids.add(record.Id);
                    }
                }
                List<SObject> clonedRecords= CloneThisUserUtils.createDynamicObjects('UserTeamMember', 'OwnerId', new List<String>{'OpportunityAccessLevel', 'OwnerId', 'TeamMemberRole', 'UserId'}, userId, scope);
                if(!clonedRecords.isEmpty()){
                    CloneThisUserUtils.insertSObjectThroughApi(clonedRecords, 'UserTeamMember');
                }
                if(!ids.isEmpty()) {
                    this.listOfOriginalIdRecords.put('UserTeamMember', ids);
                }
            }
        }
        if(type == 'opportunityTeam'){
            this.listOfOriginalIdRecords = new Map<String, List<String>>();
            if (!scope.isEmpty() && (Boolean)Schema.getGlobalDescribe().containsKey('OpportunityTeamMember')){
                List<String> ids = new List<String>();
                for(SObject record: scope){
                    if(removeOriginal) {
                        ids.add(record.Id);
                    }
                }
                List<String> fields = new List<String>{'OpportunityAccessLevel', 'OpportunityId',  'UserId', 'TeamMemberRole'};
                if(Schema.getGlobalDescribe().containsKey('CurrencyType')) {
                    fields.add('CurrencyIsoCode');
                }
                List<SObject> clonedRecords= CloneThisUserUtils.createDynamicObjects('OpportunityTeamMember', 'UserId', fields, userId, scope);
                if(!clonedRecords.isEmpty()){
                    CloneThisUserUtils.insertSObjectThroughApi(clonedRecords, 'OpportunityTeamMember');
                }
                if(!ids.isEmpty()) {
                    this.listOfOriginalIdRecords.put('OpportunityTeamMember', ids);
                }
            }
        }
        if(type == 'accountTeamMembership'){
            this.listOfOriginalIdRecords = new Map<String, List<String>>();
            if (!scope.isEmpty() && (Boolean)Schema.getGlobalDescribe().containsKey('UserAccountTeamMember')) {
                List<String> ids = new List<String>();
                for(SObject record: scope) {
                    if(removeOriginal) {
                        ids.add(record.Id);
                    }
                }
                List<SObject> clonedRecords= CloneThisUserUtils.createDynamicObjects('UserAccountTeamMember', 'OwnerId', new List<String>{'AccountAccessLevel', 'CaseAccessLevel', 'OpportunityAccessLevel', 'OwnerId', 'TeamMemberRole', 'UserId'}, userId, scope);
                if(!clonedRecords.isEmpty()){
                    CloneThisUserUtils.insertSObjectThroughApi(clonedRecords, 'UserAccountTeamMember');
                }
                if(!ids.isEmpty()) {
                    this.listOfOriginalIdRecords.put('UserAccountTeamMember', ids);
                }
            }
        }
        if(type == 'accountTeam'){
            this.listOfOriginalIdRecords = new Map<String, List<String>>();
            if (!scope.isEmpty() && (Boolean)Schema.getGlobalDescribe().containsKey('AccountTeamMember')){
                List<String> ids = new List<String>();
                for(SObject record: scope){
                    if(removeOriginal) {
                        ids.add(record.Id);
                    }
                }
                List<String> fields = new List<String>{'AccountAccessLevel', 'AccountId',  'CaseAccessLevel', 'ContactAccessLevel', 'OpportunityAccessLevel', 'TeamMemberRole', 'UserId'};
                if(Schema.getGlobalDescribe().containsKey('CurrencyType')) {
                    fields.add('CurrencyIsoCode');
                }
                List<SObject> clonedRecords= CloneThisUserUtils.createDynamicObjects('AccountTeamMember', 'UserId', fields, userId, scope);
                if(!clonedRecords.isEmpty()){
                    CloneThisUserUtils.insertSObjectThroughApi(clonedRecords, 'AccountTeamMember');
                }
                if(!ids.isEmpty()) {
                    this.listOfOriginalIdRecords.put('AccountTeamMember', ids);
                }
            }
        }
    }

    global void finish(DataBase.BatchableContext bc){
        String newUserName = [SELECT Name FROM User Where Id =: userId].Name;
        String subjectMessage = '';
        String bodyMessage = '';
        if(type == 'PermissionSetLicenseAssign'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Permission Set License Assignment'){
                    subjectMessage = 'Clone This User: Permission Set License Assignments completed for ' + newUserName;
                    bodyMessage = 'Permission Set License Assignments have been completed for ' + newUserName;
                }
            }
            BatchCloneThisUser batchPermSetAssignment = new BatchCloneThisUser(userId, toCloneId, 'PermissionSetAssignment', listCTU, this.removeOriginalFromType);
            Id batchIdPermSetAssignment = Database.executeBatch(batchPermSetAssignment, 25);
        }
        if(type == 'PermissionSetAssignment'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Permission Set Assignment'){
                    subjectMessage = 'Clone This User: Permission Set Assignments completed for ' + newUserName;
                    bodyMessage = 'Permission Set Assignments have been completed for ' + newUserName;
                }
            }
            BatchCloneThisUser batchGroupMember = new BatchCloneThisUser(userId, toCloneId, 'GroupMemberQueue', listCTU, this.removeOriginalFromType);
            Id batchIdGroupMember = Database.executeBatch(batchGroupMember, 25); 
        }
        if(type == 'GroupMemberQueue'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Queue Membership'){
                    subjectMessage = 'Clone This User: Queue Membership completed for ' + newUserName;
                    bodyMessage = 'Queue Membership have been completed for ' + newUserName; 
                }
            }
            BatchCloneThisUser batchGroupMemberPublic = new BatchCloneThisUser(userId, toCloneId, 'PublicGroupMembership', listCTU, this.removeOriginalFromType);
            Id batchIdGroupMember = Database.executeBatch(batchGroupMemberPublic, 25);
        }
        if(type == 'PublicGroupMembership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Public Group Membership'){
                    subjectMessage = 'Clone This User: Public Group Membership completed for ' + newUserName;
                    bodyMessage = 'Public Group Membership have been completed for ' + newUserName;
                }
            }
            BatchCloneThisUser batchChatterGroups = new BatchCloneThisUser(userId, toCloneId, 'ChatterGroup', listCTU, this.removeOriginalFromType);
            Id batchIdGroupMember = Database.executeBatch(batchChatterGroups, 25);
        }
        if(type == 'ChatterGroup'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Chatter Group'){
                    subjectMessage = 'Clone This User: Chatter Group completed for ' + newUserName;
                    bodyMessage = 'Chatter Group have been completed for ' + newUserName;
                }
            }
            BatchCloneThisUser batchChatterGroups = new BatchCloneThisUser(userId, toCloneId, 'accountOwnership', listCTU, this.removeOriginalFromType);
            Id batchIdGroupMember = Database.executeBatch(batchChatterGroups, 25);
        }
        if(type == 'accountOwnership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Account Ownership') { 
                    if(!this.errorMap.containsKey('accountOwnership')) {
                        subjectMessage = 'Clone This User: Account Ownership completed for ' + newUserName;
                        bodyMessage = 'Account Ownership have been completed for ' + newUserName;
                    } else {
                        subjectMessage = 'Clone This User: Error on Account Ownership for ' + newUserName;
                        bodyMessage = 'Account Ownership have been not completed for ' + newUserName + '. Error (' + this.errorMap.get('accountOwnership') + ')';
                    }
                }
            }
            BatchCloneThisUser batchLeadOwnership = new BatchCloneThisUser(userId, toCloneId, 'leadOwnership', listCTU, this.removeOriginalFromType);
            Id batchIdLeadOwnership = Database.executeBatch(batchLeadOwnership, 25);
        }
        if(type == 'leadOwnership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Lead Ownership') { 
                    if(!this.errorMap.containsKey('leadOwnership')) {
                        subjectMessage = 'Clone This User: Lead Ownership completed for ' + newUserName;
                        bodyMessage = 'Lead Ownership have been completed for ' + newUserName;
                    } else {
                        subjectMessage = 'Clone This User: Error on Lead Ownership for ' + newUserName;
                        bodyMessage = 'Lead Ownership have been not completed for ' + newUserName + '. Error (' + this.errorMap.get('leadOwnership') + ')';
                    }
                }
            }
            BatchCloneThisUser batchOpptOwner = new BatchCloneThisUser(userId, toCloneId, 'opportunityOwnership', listCTU, this.removeOriginalFromType);
            Id batchIdOpptOwner = Database.executeBatch(batchOpptOwner, 25);
        }
        if(type == 'opportunityOwnership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Opportunity Ownership') { 
                    if(!this.errorMap.containsKey('opportunityOwnership')) {
                        subjectMessage = 'Clone This User: Opportunity Ownership completed for ' + newUserName;
                        bodyMessage = 'Opportunity Ownership have been completed for ' + newUserName;
                    } else {
                        subjectMessage = 'Clone This User: Error on Opportunity Ownership for ' + newUserName;
                        bodyMessage = 'Opportunity Ownership have been not completed for ' + newUserName + '. Error (' + this.errorMap.get('opportunityOwnership') + ')';
                    }
                }
            }
            BatchCloneThisUser batchCaseOwner = new BatchCloneThisUser(userId, toCloneId, 'caseOwnership', listCTU, this.removeOriginalFromType);
            Id batchIdCaseOwner = Database.executeBatch(batchCaseOwner, 25);
        }
        if(type == 'caseOwnership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Case Ownership') { 
                    if(!this.errorMap.containsKey('caseOwnership')) {
                        subjectMessage = 'Clone This User: Case Ownership completed for ' + newUserName;
                        bodyMessage = 'Case Ownership have been completed for ' + newUserName;
                    } else {
                        subjectMessage = 'Clone This User: Error on Case Ownership for ' + newUserName;
                        bodyMessage = 'Case Ownership have been not completed for ' + newUserName + '. Error (' + this.errorMap.get('caseOwnership') + ')';
                    }
                }
            }
            BatchCloneThisUser batchOpptTM = new BatchCloneThisUser(userId, toCloneId, 'caseTeamMembership', listCTU, this.removeOriginalFromType);
            Id batchIdOpptOwner = Database.executeBatch(batchOpptTM, 25);
        }
        if(type == 'caseTeamMembership'){
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Case Team Membership') { 
                    if(!this.errorMap.containsKey('caseTeamMembership')) {
                        subjectMessage = 'Clone This User: Case Team Membership completed for ' + newUserName;
                        bodyMessage = 'Case Team Membership have been completed for ' + newUserName;
                    } else {
                        subjectMessage = 'Clone This User: Error on Case Team Membership for ' + newUserName;
                        bodyMessage = 'Case Team Membership have been not completed for ' + newUserName + '. Error (' + this.errorMap.get('caseTeamMembership') + ')';
                    }
                }
            }
            BatchCloneThisUser batchRun = new BatchCloneThisUser(this.userId, this.toCloneId, 'opportunityTeamMembership', this.listCTU, this.removeOriginalFromType);
            Id batchIdRun = Database.executeBatch(batchRun, 25);
        }
        if(type == 'opportunityTeamMembership'){
            Boolean isOpportunityTeamMembershipEnabled = (Boolean)Schema.getGlobalDescribe().containsKey('UserTeamMember');
            if(isOpportunityTeamMembershipEnabled && this.removeOriginalFromType.get(type) && this.listOfOriginalIdRecords.containsKey('UserTeamMember') && !Test.isRunningTest()) {
                CloneThisUserUtils.deleteSObjectThroughApi(this.listOfOriginalIdRecords.get('UserTeamMember'));
            }
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Opportunity Team Membership' && isOpportunityTeamMembershipEnabled){
                    subjectMessage = 'Clone This User: Opportunity Team Membership completed for ' + newUserName;
                    bodyMessage = 'Opportunity Team Membership have been completed for ' + newUserName;
                }
            }
            
            BatchCloneThisUser batchRun = new BatchCloneThisUser(this.userId, this.toCloneId, 'opportunityTeam', this.listCTU, this.removeOriginalFromType);
            Id batchIdRun = Database.executeBatch(batchRun, 25);
        }
        if(type == 'opportunityTeam'){
            Boolean isOpportunityTeamEnabled = (Boolean)Schema.getGlobalDescribe().containsKey('OpportunityTeamMember');
            if(isOpportunityTeamEnabled && this.removeOriginalFromType.get(type) && this.listOfOriginalIdRecords.containsKey('OpportunityTeamMember') && !Test.isRunningTest()) {
                CloneThisUserUtils.deleteSObjectThroughApi(this.listOfOriginalIdRecords.get('OpportunityTeamMember'));
            }
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Opportunity Team' && isOpportunityTeamEnabled){
                    subjectMessage = 'Clone This User: Opportunity Team completed for ' + newUserName;
                    bodyMessage = 'Opportunity Team have been completed for ' + newUserName;
                }
            }
            BatchCloneThisUser batchRun = new BatchCloneThisUser(this.userId, this.toCloneId, 'accountTeamMembership', this.listCTU, this.removeOriginalFromType);
            Id batchIdRun = Database.executeBatch(batchRun, 25);
        }
        if(type == 'accountTeamMembership'){
            Boolean isAccountTeamMembershipEnabled = (Boolean)Schema.getGlobalDescribe().containsKey('UserAccountTeamMember');
            if(isAccountTeamMembershipEnabled && this.removeOriginalFromType.get(type) && this.listOfOriginalIdRecords.containsKey('UserAccountTeamMember') && !Test.isRunningTest()) {
                CloneThisUserUtils.deleteSObjectThroughApi(this.listOfOriginalIdRecords.get('UserAccountTeamMember'));
            }
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Account Team Membership' && isAccountTeamMembershipEnabled){
                    subjectMessage = 'Clone This User: Account Team Membership completed for ' + newUserName;
                    bodyMessage = 'Account Team Membership have been completed for ' + newUserName;
                }
            }
            
            BatchCloneThisUser batchRun = new BatchCloneThisUser(this.userId, this.toCloneId, 'accountTeam', this.listCTU, this.removeOriginalFromType);
            Id batchIdRun = Database.executeBatch(batchRun, 25);
        }
        if(type == 'accountTeam'){
            Boolean isAccountTeamEnabled = (Boolean)Schema.getGlobalDescribe().containsKey('AccountTeamMember');
            if(isAccountTeamEnabled && this.removeOriginalFromType.get(type) && this.listOfOriginalIdRecords.containsKey('AccountTeamMember') && !Test.isRunningTest()) {
                CloneThisUserUtils.deleteSObjectThroughApi(this.listOfOriginalIdRecords.get('AccountTeamMember'));
            }
            for(ctu__CTU_Mass_Clone__c ctmc : listCTU){
                if(ctmc.ctu__Type__c == 'Account Team' && isAccountTeamEnabled){
                    subjectMessage = 'Clone This User: Account Team completed for ' + newUserName;
                    bodyMessage = 'Account Team have been completed for ' + newUserName;
                }
            }
        }

        if(!String.isBlank(subjectMessage) && !String.isBlank(bodyMessage)){
            try {
                SendSingleEmail.send(subjectMessage, bodyMessage);
            } catch (EmailException e) {
                System.debug(e);
            }
        }
    }
}