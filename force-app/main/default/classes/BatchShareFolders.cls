public with sharing class BatchShareFolders implements Database.Batchable<Id>, Database.AllowsCallouts, Database.Stateful {
    
    private Map<Id, ScheduleSharings.FolderSharesBody> newSharings { set; get;}
    private String endpoint { set; get; }
    private String sessionId { set; get; }
    private String schedulableId { set; get; }

    public BatchShareFolders(Map<Id, ScheduleSharings.FolderSharesBody> pNewSharings, String pEndpoint, String sessionId, String pSchedulableId) {
        this.newSharings = pNewSharings;
        this.endpoint = pEndpoint;
        this.sessionId = sessionId;
        this.schedulableId = pSchedulableId;
    }

    public List<Id> start(Database.BatchableContext bc){
        List<Id> idsList = new List<Id>();
        idsList.addAll(this.newSharings.keySet());
        return idsList;
    }

    public void execute(Database.BatchableContext bc, List<Id> foldersIds){
        for (Id folderId : foldersIds) {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            String endPointShareFolders = this.endpoint + '/' + folderId + '/shares';
            request.setEndpoint(endPointShareFolders);
            request.setMethod('PUT');
            request.setHeader('Authorization', 'Bearer ' + this.sessionId);
            request.setHeader('Content-Type', 'application/json');

            //String body = reqBody(foldersToShare.get(folderId));
            request.setBody(JSON.serialize(this.newSharings.get(folderId)));
            request.setTimeout(120000);
            
            http.send(request);
        }
    }

    public void finish(DataBase.BatchableContext bc){
        system.abortJob(this.schedulableId);
    }
}