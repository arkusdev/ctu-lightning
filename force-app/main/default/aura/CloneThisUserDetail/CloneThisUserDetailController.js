({
    init: function (cmp, evt, helper) {
        var pageRef = cmp.get("v.pageReference");
        if (pageRef) {
            var results = pageRef.state.ctu__g;
            var resultsDecoded = decodeURIComponent(escape((atob(results))));
            cmp.set("v.results", JSON.parse(resultsDecoded));
        }
    },
    reInit: function (cmp, evt, helper) {
        $A.get('e.force:refreshView').fire();
    },
    
    handleBack: function (cmp) {
        cmp.find("navigationService").navigate({
            type: 'standard__component',
            attributes: {
                componentName: 'ctu__cloneThisUser',
            }
        },
        true);
    },

    cloneAgain: function (cmp, evt) {
        var userid = evt.getParam('id');
        var permissionSetAssignments = evt.getParam('permissionSetAssignments');
        var queueMembership = evt.getParam('queueMembership');
        var publicGroupMembership = evt.getParam('publicGroupMembership');
        var permissionSetLicenseAssignments = evt.getParam('permissionSetLicenseAssignments');
        var chatterGroups = evt.getParam('chatterGroups');
        var generatePassword = evt.getParam('generatePassword');

        cmp.find("navigationService").navigate({
            type: 'standard__component',
            attributes: {
                componentName: 'ctu__cloneThisUserForm',
            },
            state: {
                "ctu__userId": userid,
                "ctu__permissionSetAssignments": permissionSetAssignments,
                "ctu__queueMembership": queueMembership,
                "ctu__publicGroupMembership": publicGroupMembership,
                "ctu__permissionSetLicenseAssignments": permissionSetLicenseAssignments,
                "ctu__chatterGroups": chatterGroups,
                "ctu__generatePassword": generatePassword
            }
        },
        true);
    }
})