({
    init: function (cmp, event, helper) {
        var isSalesforceNative = (window.navigator.userAgent.toLowerCase().indexOf('salesforce1') != -1);
        cmp.set("v.sfone", isSalesforceNative);
        var pageRef = cmp.get("v.pageReference");
        if (pageRef) {
            var search = pageRef.state.ctu__search;
            cmp.set("v.search", search);
        }
    },
    
    scrollToTop : function(component, event, helper) {
        if (component.get('v.sfone')){   
            $A.get('e.force:refreshView').fire();
            $A.get("e.force:closeQuickAction").fire();
        }
	},

    handleUserSelected: function (cmp, evt) {
        var userid = evt.getParam('id');
        cmp.find("navigationService").navigate({
            type: 'standard__component',
            attributes: {
                componentName: 'ctu__cloneThisUserForm',
            },
            state: {
                "ctu__userId": userid
            }
        },
        true);
    },

    onRender : function(component, event, helper){
        if(component.get('v.reRendered') == false){
            component.set('v.reRendered', true);
            helper.checkUtilityBar(component);
        }
    }
})