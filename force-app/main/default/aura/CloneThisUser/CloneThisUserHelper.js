({
    checkUtilityBar : function(component){
        let utilityAPI = component.find("utilitybar");
        utilityAPI.getAllUtilityInfo()
            .then(function(response) {
                let myUtilityInfo = response;
                for (let index = 0; index < myUtilityInfo.length; index++) {
                    if(myUtilityInfo[index].utilityLabel == 'CloneThisUser' && myUtilityInfo[index].utilityVisible){
                        component.set('v.onUtilityBar', true);
                    }
                }
            })
            .catch(function(error) {
                console.log('Error promise: ' + error);
            });
    }
})