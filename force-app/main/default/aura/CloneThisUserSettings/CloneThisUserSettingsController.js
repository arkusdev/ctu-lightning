({
    init: function (component, event, helper) {
        var actions = helper.getRowActions.bind(this, component);
        component.set('v.columns', [
            {label: 'Field Label', fieldName: 'FieldLabel', type: 'text'},
            {label: 'API Name', fieldName: 'APIName', type: 'text'},
            {label: 'Data Type', fieldName: 'DataType', type: 'text'},
            {label: 'Required', fieldName: 'IsRequired', type: 'text'},
            {label: 'Installed Package', fieldName: 'InstalledPackage', type: 'text'},
            {label: 'Default value', fieldName: 'DefaultValueLabel', type: 'text'},
            { type: 'action', typeAttributes: { rowActions: actions }}
        ]);

        var actionsGeneralOptions = [
            { label: 'Change default value', name: 'handleChangeGeneralOption' },
        ]
        component.set('v.goColumns', [
            {label: 'Category', fieldName: 'ctu__Data_Category__c', type: 'text'},
            {label: 'Label', fieldName: 'ctu__Data_Label__c', type: 'text'},
            {label: 'Default Value', fieldName: 'ctu__Default_Value__c', type: 'text'},
            { type: 'action', typeAttributes: { rowActions: actionsGeneralOptions }}
        ]);
        helper.getData(component, event, helper);
        helper.getStandardData(component);
        helper.getGeneralOptions(component);
    },

    changeDefaultValue : function (component, event, helper) {
        var row = event.getParam('row');
        helper.changeDefaultValue(component, row);
    },

    handleChangeGeneralOption: function (component, event, helper) {
        var row = event.getParam('row');
        helper.handleChangeGeneralOption(component, row, helper);
    },

    handleSaveAdvancedOptions : function (component, event, helper) {
        helper.handleSaveAdvancedOptions(component);
    },

    updateSelectedText : function (component, event, helper) {
        helper.updateIgnoredFields(component, event, helper);
    },

    updateStandardText : function (component, event, helper) {
        helper.updateIgnoredFieldsStandard(component, event, helper);
    },

    handleSave : function (component, event, helper) {
        helper.save(component, event);
    },
    handleSaveManageUser : function (component, event, helper) {
        helper.saveManageUsers(component);
    },
    handleSaveViewQuickLinks : function (component, event, helper) {
        helper.saveViewQuickLinks(component);
    },
    handleReset :function (component, event, helper) {
        helper.reset(component, event, helper);
    },
    handleReturn : function (component, event, helper) {
        var navService = component.find("navService");
        var pageReference = {
            type: 'standard__navItemPage',
            attributes: {
                apiName: 'ctu__Clone_This_User_lwc'
            }
        };
        event.preventDefault();
        navService.navigate(pageReference);
    },

    avoidfocus : function(component, event){
        event.preventDefault();
        var elements = document.getElementsByClassName("myTest");
        for (var i=0; i<elements.length; i++) {
            elements[i].blur();
        }
    },

    handleDocument : function(component, event, helper){
        let classReference = event.getSource().getLocalId();
        if(document.getElementsByClassName(classReference).length > 0){
            return;
        }
        var modalBody;
        let infoMap = [];
        if(classReference == 'muinfo'){
            infoMap.push({'name': 'Assign a Permission Set License to a User', 'link': 'https://help.salesforce.com/articleView?id=users_permissionset_licenses_assign.htm&type=5'});
            infoMap.push({'name': 'Create and Edit Groups', 'link': 'https://help.salesforce.com/articleView?id=creating_and_editing_groups.htm&type=5'});
            infoMap.push({'name': 'Set Up Queues', 'link': 'https://help.salesforce.com/articleView?id=setting_up_queues.htm&type=5'});
        }
        if(classReference == 'apsinfo'){
            infoMap.push({'name': 'Assign Permission Sets to a Single User', 'link': 'https://help.salesforce.com/articleView?id=perm_sets_assigning.htm&type=5'})
        }
        if(classReference == 'rpinfo'){
            infoMap.push({'name': 'Reset Passwords for Your Users', 'link': 'https://help.salesforce.com/articleView?id=resetting_and_expiring_passwords.htm&type=5'})
        }
        if(classReference == 'aqinfo'){
            infoMap.push({'name': 'Set Up Queues', 'link': 'https://help.salesforce.com/articleView?id=setting_up_queues.htm&type=5'});
        }
        component.set('v.infoMap', infoMap);
        if(component.get('v.notVF') == true){
            $A.createComponent("c:CTUSettingsInfo", {
            infoMap: infoMap,
            classRef: classReference
            }, function(content, status) {
                    if (status === "SUCCESS") {
                        modalBody = content;
                        component.find('overlayLib').showCustomPopover({
                            body: modalBody,
                            referenceSelector: '.' + classReference,
                            cssClass: "popoverclass,cPopoverOpener",
                        })
                    }
                })
        } else {
            let elements;
            switch (classReference) {
                case 'muinfo':
                    elements = document.getElementsByClassName('popmu');
                    if(elements[0] != undefined){
                        $A.util.removeClass(elements[0], 'slds-hide');
                        $A.util.addClass(elements[0], 'slds-visible');
                    }
                    break;

                case 'apsinfo':
                    elements = document.getElementsByClassName('popps');
                    if(elements[0] != undefined){
                        $A.util.removeClass(elements[0], 'slds-hide');
                        $A.util.addClass(elements[0], 'slds-visible');
                    }
                    break;

                case 'rpinfo':
                    elements = document.getElementsByClassName('poprp');
                    if(elements[0] != undefined){
                        $A.util.removeClass(elements[0], 'slds-hide');
                        $A.util.addClass(elements[0], 'slds-visible');
                    }
                    break;

                case 'aqinfo':
                    elements = document.getElementsByClassName('popq');
                    if(elements[0] != undefined){
                        $A.util.removeClass(elements[0], 'slds-hide');
                        $A.util.addClass(elements[0], 'slds-visible');
                    }
                    break;
            
                default:
                    break;
            }
        }
        
    },

    closepop : function(component, event, helper){
        let elements;
        let source = event.getSource().getLocalId();
        switch (source) {
            case 'closemu':
                elements = document.getElementsByClassName('popmu');
                if(elements[0] != undefined){
                    $A.util.removeClass(elements[0], 'slds-visible');
                    $A.util.addClass(elements[0], 'slds-hide');
                }
                break;
            case 'closeps':
                elements = document.getElementsByClassName('popps');
                if(elements[0] != undefined){
                    $A.util.removeClass(elements[0], 'slds-visible');
                    $A.util.addClass(elements[0], 'slds-hide');
                }
                break;
            case 'closerp':
                elements = document.getElementsByClassName('poprp');
                if(elements[0] != undefined){
                    $A.util.removeClass(elements[0], 'slds-visible');
                    $A.util.addClass(elements[0], 'slds-hide');
                }
                break;
            case 'closeq':
                elements = document.getElementsByClassName('popq');
                if(elements[0] != undefined){
                    $A.util.removeClass(elements[0], 'slds-visible');
                    $A.util.addClass(elements[0], 'slds-hide');
                }
                break;
            default:
                break;
        }
    },
})