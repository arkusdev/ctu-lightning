({
    getData : function(component, event, helper) {
        component.set("v.showSpinner", true);
        let action = component.get("c.getData");
        action.setParams({});
        action.setCallback(this, function(result) {
            let data = result.getReturnValue().returnData;
            data.sort(function(a, b){
                if(a.FieldLabel < b.FieldLabel) { return -1; }
                if(a.FieldLabel > b.FieldLabel) { return 1; }
                return 0;
            });

            let hiddenFields = [];
            data.forEach(function(field){
                if(field.IsHidden){
                    hiddenFields.push(field.APIName);
                }
                field.IsRequired = field.IsRequired ? 'Yes' : 'No';
                field.DefaultValueLabel = field.DefaultValue ? field.DefaultValue.ctu__Default_Value_Label__c : '';
            });
            debugger;
            component.set("v.data", data);
            component.set("v.manageUsers", result.getReturnValue().checkManageUsers);
            component.set("v.modifyAllData", result.getReturnValue().checkModifyAllData);
            component.set("v.assignPS", result.getReturnValue().checkPsAssign);
            component.set("v.resetP", result.getReturnValue().checkPsAssign);
            component.set("v.createQueue", result.getReturnValue().checkQueue);
            component.set("v.isSystemAdmin", result.getReturnValue().isSystemAdmin);
            component.set("v.viewQuickLinksBar", result.getReturnValue().canViewQuickLinksBar);
            component.set("v.selectedRows",hiddenFields);
            component.set("v.selectedRowsReset",hiddenFields);
            component.set('v.selectedRowsCount', hiddenFields.length);
            component.set("v.showSpinner", false);
            

        });
        $A.enqueueAction(action);
    },

    getRowActions: function(component, row, cb) {
        var actions = [];
        actions.push({
                label: "Change default value", 
                name: "changeDefaultValue"
            });
        cb(actions);
    },

    changeDefaultValue: function(component, row){
        component.set('v.selectedFieldDefaultValue', row.DefaultValue ? (row.DefaultValue.ctu__Default_Value__c ? row.DefaultValue.ctu__Default_Value__c : (typeof row.DefaultValue == 'string') ? row.DefaultValue : '') : '');
        component.set('v.selectedFieldDefaultValueLabel', row.DefaultValueLabel);
        var type = row.DataType, fieldLabel = row.FieldLabel, fieldName = row.InstalledPackage != '' ? `${row.InstalledPackage}__${row.APIName}` : row.APIName;
        var modal;
        $A.createComponent('c:DefaultFieldValueModal',{fieldName: fieldName, fieldLabel: row.FieldLabel, fieldValue: component.getReference('v.selectedFieldDefaultValue'), fieldValueLabel: component.getReference('v.selectedFieldDefaultValueLabel'), dataType: type},
                           function(content, status) {
                               if (status === 'SUCCESS') {
                                   modal = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: 'Set the field default value',
                                       body: modal,
                                       showCloseButton: true,
                                       cssClass: "displaySelector",
                                       closeCallback: function() {
                                            var cDataModified = false;
                                            var sdata = component.get("v.sdata");
                                            var data = component.get("v.data");
                                            var editedRow = sdata.filter(d => d.FieldLabel == fieldLabel)[0];
                                            if (!editedRow) {
                                                cDataModified = true;
                                                editedRow = data.filter(d => d.FieldLabel == fieldLabel)[0];
                                            }
                                            editedRow.DefaultValue = component.get('v.selectedFieldDefaultValue');
                                            editedRow.DefaultValueLabel = component.get('v.selectedFieldDefaultValueLabel');
                                            if (cDataModified){
                                                var editedCustomRecords = component.get('v.editedCustomRecords');
                                                var newEditedCustomRecords = editedCustomRecords.filter(r => r.FieldLabel !==  fieldLabel);
                                                newEditedCustomRecords.push(editedRow);
                                                component.set('v.editedCustomRecords', newEditedCustomRecords);
                                                component.set('v.data', data);
                                            }else{ 
                                                var editedStandardRecords = component.get('v.editedStandardRecords');
                                                var newEditedStandardRecords = editedStandardRecords.filter(r => r.FieldLabel !==  fieldLabel);
                                                newEditedStandardRecords.push(editedRow);
                                                component.set('v.editedStandardRecords', newEditedStandardRecords);
                                                component.set('v.sdata', sdata);
                                            }
                                       }
                                   });
                               }
                           });
    },
    
    handleChangeGeneralOption: function(component, row, helper){
        component.set('v.selectedFieldDefaultValue', (row.ctu__Default_Value__c == 'Yes'? true: false));
        component.set('v.selectedFieldDefaultValueLabel', row.ctu__Data_Label__c);
        var modal;
        $A.createComponent('c:DefaultGeneralFieldValueModal',{  fieldValue: component.getReference('v.selectedFieldDefaultValue'), 
                                                                fieldValueLabel: component.getReference('v.selectedFieldDefaultValueLabel'),
                                                                valueModified: component.getReference('v.valueModified')},
            function(content, status) {
                if (status === 'SUCCESS') {
                    modal = content;
                    component.find('overlayLib').showCustomModal({
                        header: 'Set the field default value',
                        body: modal,
                        showCloseButton: true,
                        cssClass: "displaySelector",
                        closeCallback: function() {
                            if(component.get('v.valueModified')){
                                var defaultValueUpdated= component.get('v.selectedFieldDefaultValue');
                                var goData = component.get('v.goData');
                                goData.forEach(element => {
                                    if(element.Name == row.Name){
                                        element.ctu__Default_Value__c= defaultValueUpdated? 'Yes': 'No';
                                    }
                                });
                                component.set('v.goData', goData);
                                let rowModified= JSON.parse(JSON.stringify(row)); 
                                let advancedOptions = component.get('v.advancedOptions');
                                let indexOption= advancedOptions.findIndex(element => element.Name == rowModified.Name)
                                if(indexOption == -1){
                                    advancedOptions.push({Id: rowModified.Id, Name: rowModified.Name, ctu__Default_Value__c: (rowModified.ctu__Default_Value__c== 'Yes'? true: false), ctu__Data_Label__c: rowModified.ctu__Data_Label__c, ctu__Data_Category__c: rowModified.ctu__Data_Category__c});
                                }else{
                                    advancedOptions[indexOption].ctu__Default_Value__c= (rowModified.ctu__Default_Value__c== 'Yes'? true: false);
                                }
                                component.set('v.advancedOptions', advancedOptions);
                            }
                        }
                    });
                }
            });
    },

    getStandardData : function(component){
        component.set("v.showSpinner", true);
        let action = component.get("c.getStandardData");
        action.setParams({});
        action.setCallback(this, function(result) {
            if(result.getState() === 'SUCCESS'){
                let data = result.getReturnValue().returnData;
                data.sort(function(a, b){
                    if(a.FieldLabel < b.FieldLabel) { return -1; }
                    if(a.FieldLabel > b.FieldLabel) { return 1; }
                    return 0;
                });

                let hiddenFields = [];
                data.forEach(function(field){
                    if(field.IsHidden){
                        hiddenFields.push(field.APIName);
                    }
                    field.IsRequired = field.IsRequired ? 'Yes' : 'No';
                    field.DefaultValueLabel = field.DefaultValue ? field.DefaultValue.ctu__Default_Value_Label__c : '';
                });
                component.set("v.sdata", data);
                component.set("v.standardSelectedRows",hiddenFields);
                component.set("v.standardSelectedRowsReset",hiddenFields);
                component.set('v.standardSelectedRowsCount', hiddenFields.length);
            } else {
                console.log(result.getError());
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },
    
    getGeneralOptions : function(component){
        component.set("v.showSpinner", true);
        let action = component.get("c.getGeneralOptionsData");
        action.setParams({});
        action.setCallback(this, function(result) {
            if(result.getState() === 'SUCCESS'){
                let res = result.getReturnValue();
                res.sort((a,b) => {
                    var nameA= a.ctu__Data_Category__c.toUpperCase();
                    var nameB= b.ctu__Data_Category__c.toUpperCase();
                    if (nameA < nameB || nameA == "GENERAL") {
                        return -1;
                      }
                      if (nameA > nameB) {
                        return 1;
                      }
                      return 0;
                })
                let indexFirstElement= res.findIndex(element => element.ctu__Data_Category__c.toUpperCase() == "GENERAL")
                let firstElement= res.splice(indexFirstElement, 1);
                res.splice(0, 0, firstElement[0])
                component.set("v.goData",res);
                this.renderGeneralOptions(component);
            } else {
                console.log(result.getError());
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },

    renderGeneralOptions: function(component){
        var goData= component.get("v.goData");
        goData.forEach(element => {
            element.ctu__Default_Value__c= element.ctu__Default_Value__c? 'Yes': 'No'
        });
        component.set("v.goData", goData);
    },

    handleSaveAdvancedOptions : function(component){
        let action = component.get("c.saveAdvancedOptions");
        action.setParams({ 'advancedOptions' : component.get('v.advancedOptions')});
        action.setCallback(this, function(result) {
            var status = result.getState();
            if (status === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Your settings were successfully updated",
                    "variant": "success"
                });
                toastEvent.fire();
            }else{
                console.log(result.getError()[0]);
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },

    updateIgnoredFields : function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        component.set('v.selectedRowsCount', selectedRows.length);
    },

    updateIgnoredFieldsStandard : function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        component.set('v.standardSelectedRowsCount', selectedRows.length);
    },
    
    save : function(component, event) {
        var editedStandardRecords =  component.get("v.editedStandardRecords");
        var editedCustomRecords =  component.get("v.editedCustomRecords");
        component.set("v.showSpinner", true);

        var a = event.getSource();
        var id = a.getLocalId();

        var standardFields = id == 'saveStandardFields';
        
        let selectedRows = [];
        let hiddenFields = [];
        let selectedRowsReset = [];
        let defaultValues = [];
        let usersFieldTable = component.find('usersFieldTable');
        let userFieldTable = component.find('userFieldTable');

        if (standardFields){
            if(usersFieldTable != undefined){
                selectedRows = usersFieldTable.getSelectedRows();
            }
            editedStandardRecords.forEach(d => {
                defaultValues.push({
                    Name: d.APIName,
                    ctu__Field_Name__c: d.InstalledPackage ? `${d.InstalledPackage}__${d.APIName}` : d.APIName,
                    ctu__Default_Value__c: d.DefaultValue,
                    ctu__Default_Value_Label__c: !d.DefaultValueLabel ? '' : d.DefaultValueLabel.substring(0, 100),
                    ctu__Data_Type__c: d.DataType
                });
            });
        }else{
            if(userFieldTable != undefined){
                selectedRows = userFieldTable.getSelectedRows();
            }
            editedCustomRecords.forEach(d => {
                defaultValues.push({
                    Name: d.APIName,
                    ctu__Field_Name__c: d.InstalledPackage ? `${d.InstalledPackage}__${d.APIName}` : d.APIName,
                    ctu__Default_Value__c: d.DefaultValue,
                    ctu__Default_Value_Label__c: !d.DefaultValueLabel ? '' : d.DefaultValueLabel.substring(0, 100),
                    ctu__Data_Type__c: d.DataType
                });
            });
        }

        selectedRows.forEach(function(field){
            selectedRowsReset.push(field.APIName);
            hiddenFields.push(field.APIName);
        });

        let resetFields = standardFields ? component.get('v.selectedRowsReset') : component.get('v.standardSelectedRowsReset');
        resetFields.forEach(el => {
            hiddenFields.push(el);
        });

        component.set("v.editedStandardRecords", []);
        component.set("v.editedCustomRecords", []);
        
        let action = component.get("c.updateData");
        action.setParams({ 'data' : hiddenFields, 'defaultValues' : defaultValues});
        action.setCallback(this, function(result) {
            var status = result.getState();
            if (status === "SUCCESS") {
                component.set(standardFields ? "v.standardSelectedRowsReset" : "v.selectedRowsReset", selectedRowsReset);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Your settings were successfully updated",
                    "variant": "success"
                });
                toastEvent.fire();
            }else{
                console.log(result.getError()[0]);
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },
    saveViewQuickLinks : function(component) {
        var viewQuickLinks = component.get('v.viewQuickLinksBar');
        let action = component.get("c.updateViewQuickLinks");
        action.setParams({
            'viewQuickLinks' : viewQuickLinks,
        });
        action.setCallback(this, function(result) {
            var status = result.getState();
            if (status === "SUCCESS") {
                var notVF = component.get('v.notVF');
                if (notVF){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Your settings were successfully updated",
                        "variant": "success"
                    });
                    toastEvent.fire();
                }else{
                    var showVFPage = $A.get("e.c:ShowVFToast");
                    showVFPage.setParams({msg: "Your settings were successfully updated"});
                    showVFPage.fire();
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },
    saveManageUsers : function(component) {
        var manageUsers = component.get('v.manageUsers');
        var modifyData = component.get('v.modifyAllData');
        var assignPS = component.get('v.assignPS');
        var resetP = component.get('v.resetP');
        var createQueue = component.get('v.createQueue');

        let action = component.get("c.updateManageUsers");
        action.setParams({ 
            'manageUsers' : manageUsers,
            'modifyAllData' : modifyData,
            'assignPS' : assignPS,
            'resetPassword' : resetP,
            'cqueue': createQueue
        });
        action.setCallback(this, function(result) {
            var status = result.getState();
            if (status === "SUCCESS") {
                var notVF = component.get('v.notVF');
                if (notVF){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Your settings were successfully updated",
                        "variant": "success"
                    });
                    toastEvent.fire();
                }else{
                    var showVFPage = $A.get("e.c:ShowVFToast");
                    showVFPage.setParams({msg: "Your settings were successfully updated"});
                    showVFPage.fire();
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },
    reset : function(component, event, helper) {
        var a = event.getSource();
        var id = a.getLocalId();

        switch (id){
            case 'resetStandardFields':
                component.set("v.standardSelectedRows",component.get("v.standardSelectedRowsReset"));
                component.set('v.standardSelectedRowsCount', component.get("v.standardSelectedRowsReset").length);
            break;

            case 'resetCustomFields':
                component.set("v.selectedRows",component.get("v.selectedRowsReset"));
                component.set('v.selectedRowsCount', component.get("v.selectedRowsReset").length);
            break;
        }
    }
})