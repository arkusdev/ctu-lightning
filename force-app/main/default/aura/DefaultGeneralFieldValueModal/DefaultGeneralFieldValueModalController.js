({
    handleSave : function(component, event, helper){
        var fieldValue = component.get('v.fieldValue');
        var fieldValueLabel = component.get('v.fieldValueLabel');
        component.set('v.fieldValue', fieldValue);
        component.set('v.fieldValueLabel', fieldValueLabel);
        component.set('v.valueModified', true);
        component.find('overlayLib').notifyClose();
    }
})