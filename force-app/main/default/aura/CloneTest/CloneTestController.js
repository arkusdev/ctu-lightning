({
    doInit : function(component, event, helper){
        if (typeof require !== "undefined") {
            var evt = component.getEvent("requireJSEvent");
		    evt.fire();
        } else {
            var head = document.getElementsByTagName('head')[0];
            var script = document.createElement('script');
            
            script.src = "/resource/RequireJS"; 
            script.type = 'text/javascript';
            script.key = "/resource/RequireJS"; 
            script.helper = this;
            script.id = "script_" + component.getGlobalId();
            var hlp = helper;
            script.onload = function scriptLoaded(){
                var evt = component.getEvent("requireJSEvent");
		        evt.fire();
            };
            head.appendChild(script);
        }
    },
    initJS : function(component, event, helper){
        require.config({
            paths: {
                "jquery": "/resource/Bootstrap/js/jquery-3.6.0.min.js",
                "velocity": "/resource/Velocity/velocity-master/velocity",
                "velocity.ui": "/resource/Velocity/velocity-master/velocity.ui",
            },
            shim: {
                "velocity": {
                    deps: [ "jquery" ]
                },
                // Optional, if you're using the UI pack:
                "velocity.ui": {
                    deps: [ "velocity" ]
                }
            }
        });
        console.log("RequiresJS has been loaded? "+(require !== "undefined"));
        //loading libraries sequentially
        require(["jquery"], function($) {
            console.log("JQuery  has been loaded? "+($ !== "undefined"));
            require(["velocity"], function(velocity) {
                console.log("Velocity  has been loaded? "+(velocity !== "undefined"));
                //console.log(velocity);
                require(["velocity.ui"], function(velocityui) {
                    console.log("Velocity.ui  has been loaded? "+(velocityui !== "undefined"));
                });
            });//require end*/
        });
    },
	myAction : function(cmp, event, helper) {
        var text = cmp.get("v.inputSearcher");
		alert(text);
	},
    search : function(cmp, event, helper) {
        cmp.set("v.Stage",1);
        var wordSearch = cmp.get("v.inputSearcher");
        //helper.SearchUsers(cmp, wordSearch);
        helper.test(cmp, cmp.get("v.inputSearcher"));
	},
    chargeUserVariable : function(cmp, event, helper) {
        var userChosen = cmp.get("v.inputSearcher");
        //helper.SearchUsers(cmp, wordSearch);
        helper.test(cmp, cmp.get("v.inputSearcher"));
	},
    /*takeUser : function(cmp, event, helper) {
        var userC = event.getParam("shareUser");
       cmp.set("v.userToClone",userC);
       cmp.set("v.Stage",event.getParam("viewStage"));
       
	},*/
    takeUser : function(cmp, event, helper) {
       cmp.set("v.Stage",event.getParam("viewStage"));
       var userC = event.getParam("shareUser");
       //alert(userC.Id+' '+userC.Name+' @@@@  '+userC.Username);
       cmp.set("v.NameUserToCLone",userC.Name);
       cmp.set("v.idUserToCLone",userC.Id);
	},
    init : function(cmp,event,helper){
        cmp.set("v.Stage",1);
        //alert('init '+cmp.get("v.inputSearcher"));
        //helper.test(cmp, cmp.get("v.inputSearcher"));
    }
})