({
	SearchUsers : function(component,word) {
		this.upsertExpense(component, word, function(a) {
        component.set("v.users", a.getReturnValue());
      });
	},
    upsertExpense : function(component, word, callback) {
      var action = component.get("c.getSearch");
      if (callback) {
          action.setCallback(this, callback);
      }
      $A.enqueueAction(action);
    },
    test : function(cmp,word) {
		var action = cmp.get("c.searchUsers");
        action.setParams({ 
          "search": word,
      	});
        $A.log("prueba ");
        action.setCallback(this, function(a) {
            cmp.set("v.users",a.getReturnValue());
        });
        $A.enqueueAction(action);
	}
    
})