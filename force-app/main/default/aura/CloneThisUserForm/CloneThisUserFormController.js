({
    init: function (cmp, evt, helper) {
        var pageRef = cmp.get("v.pageReference");
        if (pageRef) {
            var userId = pageRef.state.ctu__userId;
            cmp.set("v.userId", userId);

            var permissionSetAssignments = pageRef.state.ctu__permissionSetAssignments ? pageRef.state.ctu__permissionSetAssignments : true;
            cmp.set("v.permissionSetAssignments", permissionSetAssignments == true || permissionSetAssignments == "true");

            var queueMembership = pageRef.state.ctu__queueMembership ? pageRef.state.ctu__queueMembership : true;
            cmp.set("v.queueMembership", queueMembership == true || queueMembership == "true");

            var publicGroupMembership = pageRef.state.ctu__publicGroupMembership ? pageRef.state.ctu__publicGroupMembership : true;
            cmp.set("v.publicGroupMembership", publicGroupMembership == true || publicGroupMembership == "true");

            var permissionSetLicenseAssignments = pageRef.state.ctu__permissionSetLicenseAssignments ? pageRef.state.ctu__permissionSetLicenseAssignments : true;
            cmp.set("v.permissionSetLicenseAssignments", permissionSetLicenseAssignments == true || permissionSetLicenseAssignments == "true");

            var chatterGroups = pageRef.state.ctu__chatterGroups ? pageRef.state.ctu__chatterGroups : true;
            cmp.set("v.chatterGroups", chatterGroups == true || chatterGroups == "true");

            var generatePassword = pageRef.state.ctu__generatePassword ? pageRef.state.ctu__generatePassword : true;
            cmp.set("v.generatePassword", generatePassword == true || generatePassword == "true");
        }
    },

    reInit: function (cmp, evt, helper) {
        $A.get('e.force:refreshView').fire();
    },
    
    handleUserCreated: function (cmp, evt) {
        var data = evt.getParam('data');
        cmp.find("navigationService").navigate({
            type: 'standard__component',
            attributes: {
                componentName: 'ctu__cloneThisUserDetail',
            },
            state: {
                ctu__g: data
            }
        },
        true);
        
    },
    handleCancel: function (cmp, evt) {
        var search = evt.getParam('search');
        cmp.find("navigationService").navigate({
                type: 'standard__component',
                attributes: {
                    componentName: 'ctu__cloneThisUser',
                },
                state: {
                    ctu__search: search
                }
            },
            true
        );
    }
})