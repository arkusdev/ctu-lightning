({
	Cancel : function(cmp, event, helper) {
        var shareUserEvent = cmp.getEvent("Canceled");
        shareUserEvent.fire();
	},
    saveClone : function (cmp, event, helper){
         ///alert(cmp.get("v.userToCloneId"));
    	helper.clone(cmp,cmp.get("v.userToCloneId"));
	},
    testing:function(cmp, event, helper){
        var usernew = cmp.get("v.userToClone");
        alert(cmp.get("v.userToCloneId")+" "+
              usernew.FirstName	+" "+
              usernew.LastName	+" "+
              usernew.Email		+" "+
              usernew.UserName	+" "+
              usernew.Alias		+" "+
              usernew.CommunityNickname	+" "+
              cmp.get("v.PermissionSet")+" "+
              cmp.get("v.QueueMember")	+" "+
              cmp.get("v.PublicGroup")	+" "+
              cmp.get("v.PermissionSetLicense"));
       var action = cmp.get("c.setFields");
       action.setParams({ 
          "idu"		:cmp.get("v.userToCloneId"),
          "fn"		:usernew.FirstName			,	
          "ln"		:usernew.LastName			,
          "email"	:usernew.Email				,
          "un"		:usernew.UserName			,
          "alias"	:usernew.Alias				,
          "nickn" 	:usernew.CommunityNickname	,
          "gPx" 	:cmp.get("v.GeneratePss")  			,
          "pAx" 	:cmp.get("v.PermissionSet")  		,
          "qMx" 	:cmp.get("v.QueueMember")  			,
          "pGMx" 	:cmp.get("v.PublicGroup") 			,
          "pSLAx" 	:cmp.get("v.PermissionSetLicense")	,
       });
        
        action.setCallback(this, function(a){
            alert(a.getReturnValue());
        });
       $A.enqueueAction(action);
    },
    testkeyUp : function (cmp, event, helper){
        //alert();
        //console.log(event.source.getElement());
        //cmp.find("FirstName").getElement();
        var fname = event.source.getElement();
        var error = "";
        //console.log(event );
        console.log(event.source.j.Ne );
        if(fname.value.length  == 0){
            fname.placeholder = "Field can't be empty";
            $(fname).css({"border-color":"#FF3300","background-color":"#FFE6E6"});
        }else{
            if((event.source.j.Ne == "lasname")||(event.source.j.Ne == "FirstName")||(event.source.j.Ne == "nickname")||(event.source.j.Ne == "alias")){
                if(!helper.validateNoNumbers(fname.value)){
                        error = "Not allowed mixing letters and numbers";
            	}
            }
            if( (event.source.j.Ne == "Username") || (event.source.j.Ne == "email") ){
                if(!helper.validateMail(fname.value)){
                        error = "Not allowed mixing letters and numbers";
            	}
            }
            if(error.length != 0){
                //console.log("mal");
                 fname.placeholder =error;
                 $(fname).css({"border-color":"#FF0000","background-color":"#FFE6E6"});  
            }else{
                 $(fname).css({"border-color":"#33CC33","background-color":"#FFFFFF"});
            }
      }
	 }
})