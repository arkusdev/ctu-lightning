({
	clone : function(component, userid) {
         
		this.cloneApex(component, userid, function(a) {
            if(a.getReturnValue()){
                
                component.set("v.returnM", 'fine');
            }else{
                component.set("v.returnM", 'bad');
            }
            alert(a.getReturnValue()); 
          });
       
	},
    cloneApex : function(component, id, callback) {
      var action = component.get("c.save");
      
      action.setParams({ 
          "generatePassword" 					:	component.get("v.GeneratePss")  ,
          "PermissionSetAssignments" 			:	component.get("v.PermissionSet")  ,
          "QueueMembership" 					:	component.get("v.QueueMember")  ,
          "PublicGroupMembership" 				:	component.get("v.PublicGroup") ,
          "PermissionSetLicenseAssignments" 	:	component.get("v.PermissionSetLicense"),
      });
      if (callback) {
          action.setCallback(this, callback);
      }
      $A.enqueueAction(action);
      alert(component.get("v.returnM")+' '+component.get("v.GeneratePss")+' '+component.get("v.PermissionSet"));
    } ,
    validateMail : function ValidateEmail(inputText){  
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
         console.log(inputText.match(mailformat));
        if(inputText.match(mailformat))  
        {   
            return true;  
        }else{ 
            return false;  
        }  
    },
    validateNoNumbers : function Validate( inp){    
        var iter = function (word){
            var i = 0;
            while( i < word.length ){
                if(parseInt(word.charAt(i))){
                    return false;
                }
                i++;
            }
            return true;
        }   
        if(iter(inp))
        {
           //alert('true');
           return true;
        }else{
            //alert('false');
           return false;
        }
    }
	
    
})