({
    validURL : function(url){
        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
            '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return !!pattern.test(url);
    },
    validateEmail : function(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    },
    formatAMPM: function(time) {
        var timeSplit = time.split('.');
        time = timeSplit.length > 0 ? timeSplit[0] : time;
        // Check correct time format and split into components
        time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice (1);  // Remove full string match value
            time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join (''); // return adjusted time or original string
    },
    getNameById: function(component, recordId){
        var action = component.get('c.getNameByRecordId');
        action.setParams({ 'recordId' : recordId });
        action.setCallback(this, function(result) {
            debugger;
            var status = result.getState();
            var fieldValueLabel = '';
            if (status === "SUCCESS") {
                fieldValueLabel = result.getReturnValue();
            }else{
                console.log(result.getError()[0]);
            }
            component.set('v.fieldValue', recordId);
            component.set('v.fieldValueLabel', fieldValueLabel);
            component.find('overlayLib').notifyClose();
        });
        $A.enqueueAction(action);
    },

    loadProfiles: function(component){
        var action = component.get('c.getProfiles');
 
        action.setCallback(this, function(response) {
            var state = response.getState();
            var returnValue = response.getReturnValue();
            if (state === 'SUCCESS') {
                component.set('v.profiles', returnValue);
            }
            else{ 
                console.log(result.getError()[0]);
            }
        });
        $A.enqueueAction(action);
    }
})