({
    init : function(component){
        debugger;
        var fieldValue = component.get('v.fieldValue');
        var fieldName = component.get('v.fieldName');
        component.set('v.fieldValueCopy', fieldValue);
        component.set('v.fieldValueLabelCopy', component.get('v.fieldValueLabel'));
        switch (component.get('v.dataType')){
            case 'REFERENCE':
                component.set('v.isLookup', true);
                var isProfile = fieldName == 'ProfileId';
                var isUserRoleId = fieldName == 'UserRoleId';
                var isDelegatedApproverId = fieldName == 'DelegatedApproverId';
                var showCustomLookup = isProfile || isUserRoleId || isDelegatedApproverId;
                component.set('v.showCustomLookup', showCustomLookup);

                if (showCustomLookup){
                    if (isProfile) component.set('v.objectName', 'Profile');
                    if (isUserRoleId) component.set('v.objectName', 'UserRole');
                    if (isDelegatedApproverId) component.set('v.objectName', 'User');
                    return;
                }

                if ((!fieldValue || fieldValue == '')) {
                    fieldValue = null;
                }
            break;

            case 'LOCATION':
                var locationSplit = fieldValue.split(',');
                var latitude = locationSplit.length > 0 ? fieldValue.split(',')[0] : '';
                var longitude = locationSplit.length > 1 ? fieldValue.split(',')[1] : '';
                fieldValue = {
                    latitude: latitude,
                    longitude: longitude
                }
            break;

            case 'BOOLEAN':
                var fieldValueLabel = (fieldValue == 'true' || fieldValue == true) ? 'Yes' : 'No';
                if (!fieldValue || fieldValue == '' || fieldValue == 'false'){
                    fieldValue = false;
                }
                component.set('v.fieldValueLabelCopy', fieldValueLabel);
            break;
        }
        component.find('fieldValue').set('v.value', fieldValue);
    },
    handleChange : function(component, event){
        if (component.get('v.isLookup')){
            var recordId = event.getSource().get('v.value');
            component.set('v.fieldValue', recordId);
            component.set('v.lookupSet', true);
            component.find("recordValue").reloadRecord();
        }
        if (component.get('v.dataType') == 'TIME'){
            component.find('fieldValue').set('v.value', event.getSource().get('v.value'));
        }
    },
    setEmptyValue : function(component){
        component.set('v.fieldValue', null);
        component.set('v.fieldValueLabel', '');
        component.find('overlayLib').notifyClose();
    },
    handleSaveCustomLookup : function(component){
        component.set('v.fieldValue', component.get('v.fieldValueCopy'));
        component.set('v.fieldValueLabel', component.get('v.fieldValueLabelCopy'));
        component.find('overlayLib').notifyClose();
    },
    handleSave : function(component, event, helper){
        var fieldValue = component.find('fieldValue').get('v.value');
        var dataType = component.get('v.dataType');
        var fieldValueLabel;
        var callback = false;
        event.preventDefault();
        if (fieldValue != undefined && fieldValue !== ''){
            switch (dataType){
                case 'URL':
                    if (!helper.validURL(fieldValue)){
                        component.set('v.notValid', true);
                        component.set('v.errorMessage', 'Incorrect URL format');
                        return;
                    }
                break;
                case 'EMAIL':
                    if (!helper.validateEmail(fieldValue)){
                        component.set('v.notValid', true);
                        component.set('v.errorMessage', 'Incorrect Email format');
                        return;
                    }
                break;
                case 'DOUBLE':
                    if (isNaN(fieldValue)){
                        component.set('v.notValid', true);
                        component.set('v.errorMessage', 'Incorrect Number format');
                        return;
                    }
                break;
                case 'LOCATION':
                    fieldValue = fieldValue.latitude + ',' + fieldValue.longitude;
                break;
                case 'REFERENCE':
                    if (component.get('v.record')){
                        fieldValueLabel = component.get('v.record').Name;
                    }
                    else{
                        callback = true;
                        fieldValueLabel = helper.getNameById(component, fieldValue);
                    }
                break;
                case 'TIME':
                    fieldValueLabel = helper.formatAMPM(fieldValue.toString());
                break;
                case 'BOOLEAN':
                    fieldValueLabel = (fieldValue == true || fieldValue == 'true') ? 'Yes' : 'No';
                break;
            }
        }
        if (!callback){
            var fieldValueText = fieldValue == undefined ? '' : fieldValue.toString();
            component.set('v.fieldValue', fieldValueText);
            component.set('v.fieldValueLabel', fieldValueLabel ? fieldValueLabel : fieldValueText);
            component.find('overlayLib').notifyClose();
        }
    }
})