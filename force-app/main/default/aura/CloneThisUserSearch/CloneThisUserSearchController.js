({
    searchUsers : function(cmp, event, helper) {
        timer = cmp.get('v.timer');
        clearTimeout(timer);
        var query = cmp.find("input-search").getElement().value;   
        if(query.length > 1 ){
            newTimer =setTimeout(function(){
                $A.run(function(){     
                    var aGetData = cmp.get("c.getUsers");        
                    aGetData.setParams({'search':query});
                    aGetData.setCallback(this, function(action) {
                        if (action.getState() === "SUCCESS") { 
                            cmp.set('v.users',action.getReturnValue());
                            cmp.set('v.displayList', '');
                        }
                    });
                    $A.enqueueAction(aGetData);
                },300);
            })
            cmp.set('v.timer',newTimer);
        }else if(query.length == 0 ){
            cmp.set('v.displayList', 'none');
        }
    },
    selectUserToClone : function(cmp, event, helper) {
        var user = cmp.get('v.users')[event.currentTarget.value];
        cmp.set('v.userToClone', user);
        cmp.set('v.displayList', 'none');
    },
    addAnotherClone : function(cmp, event, helper){
        cmp.set('v.displayForm', '');
    }
})