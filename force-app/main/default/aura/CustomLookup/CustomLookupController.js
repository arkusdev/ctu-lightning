({
    doInit : function(component, event, helper) {
        $A.util.toggleClass(component.find('resultsDiv'), 'slds-is-open');
        if($A.util.isEmpty(component.get('v.uniqueId'))){
            component.set('v.uniqueId', Math.random());
        }
        if( !$A.util.isEmpty(component.get('v.value')) ) {
            helper.searchRecordsHelper( component, helper );
        }
        setTimeout( function(){
            if(component.find('inputLookup')){
                component.find('inputLookup').focus();
            }
        }, 250);
        helper.addListeners(component, event);
    },

    //When is enter text
    searchRecords : function(component, event, helper){
        if(event.which != 37 && event.which != 38 && event.which != 39 && event.which != 40){
            if(component.get('v.searchString').length > 1) {
                helper.searchRecordsHelper( component, helper );
            } else {
                $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
            }
        }
    },

    //When a item is selected
    selectItem : function( component, event, helper ) {
        if(!$A.util.isEmpty(event.target.id)) {
            debugger;
            var recordsList = component.get('v.recordList');
            
            var selectedRecord;
            var index = recordsList.findIndex(x => x.Id === event.target.id);
            
            if(index != -1) {
                selectedRecord = recordsList[index];
            }
            component.set('v.selectedRecord',selectedRecord[component.get('v.fieldName')]);
            component.set('v.value',selectedRecord.Id);
            component.set('v.valueLabel',selectedRecord.Name);
            $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');

            var idObj = selectedRecord.Id;
            var objectSelected = $A.get("e.c:ObjectSelected");
            objectSelected.setParams({
                "objectId" : idObj
            })
            objectSelected.fire();
            component.set('v.position', -1);
        }
    },

    //Remove a selected Item
    removeItem: function(component, event, helper){
        component.set('v.selectedRecord', '');
        component.set('v.value', '');
        component.set('v.searchString', '');

        var objectSelected = $A.get("e.c:ObjectSelected");
        objectSelected.setParams({
                "objectId" : ''
        })
        objectSelected.fire();

        setTimeout( function(){
            if(component.find('inputLookup')){
                component.find('inputLookup').focus();
            }
        }, 250);
    },

    //Close the dropdown if clicked outside the dropdown
    blurEvent : function(component, event, helper){
        $A.util.removeClass(component.find('resultsDiv'), 'slds-is-open');
        let listElement = component.get('v.resulsArray');
        let position = component.get('v.position');
        if(listElement[position] != undefined){
            let recordsList = component.get('v.recordList');
            let selectedElement = recordsList.find(element => element.Id == listElement[position]);
            
            component.set('v.selectedRecord',selectedElement.Name);
            component.set('v.value',selectedElement.Id);

            var idObj = selectedElement.Id;
            var objectSelected = $A.get("e.c:ObjectSelected");
            objectSelected.setParams({
                "objectId" : idObj
            })
            objectSelected.fire();
        }
        component.set('v.position', -1);
    },
})