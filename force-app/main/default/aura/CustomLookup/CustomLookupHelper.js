({
    searchRecordsHelper : function(component, helper) {
        debugger;
        $A.util.removeClass(component.find("Spinner"), "slds-hide");
        var fieldName = component.get('v.fieldName');
        var action = component.get('c.getObjects');
        
        if (component.get('v.searchString') && component.get('v.searchString') != ''){
            action.setStorable();
            action.setParams({
                'searchParam' : component.get('v.searchString'),
                'objectName' : component.get('v.objectName')
            });
            action.setCallback(this,function(response){
                var result = response.getReturnValue();
                if(response.getState() === 'SUCCESS') {
                    if(result.length > 0) {
                        var value = component.get('v.value');
                        if( $A.util.isEmpty(value) ) {
                            $A.util.addClass(component.find('resultsDiv'),'slds-is-open');
                            helper.dynamicCreation(component, result, fieldName);
                            component.set('v.recordList',result);
                            let returnedArray = [];

                            for(let i in result){
                                returnedArray.push(result[i].Id);
                            }
                            component.set('v.resulsArray', returnedArray);
                            component.set('v.position', -1);
                        } else {
                            var index = result.findIndex(x => x.Id === value)
                            if(index != -1) {
                                var selectedRecord = result[index];
                            }
                            component.set('v.selectedRecord',selectedRecord[fieldName]);
                        }
                    } else {
                        helper.dynamicCreation(component, 'Nothing found', fieldName);
                        $A.util.addClass(component.find('resultsDiv'),'slds-is-open');
                    }
                } else {
                    console.log(response.getError());
                }
                $A.util.addClass(component.find("Spinner"), "slds-hide");
            });
            $A.enqueueAction(action);
        }
    },

    
    dynamicCreation : function(component, recordsList, fieldName) {
        var recordId = 'record-'+component.get('v.uniqueId');
        var recordDiv = document.getElementById(recordId);
        while (recordDiv.firstChild) recordDiv.removeChild(recordDiv.firstChild);
        
        if(Array.isArray(recordsList) && recordsList.length) {
            const len = recordsList.length;
            for(let k = 0; k<len; k++) {
                var li = document.createElement("li");
                li.id = recordsList[k].Id;
                li.className = 'slds-item ' + recordsList[k].Id;
                li.appendChild(document.createTextNode(this.getNameObject(recordsList[k][fieldName], recordsList[k].Id)));
                recordDiv.appendChild(li);
            }
            
        } else {
            var li = document.createElement("li");
            li.id = '';
            li.appendChild(document.createTextNode(recordsList));
            recordDiv.appendChild(li);
        }
    },

    getNameObject : function(nameObject, idObject){
        switch (idObject.substring(0, 3)) {
            case '00Q':
                //Lead
                return nameObject + ' - (Lead)';
                break;
            case '001':
                //Account
                return nameObject + ' - (Account)';
                break;
            case '006':
                //Opportunity
                return nameObject + ' - (Opportunity)';
                break;
            case '003':
                //Contact
                return nameObject + ' - (Contact)';
                break;
            default:
                return nameObject;
                break;
        }
    },

    addListeners : function(component, event){
        window.addEventListener('keyup', (eventHandle) => {
            if(eventHandle.key === 'ArrowUp'){
                let returnedArray = component.get('v.resulsArray');
                let position = component.get('v.position');
                if(returnedArray != undefined && position != undefined){
                    let oldElements = document.getElementsByClassName(returnedArray[position]);
                    let newElements = document.getElementsByClassName(returnedArray[position - 1]);

                    if(position - 1 >= 0){
                        if(oldElements[0] != undefined){
                            $A.util.removeClass(oldElements[0], 'backgroundfocus');
                        }
                        if(newElements[0] != undefined){
                            $A.util.addClass(newElements[0], 'backgroundfocus');
                            component.set('v.position', position - 1);
                        }
                    }
                }
            }
            if(eventHandle.key === 'ArrowDown'){
                let returnedArray = component.get('v.resulsArray');
                let position = component.get('v.position');
                if(returnedArray != undefined && position != undefined){
                    let oldElements = document.getElementsByClassName(returnedArray[position]);
                    let newElements = document.getElementsByClassName(returnedArray[position + 1]);
    
                    if(position + 1 < returnedArray.length){
                        if(oldElements[0] != undefined){
                            $A.util.removeClass(oldElements[0], 'backgroundfocus');
                        }
                        if(newElements[0] != undefined){
                            $A.util.addClass(newElements[0], 'backgroundfocus');
                            component.set('v.position', position + 1);
                        }
                    }
                }
            }
        });
    },
})